<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Cryptocoin;
use App\Models\Order;
use App\Models\Transaction;
use Twilio\Rest\Preview\Understand\Service\ModelBuildInstance;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Services\OrderService;
use App\Services\WalletService;
class WSPrivateCrontroller extends Controller
{

    public function setOrder(Request $request){
        if ($request->user()->tokenCan('place-orders')) {
            $obj = [];
            $coin = Cryptocoin::where("symbol",$request->get("coin"))->first();
            $obj["coin"] = $coin->id;
            $obj["qntd"] = $request->get("value");
            $obj["value"] = $request->get("price");
            $obj["type"] = $request->get("type");
            
            $os = new OrderService();
            $orders = $os->createOrder($obj);
            if (isset($orders["error"]))
                return response()->json($orders);
            else
                return response()->json(["uuid"=>$orders->uuid]);
        }
    }

    public function getMyOrders(Request $request){    
        $orders =  (Order::where("type","buy")
                            ->whereHas('coin' , function ($query) use ($request) {
                                $query->where('symbol', $request->get("coin"));
                            })
                            ->where("user_id",auth()->user()->id)
                            ->with("tax")
                            ->orderBy('id', 'DESC')
                            ->get());
        dd($orders);
    }

    public function myBalance(Request $request){
        $ws = new WalletService();
        $symbol = "get".$request->get("coin");
        return response()->json(["balance"=>$ws->{$symbol}()->getBalance()]);
    }

    public function deleteOrder(Request $request){
        if ($request->user()->tokenCan('place-orders')) {
            $obj = [];            
            $obj["uuid"] = $request->get("uuid");
            
            $os = new OrderService();
            $orders = $os->delete($obj);
            if (isset($orders["error"]))
                return response()->json($orders);
            else
                return response()->json(["success"=>$orders]);
        }
    }

}

