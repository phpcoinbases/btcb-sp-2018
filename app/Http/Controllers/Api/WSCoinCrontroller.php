<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Cryptocoin;
use App\Models\Order;
use App\Models\Transaction;
use Twilio\Rest\Preview\Understand\Service\ModelBuildInstance;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class WSCoinCrontroller extends Controller
{
    
    /**
     * Request JSON With last 10 values from BTC COIN
     * @return JSON 
     */
    public function getCoinMarket(Request $request,$type,$limit){
        //LIMITAR AO MAXIMO DEFINIDO NO CONF DO SISTEMA
        if ($limit>config("values.api.coins_limit")){
            $limit = config("values.api.coins_limit");
        }
        return Cryptocoin::where("symbol",$type)
                                                ->with(['prices' => function ($query) use ($limit)  {
                                                    $query->take($limit);
                                                    $query->orderBy('created_at', 'desc');
                                                }])
                                                ->first()
                                                ->toJson();
    }


    /**
     * Request JSON With last 24h orders
     * @return JSON 
     */
    public function getBook(Request $request,$type){
        
        $coin = Cryptocoin::where("symbol",$type)->first();

        $orders = Order::where("cryptocoin_id",$coin->id)
                        ->where('created_at', '>=', Carbon::now()->subDay()->toDateTimeString())
                        ->where("qntd",">",0)
                        ->select("value","qntd as coins","type")
                        ->orderBy("value","DESC")
                        ->get();

        $OrderVendas  = collect($orders->where('type', "sell"));
        $OrderCompras = $orders->where('type', "buy");
        
        return response()
        ->json(
            [
                ["sell"=>$OrderVendas->values()],
                ["buy"=>$OrderCompras->values()]
            ]
        );
                                                
        
    }
    /**
     * Request JSON With day ticker coin
     * @return JSON 
     */
    public function getTicker(Request $request,$type){
        
        $coin = Cryptocoin::where("symbol",$type)->first();

        $orders = Order::where("cryptocoin_id",$coin->id)
                        ->where('created_at', '>=', Carbon::now(-3)->subDay()->toDateTimeString())
                        ->get();

        $transaction = Transaction::whereHas('ordem_comprador', function ($query) use ($coin) {
                            $query->where('cryptocoin_id', "=", $coin->id);
                        })
                        ->where('created_at', '>=', Carbon::now(-3)->subDay()->toDateTimeString())                   
                        ->orderBy('created_at','ASC')
                        ->get();

        $OrderVendas  = $orders->where('type', "sell");
        $OrderCompras = $orders->where('type', "buy");
           
        //Transaction High
        $HighDay  = $transaction->max('valor_venda');
        //Transaction Low
        $LowDay   = $transaction->min('valor_venda');
        //Transaction Last
        $LastDay  = $transaction->last()->valor_venda;
        //Order Buy High     
        $buyHigh = $OrderVendas->max("value");
        //Order Sell Low
        $sellLow = $OrderCompras->min("value");
        //Volume Sum
        $SumDay  = $transaction->sum('comprador_vai_receber');

        return response()
        ->json(
            ["ticket"=>[
                'coin'=>$coin->symbol,
                'high_of_24h'=>$HighDay,
                'low_of_24h'=>$LowDay,
                'last_of_24h'=>$LastDay,
                'sell_low_of_24h'=>$sellLow,
                'buy_high_of_24h'=>$buyHigh,
                'volume_of_24h'=>$SumDay,
                'time' => Carbon::now(-3)->timestamp
            ]]
        );
                                                
        
    }

    /**
     * Request JSON With last 24h trades
     * @return JSON 
     */
    public function getTrades(Request $request,$type){
        
        $coin = Cryptocoin::where("symbol",$type)->first();

        $transaction = Transaction::whereHas('ordem_comprador', function ($query) use ($coin) {
                                                $query->where('cryptocoin_id', "=", $coin->id);
                                    })
                                    ->where('created_at', '>=', Carbon::now(-3)->subDay()->toDateTimeString())                   
                                    ->select()
                                    ->orderBy('created_at','ASC')
                                    ->get();
        
        $objs = [];                            
        if(count($transaction))
            $transaction->each(function($obj) use (&$objs){
                $array = [];
                if ($obj->vendendo == $obj->comprando){
                    $array["amount"]= $obj->comprando;
                }elseif ($obj->vendendo > $obj->comprando){                    
                    $array["amount"]= $obj->vendendo-$obj->comprando;
                }else{
                    $array["amount"]= $obj->comprando-$obj->vendendo; 
                }
                $array["value"]= $obj->valor_venda;
                $array["type"]= $obj->type_order;    
                $objs[] = $array;
            });

        return response()
                        ->json(
                            [
                                ["trades"=>$objs]
                            ]
                        );
                                                
        
    }

    /**
     * Request JSON With day ticker coin
     * @return JSON 
     */
    public function getSumarry(Request $request,$type,$year="",$month="",$day=""){
        
        if ($year=="" || $month=="" || $day==""){
            $start = Carbon::create(date("Y"), date("m"), date("d"), 0, 0, 0, 'America/Sao_Paulo');
            $end   = Carbon::create(date("Y"), date("m"), date("d"), 23, 59, 59, 'America/Sao_Paulo');    
        }else{
            $start = Carbon::create($year, $month, $day, 0, 0, 0, 'America/Sao_Paulo');
            $end   = Carbon::create($year, $month, $day, 23, 59, 59, 'America/Sao_Paulo');
        }

        $coin = Cryptocoin::where("symbol",$type)->first();

        $orders = Order::where("cryptocoin_id",$coin->id)
                        ->where('created_at', '>=', $start->toDateTimeString())
                        ->where('created_at', '<=', $end->toDateTimeString())
                        ->get();


        $transaction = Transaction::whereHas('ordem_comprador', function ($query) use ($coin) {
                            $query->where('cryptocoin_id', "=", $coin->id);
                        })
                        ->where('created_at', '>=', $start->toDateTimeString())
                        ->where('created_at', '<=', $end->toDateTimeString())                   
                        ->orderBy('created_at','ASC')
                        ->get();

        $OrderVendas  = $orders->where('type', "sell");
        $OrderCompras = $orders->where('type', "buy");
           
        //Transaction High
        $HighDay  = $transaction->max('valor_venda');
        //Transaction Low
        $LowDay   = $transaction->min('valor_venda');
        //Transaction Last
        $LastDay  = $transaction->last()->valor_venda;
        //Transaction Start
        $StartDay  = $transaction->first()->valor_venda;        
        //Quantity
        $Quantity  = $transaction->sum('value_coin');
        //Volume Sum
        $SumDay  = $transaction->sum('value_money');
        $countDay  = $transaction->count();
        $avg    = $transaction->avg('valor_venda');
        return response()
        ->json(
            ["summary"=>[
                'coin'=>$coin->symbol,
                'date'=>$start->format("Y-m-d"),
                'open'=>$StartDay,
                'close'=>$LastDay,
                'low'=>$LowDay,
                'high'=>$HighDay,
                'volume'=>$SumDay,
                'quantity'=>$Quantity,
                'amount'=>$countDay,
                'average' => $avg
            ]]
        );
                                                
        
    }

}
