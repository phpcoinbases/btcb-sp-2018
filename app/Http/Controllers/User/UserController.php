<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class UserController extends Controller
{
    /**
     * Show the user perfil
     *
     * @return \Illuminate\Http\Response
     */
    public function perfil(Request $request){
        return view("dashboard.user.perfil");
    }

    
}
