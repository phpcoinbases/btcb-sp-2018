<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\User;
use App\Models\Deposit;
use Illuminate\Support\Facades\Hash;
use App\Models\Magazine;
use App\Models\Tx;
use App\Models\Tax;
use App\Models\TaxUser;
use Webpatser\Uuid\Uuid;
use App\Services\WalletService;
use App\Models\Wallet;
use App\Models\Cryptocoin;

class AdminController extends Controller
{


    public function verifySys(){
        if (!Auth::check() || !Auth::user()->hasRole("super-admin")){
            return false;
        }
        return true;
    }


    public function login(Request $request){
        return view("admin.login.login");
    }

    public function postlogin(Request $request){  
        $item = auth()->attempt([
            'email'    => $request->get("email"),
            'password' =>$request->get("password")
        ]);
        if ($item)
        {
            return  redirect()->route("admin.index");
        }
        return redirect()->back()->withErrors(["email"=>"Email/Senha incorreto"]);
    }

    public function index(Request $request){
        //VALID SYS
        if (!$this->verifySys()){return redirect()->route("admin.login");}
            
        return view("admin.home")->with(["deposits"=>Deposit::with("user")->get()]);
    }

    public function clientes(Request $request){
        //VALID SYS
        if (!$this->verifySys()){return redirect()->route("admin.login");}
            
        return view("admin.clients");
    }


    public function landingCadastro(Request $request){
        //VALID SYS
        if (!$this->verifySys()){return redirect()->route("admin.login");}
            
        return view("admin.landing.cadastro");
    }
    
    public function landingContato(Request $request){
        //VALID SYS
        if (!$this->verifySys()){return redirect()->route("admin.login");}
            
        return view("admin.landing.contato");
    }
    
    public function news(Request $request){
        //VALID SYS
        if (!$this->verifySys()){return redirect()->route("admin.login");}
            
        return view("admin.landing.news");
    }

    public function deleteNews(Request $request,$id){
        //VALID SYS
        if (!$this->verifySys()){return redirect()->route("admin.login");}
        $news = Magazine::find($id)->delete();    
        return redirect()->back();
    }

    public function saveNews(Request $request){
        //VALID SYS
        if (!$this->verifySys()){return redirect()->route("admin.login");}
        $validator = Validator::make($request->all(), [
            'description' => 'required',
            'link' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }   
        
        Magazine::create([
            "link"=>$request->get("link"),
            "description"=>$request->get("description"),
        ]);

        
        return redirect()->back();
    }  


    public function updateDeposit($id){
        $deposit = Deposit::find($id);
        $deposit->status = $request->status;
        if ($deposit->save()) {
            return redirect()->back();
        }else{
            return redirect()->back()->withErrors(["error"=>"Nao foi possivel alterar o status"]);
        }
    }

    public function tx(Request $request){
        //VALID SYS
        if (!$this->verifySys()){return redirect()->route("admin.login");}
            
        return view("admin.tx.index");
    }

    public function txTaxas(Request $request,$id){
        //VALID SYS
        if (!$this->verifySys()){return redirect()->route("admin.login");} 
        $tx = Tx::where("id",$id)->with("taxes")->first();           

        return view("admin.tx.taxas")->with(compact("tx"));
    }


    public function saveTxUser(Request $request){
        $txuser = new TaxUser();
        $txuser->uuid =  (string) Uuid::generate()->string;
        $txuser->user_id = $request->get("user_id");
        $txuser->tx_id = $request->get("taxa_user");
        if ($txuser->save()) {
            return redirect()->back();
        }else{
            return redirect()->back()->withErrors(["error"=>"Nao foi possivel alterar a taxa do usuario"]);
        }

    }

    public function saveTx(Request $request){
        $tx = Tx::create(["name"=>$request->name]);        
        if ($tx->id>0) {
            return redirect()->back();
        }else{
            return redirect()->back()->withErrors(["error"=>"Nao foi possivel criar o Grupo de Taxa"]);
        }
    }

    public function saveTaxas(Request $request,$id){
        $tx = Tx::where("id",$id)->with("taxes")->first(); 
        if($tx->taxes->first())
            $tx->taxes->each(function($obj){
                $obj->delete();
            });

        $array = [
            new Tax(["our_tax"=>$request->get('deposit_cash'),"fixed"=>0,"type"=>"deposit_cash"]),
            new Tax(["our_tax"=>$request->get('deposit_btc'),"fixed"=>0,"type"=>"deposit_btc"]),
            new Tax(["our_tax"=>$request->get('withdraw_cash')["our_tax"],"fixed"=>$request->get('withdraw_cash')["fixed"],"type"=>"withdraw_cash"]),
            new Tax(["our_tax"=>$request->get('withdraw_not_bank')["our_tax"],"fixed"=>$request->get('withdraw_not_bank')["fixed"],"type"=>"withdraw_not_bank"]),
            new Tax(["our_tax"=>$request->get('withdraw_btc')["our_tax"],"fixed"=>$request->get('withdraw_btc')["fixed"],"type"=>"withdraw_btc"]),
            new Tax(["our_tax"=>$request->get('withdraw_btc')["our_tax"],"fixed"=>$request->get('withdraw_btc')["fixed"],"type"=>"withdraw_eth"]),
            new Tax(["our_tax"=>$request->get('passive'),"fixed"=>0,"type"=>"passive"]),
            new Tax(["our_tax"=>$request->get('ative'),"fixed"=>0,"type"=>"ative"])            
        ];    
        $tx->taxes()->saveMany($array);
        return redirect()->back();

    }

    public function deleteTx(Request $request,$id){        
        if (Tx::find($id)->delete()) {
            return redirect()->back();
        }else{
            return redirect()->back()->withErrors(["error"=>"Nao foi possivel criar o Grupo de Taxa"]);
        }
    }   
    
    public function wallets(Request $request){
        //VALID SYS
        if (!$this->verifySys()){return redirect()->route("admin.login");}
        if ($request->method()=="POST"){
            
            $wallets = Wallet::where("user_id",auth()->user()->id)->delete();
            $coins = ["BTC"=>$btc,"ETH"=>$eth,"BCH"=>$bch,"LTC"=>$ltc];
                
            foreach ($coins as $key => $value){
                $wallet = Wallet::create([
                    'cryptocoin_id'=>Cryptocoin::where("symbol",$key)->first()->id,
                    'user_id'=>auth()->user()->id,
                    'address'=>$request->get("wallet")[$key],
                ]);
            }
        }
        $user = auth()->user()->with('wallets')->first();
        if ($user!=null){
            if($user->wallets()->count()==0){
                $ws = new  WalletService();        
                $uuid = (string) Uuid::generate()->string;
                $btc = ($ws->getBTC()->client->getaccountaddress($uuid)->result());
                $bch = ($ws->getBCH()->client->getaccountaddress($uuid)->result());
                $ltc = ($ws->getLTC()->client->getaccountaddress($uuid)->result());
                $eth = ($ws->getETH()->client->personal()->newAccount($uuid)->toString());
                                
                $coins = ["BTC"=>$btc,"ETH"=>$eth,"BCH"=>$bch,"LTC"=>$ltc,"XRP"=>1];
                
                foreach ($coins as $key => $value){
                    if ($key!="XRP")
                        $wallet = Wallet::create([
                            'cryptocoin_id'=>Cryptocoin::where("symbol",$key)->first()->id,
                            'user_id'=>$user->id,
                            'address'=>$value,
                        ]);
                    else
                        $ws->getXRP()->saveWallet();
                }
            }
        }
        return view("admin.wallets.index");
    }


    public function deposits(Request $request){
        //VALID SYS
        if (!$this->verifySys()){return redirect()->route("admin.login");}
            
        return view("admin.deposits.index")->with(["deposits"=>Deposit::with("user")->where("status","pending")->get()]);
    }

    public function deposits_denied(Request $request){
        //VALID SYS
        if (!$this->verifySys()){return redirect()->route("admin.login");}
        $obj = Deposit::find($request->get("deposit_id"));
        $obj->status = "negative";
        $obj->save();
        return redirect()->back()  ;
       
    }

    public function deposits_accept(Request $request){
        //VALID SYS
        if (!$this->verifySys()){return redirect()->route("admin.login");}
        $obj = Deposit::find($request->get("deposit_id"));
        $obj->status = "active";
        $obj->save();
        return redirect()->back() ; 
       
    }

    public function saques(Request $request){
        
    }
    
}
