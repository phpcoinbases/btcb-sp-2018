<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\PasswordSecurity;
use App\User;
class PasswordSecurityController extends Controller
{
    public function show2faForm(Request $request){
        $user = Auth::user();
        $google2fa_url = "";        
        if($user->passwordSecurity()->exists()){
            $google2fa = app('pragmarx.google2fa');
            $google2fa->setAllowInsecureCallToGoogleApis(true);
            $google2fa_url = $google2fa->getQRCodeGoogleUrl(
                env("APP_NAME"),
                $user->email,
                $user->passwordSecurity->google2fa_secret
            );
        }
        $data = array(
            'user' => $user,
            'google2fa_url' => $google2fa_url
        );
        return view('auth.2fa')->with('data', $data);
    }

    public function generate2faSecret(Request $request){
        $user = Auth::user();
        // Initialise the 2FA class
        $google2fa = app('pragmarx.google2fa');
        $google2fa->setAllowInsecureCallToGoogleApis(true);

        // Add the secret key to the registration data
        PasswordSecurity::create([
            'user_id' => $user->id,
            'google2fa_enable' => 0,
            'google2fa_secret' => $google2fa->generateSecretKey(),
        ]);
            
        return redirect('/2fa')->with('success',"Codigo QR Code gerado com sucesso. Valide sua conta");
    }

    public function enable2fa(Request $request){
        $user = Auth::user();
        $google2fa = app('pragmarx.google2fa');
        $secret = $request->get('verify-code');
        $valid = $google2fa->verifyKey($user->passwordSecurity->google2fa_secret, $secret);
        if($valid){
            $user->passwordSecurity->google2fa_enable = 1;
            $user->passwordSecurity->save();
            return redirect('2fa')->with('success',"2FA foi ativado com sucesso.");
        }else{
            return redirect('2fa')->with('error',"Codigo de validação invalido. Tente novamente");
        }
    }

    public function disable2fa(Request $request){
        $user = Auth::user();
        $user->passwordSecurity->google2fa_enable = 0;
        $user->passwordSecurity->save();
        return redirect('/2fa')->with('success',"2FA está desativado.");
    }

    public function confirm($confirmation_code)
    {
        if( ! $confirmation_code)
        {
            return redirect('/login')->with(["error"=>"Digite o Codigo na URL"]);
        }
        $user = User::whereConfirmationCode($confirmation_code)->first();

        if ( ! $user)
        {
            return redirect('/login')->with(["error"=>"Codigo ja validado"]);
        }

        $user->confirmed = 1;
        $user->confirmation_code = null;
        $user->save();


        return redirect('/dashboard')->with('success',"");
    }    

}
