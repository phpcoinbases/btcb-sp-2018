<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use App\Models\TaxUser;
use App\Models\Tx;
use Webpatser\Uuid\Uuid;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    /**
     * Check user session.
     *
     * @return Response
     */
    public function checkSession()
    {
        return Response::json(['guest' => Auth::guest()]);
    }

 /**

    * Handle Social login request

    *

    * @return response

    */

    public function socialLogin($social)
   
      {
        
          return Socialite::driver($social)->redirect();
   
      }
   
      /**
   
       * Obtain the user information from Social Logged in.
   
       * @param $social
   
       * @return Response
   
       */
   
      public function handleProviderCallback($social)
   
      {
   
          $userSocial = Socialite::driver($social)->user();
            
          if ($userSocial->getEmail()==""){
              $email = $userSocial->nickname;
          }else{
              $email = $userSocial->getEmail();
          }
          $user = User::where(['email' => $email])->first();
          if($user){
              Auth::login($user);   
              return redirect()->route('dashboard');   
          }else{
                $confirmation_code = str_random(30);
                
                if ($userSocial->getEmail()!=""){
                    Mail::send('emails.verify', ["confirmation_code"=>$confirmation_code], function($message) use ($userSocial){
                        $message->to($userSocial->getEmail(), $userSocial->getName())
                            ->from("contato@email.coinbold.com.br")
                            ->subject('Confirme seu endereço de email');
                    });        
                }
                $user = User::create([
                    'name' => $userSocial->getName(),
                    'email' => $email,
                    'password' => Hash::make($confirmation_code),
                    'confirmation_code' => $confirmation_code,
                ]);
                $tx = Tx::where("name","like","%Default%")->first();
                if (isset($tx->id)){
                    $txuser = new TaxUser();
                    $txuser->uuid =  (string) Uuid::generate()->string;;
                    $txuser->user_id = $user->id;
                    $txuser->tx_id = $tx->id;
                    $txuser->save();
                }
                Auth::login($user);   
                $user->info()->create(["uuid"=>$userSocial->id]);
                return redirect()->route('dashboard');   
   
          }
   
        }

    

}
