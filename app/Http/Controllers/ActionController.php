<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Withdraw;
use App\Services\WalletService;
use App\Models\Cryptocoin;
use App\Models\Wallet;
use App\User;
class ActionController extends Controller
{
    public function liberar_saque(Request $request){
        $obj    = Withdraw::where("uuid",$request->uuid)->first();
        $coin   = Cryptocoin::where("id",$obj->cryptocoin_id)->first();
        $ws     = new WalletService();
        $client = ($ws->{"get$coin->symbol"}());
        $to     = ($obj->address);
        $from   = Wallet::where("cryptocoin_id",$coin->id)->where("user_id",$obj->user_id)->first();
        $user   = User::where("id",$obj->user_id)->first();
        
        //MANDA A TAXA PARA O ADMIN
        $client->transfer($from->address,"",$obj->value_tax);
        //TRANSFERE PARA A CONTA DO USUARIO
        $client->withdraw($from->address,$to,$obj->value_coin-$obj->value_tax);
        
    }
}
