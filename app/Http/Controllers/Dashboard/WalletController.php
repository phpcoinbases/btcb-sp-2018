<?php 
namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use App\Models\Tax;
use App\Services\WalletService;
use App\Services\OrderService;
use App\Models\Order;
use App\Models\Transaction;
use App\Models\Cryptocoin;
use Carbon\Carbon;
use App\Models\TaxUser;
class WalletController extends Controller
{

    public function getWallets(){
        // Verifica se está logado, se não tiver redireciona
        if ( !auth()->check() )
            return response()->json(['errors' => ['user' => ['You are not logged']]], 422);

        /*
        * Verifica se o usuario tem carteiras criadas
        */
        $coins = ["BTC"=>$btc,"ETH"=>$eth,"BCH"=>$bch,"LTC"=>$ltc,"XRP"=>1];   
        $ws = new  WalletService();   
        if (auth()->user()->load("wallets")->wallets->count()==0){
                $btc = ($ws->getBTC()->client->getaccountaddress(auth()->user()->uuid)->result());
                $bch = ($ws->getBCH()->client->getaccountaddress(auth()->user()->uuid)->result());
                $ltc = ($ws->getLTC()->client->getaccountaddress(auth()->user()->uuid)->result());
                $eth = ($ws->getETH()->client->personal()->newAccount(auth()->user()->uuid)->toString());
                            
                foreach ($coins as $key => $value){
                    if ($key!="XRP")
                        $wallet = Wallet::create([
                            'cryptocoin_id'=>Cryptocoin::where("symbol",$key)->first()->id,
                            'user_id'=>auth()->user()->id,
                            'address'=>$value,
                        ]);
                    else
                        $ws->getXRP()->saveWallet();
                }
        }
        $array = [];
        foreach($coins as $key => $coin){
            $client = ($ws->{"get$key"}());
            $array[]=["coin"=>$key,"address"=>$client->getWallet()];    
        }
        //$array[]=["coin"=>"Dinheiro","value"=>$ws->getBalance(),"address"=>""];
        return  response()->json($array);
        
    }

}