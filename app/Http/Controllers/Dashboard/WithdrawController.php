<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Cryptocoin;
use App\Models\Wallet;
use App\Models\Tax;
use App\Models\Bank;
use App\Services\WalletService;
use App\Models\TaxUser;
use Webpatser\Uuid\Uuid;
class WithdrawController extends Controller
{
 
    public function index(Request $request){
        $eth = (Cryptocoin::where("symbol","ETH")->first()->id);
        $btc = (Cryptocoin::where("symbol","BTC")->first()->id);
        $wallets = [];
        $wallets["banks"] = Bank::orderBy("code")->get();

        return view("dashboard.withdraw.index")->with(compact("wallets"));
    }

    public function money(Request $request){
        $input = request()->validate([
            'bank' => 'required',
            'ag' => 'required',
            'cc' => 'required',
            'value' => 'required'
        ], [
        ]);
        $ws = new WalletService();

        //NOSSAS TAXAS
        $tax = Tax::where("type","withdraw_cash")->latest()->first();
        
        $taxas = 1+($tax->our_tax/100);
        $fixo  = $tax->fixed;
        $valor = $request->value*$taxas-$fixo;
        if ($ws->getBalance()<$valor){
            return redirect()->back()->withErrors(["btc_error"=>'Você não tem fundos suficientes para realizar essa operacao']);
        }

        auth()->user()->withdraws()->create([
            "value"=>$request->value,
            "bank"=>$request->bank,
            "agency"=>$request->ag,
            "account"=>$request->cc,            
            "type"=>"conta",
            "status"=>"pending",
        ]);
        $withdraw = auth()->user()->withdraws()->latest()->first();

        return back()
                    ->with('success','Saque foi encaminhado para nossa central em ate 24h será realizado');

    }

    public function coin(Request $request){

        $input = request()->validate([
            'withdraw_coin' => 'required',
            'withdraw_value' => 'required',
            'withdraw_address' => 'required'
        ], [
            'withdraw_address.required'=>"Prencha o endereco da carteira que deseja transferir",
            'withdraw_coin.required'=>"Preencha o valor que deseja transferir"
        ]);

        
        $coin = Cryptocoin::where("id",$request->get("withdraw_coin"))->first();

        
        $ws = new WalletService();
        $client = ($ws->{"get$coin->symbol"}());
        
        if (!$client->validateAddress($request->withdraw_address)){
            return redirect()->back()->withErrors(["btc_error"=>'Endereço da sua carteira invalido']);
        }
        
        //NOSSAS TAXAS - TAXA DO USUARIO
        $tax = TaxUser::where("user_id",auth()->user()->id)->with("tx.taxes")->latest()->first();        
        $taxa_saque  = $tax->tx->taxes->where("type","withdraw_btc")->first()->our_tax;
        $valor_total = ($request->withdraw_value-$taxa_saque);

        $uuid =  (string) Uuid::generate()->string;
        $obj = auth()->user()->withdraws()->create([
            "value_coin"=>$request->withdraw_value,
            "value_tax"=>$taxa_saque,
            "address"=>$request->withdraw_address,
            "cryptocoin_id"=>$coin->id,
            "type"=>"coin",
            'uuid' => $uuid,
            "tax_id"=>$tax->tx->taxes->where("type","withdraw_btc")->first()->id,
            "status"=>"pending",
        ]);

        //SEND EMAIL TO USER AUTHORIZE TRANSFER
        if ($obj)        
            dd(route('withdraw.authorize', ['uuid' => $obj->uuid]));
    

            //ENVIAR EMAIL 

            return back()
                    ->with('success_btc','Foi enviado para o seu email um link pedindo autorizacao');

    }

    public function eth(Request $request){

        $input = request()->validate([
            'eth' => 'required',
            'valueeth' => 'required'
        ], [
            'eth.required'=>"Prencha o endereco da carteira que deseja transferir",
            'valueeth.required'=>"Preencha o valor que deseja transferir"
        ]);

        $ws = new WalletService();
        if (!$ws->getETH()->validate($request->eth)){
            return redirect()->back()->withErrors(["eth_error"=>'Endereço de ETH invalido']);
        }

        //NOSSAS TAXAS
        $tax = Tax::where("type","withdraw_eth")->latest()->first();
        $taxas = ($tax->our_tax+$tax->fixed);
        if ($ws->getETH()->getBalance()<($request->valueeth+$taxas)){
            return redirect()->back()->withErrors(["eth_error"=>'Você não tem fundos suficientes para realizar essa operacao']);
        }

        auth()->user()->withdraws()->create([
            "valueeth"=>$request->valueeth,
            "address"=>$request->eth,
            "type"=>"eth",
            "status"=>"pending",
        ]);

        //CRIAR EXTRATO        
        $withdraw = auth()->user()->withdraws()->latest()->first();
        auth()->user()->extracts()->create([
            "withdraw_id"=>$withdraw->id,
            "tax_id"=>$tax->id,
            "type"=>"Saque",
            "value"=>$request->valueeth
        ]);
        return back()
                    ->with('success_eth','Saque foi encaminhado para nossa central em ate 24h será realizado');

    }
}
