<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
class DashboardController extends Controller
{
    public function index()
    {
        return view('dashboard.home');
    }

    public function api(Request $request)
    {    
        $user = Auth::user();
        //dd( $user->createToken('My Token', ['place-orders'])->accessToken);
        return view('dashboard.api.index');
    }
    
    public function generateApi(Request $request)
    {    
        $user = Auth::user();
        
        if ($request->get("scope")==1){
            $key = ( $user->createToken($request->get("appname"), ['place-orders'])->accessToken);
        }else{
            $key = ( $user->createToken($request->get("appname"), [])->accessToken);
        }
        
        return redirect()->back()->with(["accessToken"=>$key]);
    }      
}
