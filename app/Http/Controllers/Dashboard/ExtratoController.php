<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ExtratoController extends Controller
{
    public function index(Request $request){
        $extratos = auth()->user()->extracts()
                                  ->with("orders","tax","wallet","cryptocoin","deposit","withdraw")                               
                                  ->orderBy('created_at', 'DESC')
                                  ->get();

        return view("dashboard.extrato.index")->with(compact("extratos"));
    }
}
