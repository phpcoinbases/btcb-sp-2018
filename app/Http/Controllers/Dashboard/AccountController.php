<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\UserInfo;
class AccountController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function perfil()
    {    
        return view('dashboard.user.perfil');
    }



    public function save(Request $request){
        $input = request()->validate([
                'comprovante' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'document_file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'perfil_file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'origem' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'name' => 'required'
            ], [
                'comprovante.image' => 'Envie o arquivo tipo JPG,PNG,GIF no maximo 2mb',
                'document_file.image' => 'Envie o arquivo tipo JPG,PNG,GIF no maximo 2mb',
                'perfil_file.image' => 'Envie o arquivo tipo JPG,PNG,GIF no maximo 2mb',
                'name.required' => 'Não deixe o nome em branco'
            ]);
        $perfil = UserInfo::where("user_id",auth()->user()->id)->first();
        
        if ($request->hasFile('comprovante')) {
            $imageName = time().'.'.request()->comprovante->getClientOriginalExtension();
            request()->comprovante->move(public_path('images'), $imageName);
            $perfil->comprovante = $imageName;    
        }
        if ($request->hasFile('document_file')) {
            $imageName = time().'.'.request()->document_file->getClientOriginalExtension();
            request()->document_file->move(public_path('images'), $imageName);
            $perfil->document_file = $imageName;
        }
        if ($request->hasFile('origem')) {
            $imageName = time().'.'.request()->origem->getClientOriginalExtension();
            request()->origem->move(public_path('images'), $imageName);
            $perfil->origem = $imageName;
        }
        if ($request->hasFile('perfil_file')) {
            $imageName = time().'.'.request()->perfil_file->getClientOriginalExtension();
            request()->perfil_file->move(public_path('images'), $imageName);
            $perfil->perfil_file = $imageName;
        }        
        if ($request->hasFile('ata')) {
            $imageName = time().'.'.request()->ata->getClientOriginalExtension();
            request()->ata->move(public_path('images'), $imageName);
            $perfil->ata = $imageName;
        }
        $perfil->save();
        auth()->user()->name = $request->name;
        auth()->user()->save();

        return back()
            ->with('success','You have successfully upload image.');
    }


    public function delete(Request $request,$type){
     
        $perfil = UserInfo::where("user_id",auth()->user()->id)->first();
        $perfil->{$type}= null;
        $perfil->save();

        return back()
            ->with('success','You have successfully upload image.');
    }

}
