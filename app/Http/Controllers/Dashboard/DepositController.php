<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Cryptocoin;
use App\Models\Wallet;
use App\Models\Tax;

class DepositController extends Controller
{
 
    public function index(Request $request){

        return view("dashboard.deposit.index")->with(compact("wallets"));
    }

    public function saveDeposit(Request $request){
        $input = request()->validate([
                'comprovante' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'value' => 'required'
            ], [
                'comprovante.required' => 'Envie o arquivo do TED no maximo 2mb',
                'value.required' => 'Digite o valor do TED'
            ]);
        if ($request->file('comprovante')->isValid()) {
            $imageName = time().'.'.request()->comprovante->getClientOriginalExtension();        
            //Set Directory
            $directory = "deposits";
            //Put on S3 Bucket:
            $path = \Storage::disk('s3')->put($directory.$filename , $request->file('comprovante'),"public");

            auth()->user()->deposits()->create([
                "value"=>$request->value,
                "type"=>"ted",
                "status"=>"pending",
                "comprovante"=>$path
            ]);

            //CRIAR EXTRATO
            $deposit = auth()->user()->deposits()->latest()->first();
            //$deposit->addMediaFromRequest('comprovante')->toMediaCollection();;
            
            $tax = Tax::where("type","deposit_cash")->latest()->first();
            auth()->user()->extracts()->create([
                "deposit_id"=>$deposit->id,
                "tax_id"=>$tax->id,
                "type"=>"Deposito",
                "value"=>$request->value
            ]);

            return back()
                ->with('success','You have successfully upload image.');
        }
    }

}
