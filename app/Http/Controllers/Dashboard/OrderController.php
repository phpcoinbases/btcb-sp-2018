<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use App\Models\Tax;
use App\Services\WalletService;
use App\Services\OrderService;
use App\Models\Order;
use App\Models\Transaction;
use App\Models\Cryptocoin;
use Carbon\Carbon;
use App\Models\TaxUser;
class OrderController extends Controller
{
    public function index(Request $request){        
        $tax = TaxUser::where("user_id", auth()->user()->id)->with("tx.taxes")->latest()->first();
        $tax = $tax->tx->taxes->where("type","ative")->first();
        return view("dashboard.order.index")->with(compact("tax"));
    }

    public function lista(Request $request){
        $nowInSPTz = Carbon::now('America/Sao_Paulo');
        
        
        $arr["compras"] = Order::where("type","buy")->where("qntd",">",0)->with("coin")->latest()->limit(10)->get();
        $arr["vendas"] = Order::where("type","sell")->where("qntd",">",0)->with("coin")->latest()->limit(10)->get();
        $arr["confirmadas"] = Transaction::with("ordem_vendedor","ordem_vendedor.coin")->latest()->limit(20)->get();
       
        return view("dashboard.order.lista")->with(compact("arr"));
    }

    /**
     * Realiza a Ordem de Venda
     * Valida os dados do comprador vendo se esse possui saldo em dinheiro
     * @param Request $request
     * @param Auth auth()
     * return Response 
     */
    public function buy(Request $request){
        $input = request()->validate([
            'buy_qntd' => 'required',
            'buy_coin' => 'required',
            'buy_price' => 'required'
        ], [
            'buy_price.required' => 'Digite o valor',
            'buy_qntd.required' => 'Digite o valor'
        ]);
        
        $orders = $this->order($request,"buy");
        
        if (isset($orders["error"])){
            return redirect()->back()->withErrors(["buy_error"=>$orders["error"]]);
        }
        return back()->with('success_buy','Ordem de venda Compra com Sucesso . ID:'.$orders->uuid);

    }

    /**
     * Realiza a Ordem de Compra
     * Valida os dados do comprador vendo se esse possui saldo na Moeda
     * @param Request $request
     * @param Auth auth()
     * return Response 
     */
    public function sell(Request $request){
        $input = request()->validate([
            'sell_qntd' => 'required',
            'sell_coin' => 'required',
            'sell_price' => 'required'
        ], [
            'sell_price.required' => 'Digite o valor',
            'sell_qntd.required' => 'Digite o valor'
        ]);
    
        $orders = $this->order($request,"sell");

        if (isset($orders["error"])){
            return redirect()->back()->withErrors(["sell_error"=>$orders["error"]]);
        }        
        return back()->with('success_sell','Ordem de venda Criada com Sucesso. ID:'.$orders->uuid);       
    }

    private function order($request,$type){
        $os = new OrderService();
        
        //NOSSAS TAXAS - TAXA DO USUARIO
        $tax = TaxUser::where("user_id",auth()->user()->id)->with("tx.taxes")->latest()->first();
        
        $obj = [];
        $obj["qntd"]  = $request->get($type."_qntd");
        $obj["value"] = $request->get($type."_price");
        $obj["coin"] = $request->get($type."_coin");
        $obj["type"] = $type;
        $obj["tax"] = $tax->tx->taxes->where("type","ative")->first()->id;
        

        $orders = $os->createOrder($obj);

        if (isset($orders["error"])){
            return $orders;
        }
        $os->setOrder($orders->id);
        return $orders;
    }
}
