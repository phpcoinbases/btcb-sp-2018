<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Twilio\Rest\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Guard;
use App\Services\WalletService;
use App\Models\Cryptocoin;
use App\Models\Tax;
use App\Models\UserList;
use App\Support\UserAccount;
use App\Models\Contact;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Foundation\Console\PackageDiscoverCommand;
use Illuminate\Support\Facades\Artisan;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {  
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public  function landingPage(Request $request){ 
        return view('landing.home');
    }
    public  function landingQuemPage(Request $request){ 
        return view('landing.quem');
    }

    public  function landingTermosPage(Request $request){ 
        return view('landing.termos');
    }


    public function returnCoins(Request $request){ 
        Artisan::command("coin:update",function($response){});       
        return view('landing.coins');
    }

    public function returnCoinsBTC(Request $request){ 
        return view('landing.bitcoin');
    }

    public  function saveUser(Request $request){ 
        return UserList::create(["email"=>$request->get("email")]);
    }

    public  function saveContact(Request $request){ 
       return Contact::create([
            "firstname"=>$request->get("firstname"),
            "lastname"=>$request->get("lastname"),
            "phone"=>$request->get("phone"),
            "cel"=>$request->get("cel"),
            "msg"=>$request->get("msg"),
        ]);
    }

}
