<?php

namespace App\Http\Middleware;

use Closure;
use App\Services\WalletService;
use App\Models\Cryptocoin;
use App\Models\Wallet;
use Illuminate\Support\Facades\Crypt;
class CheckWalletUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Verifica se está logado, se não tiver redireciona
        if ( !auth()->check() )
            return redirect()->route('login');

        /*
        * Verifica se o usuario tem carteiras criadas
        */
                    
        if (auth()->user()->load("wallets")->wallets->count()==0){
            try{
                $ws = new  WalletService();                        
                $btc = ($ws->getBTC()->client->getaccountaddress(auth()->user()->uuid)->result());
                $bch = ($ws->getBCH()->client->getaccountaddress(auth()->user()->uuid)->result());
                $ltc = ($ws->getLTC()->client->getaccountaddress(auth()->user()->uuid)->result());
                $eth = ($ws->getETH()->client->personal()->newAccount(auth()->user()->uuid)->toString());
                $coins = ["BTC"=>$btc,"ETH"=>$eth,"BCH"=>$bch,"LTC"=>$ltc,"XRP"=>1];            
                foreach ($coins as $key => $value){
                    if ($key!="XRP")
                        $wallet = Wallet::create([
                            'cryptocoin_id'=>Cryptocoin::where("symbol",$key)->first()->id,
                            'user_id'=>auth()->user()->id,
                            'address'=>$value,
                        ]);
                    else
                        $ws->getXRP()->saveWallet();
                }
            }catch(Exception $e){
                return $next($request);
            }
        }
        
        // Permite que continue (Caso não entre em nenhum dos if acima)...
        return $next($request);
    }
}
