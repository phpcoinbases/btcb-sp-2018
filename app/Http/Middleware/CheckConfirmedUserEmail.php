<?php

namespace App\Http\Middleware;

use Closure;

class CheckConfirmedUserEmail
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Verifica se está logado, se não tiver redireciona
        if ( !auth()->check() )
            return redirect()->route('login');

        /*
        * Verifica se o email foi confirmado
        */
        // Recupera o e-mail do usuário logado
        $confirmed = auth()->user()->confirmed;

        // Verifica se foi confirmado a conta do usuario
        if ( !$confirmed )
            return redirect('/confirmed');


        // Permite que continue (Caso não entre em nenhum dos if acima)...
        return $next($request);
    }
}
