<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        
        if (env("APP_ENV")=="dev"){
            return $next($request);    
        }
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized1.', 401);
            } else {
                return redirect()->route("admin.login");
            }
        } else if (!Auth::guard($guard)->user()->check_admin) {
            return response('Unauthorized3.', 401);
        }
        return $next($request);
    }
}
