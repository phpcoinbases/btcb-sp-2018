<?php

namespace App\Services;
use Illuminate\Http\Request;

use App\Models\Tax;

class TaxService{

    /**
     * Request  With last 10 values from BTC COIN
     * @return Object
     */
    public function get($type){
        return Tax::where("type",$type)->latest()->first();
    }

}
