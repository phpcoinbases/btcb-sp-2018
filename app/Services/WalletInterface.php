<?php

namespace App\Services;

use phpDocumentor\Reflection\Types\Boolean;


interface WalletInterface
{
    public function getWallet() ;
    public function getBalance();
    public function validateAddress($address);
    public function transfer($from,$to,$value);
}