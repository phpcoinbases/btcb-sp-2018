<?php

namespace App\Services;
use Illuminate\Http\Request;

use App\Models\Cryptocoin;

class CoinService{

    /**
     * Request  With last 10 values from BTC COIN
     * @return Object
     */
    public function getCoin($type,$limit){
        //LIMITAR AO MAXIMO DEFINIDO NO CONF DO SISTEMA
        if ($limit>config("values.api.coins_limit")){
            $limit = config("values.api.coins_limit");
        }
        return Cryptocoin::where("symbol",$type)
                                                ->with(['prices' => function ($query) use ($limit)  {
                                                    $query->take($limit);
                                                    $query->orderBy('created_at', 'desc');
                                                }])
                                                ->first();
    }

}
