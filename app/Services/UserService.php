<?php

namespace App\Services;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\UserInfo;

class UserService{


    public function type(){
        $dados = [];
        //CPF
        $dados["cpf_cnpj"] = auth()->user()->info()->first()->cpf_cnpj;
        //Comprovante Residencia
        $dados["document_file"] = auth()->user()->info()->first()->document_file;
        //Email Confirmado
        $dados["confirmed"] = auth()->user()->confirmed;
        //Documento
        $dados["comprovante"] = auth()->user()->info()->first()->comprovante;
        //Selfie
        $dados["perfil_file"] = auth()->user()->info()->first()->perfil_file;      
        //Origem Recursos
        $dados["origem"] = auth()->user()->info()->first()->origem;        
        //ata
        $dados["ata"] = auth()->user()->info()->first()->ata;

        return $dados;
    }


    public  function getType(){
        $lightx = false;
        $pro = false;  

        if (auth()->user()->info()->first()->document_file && auth()->user()->confirmed && auth()->user()->info()->first()->cpf_cnpj && auth()->user()->info()->first()->comprovante){
            $lightx = true;
        }
        if ($lightx  && auth()->user()->info()->first()->document_file && auth()->user()->info()->first()->ata && auth()->user()->info()->first()->ata && auth()->user()->info()->first()->document_file && auth()->user()->info()->first()->        $dados["origem"] = auth()->user()->info()->first()->perfil_file && auth()->user()->info()->first()->document_file && auth()->user()->info()->first()->origem){
            $pro = true;
        }

        if($pro){
            return "Pro";
        }else if($lightx){
            return "Light X";
        }else{
            return "Light";
        }
    }
}