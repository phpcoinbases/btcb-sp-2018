<?php

namespace App\Services;
use Illuminate\Http\Request;
use App\Models\Transaction;


class WalletService{

    public $client;

    public function getBTC(){
        return new Wallets\BitcoinWallet();
    }
    public function getETH(){
        return new Wallets\EthereumWallet();
    }
    public function getBCH(){
        return new Wallets\BCashWallet();
    }
    public function getLTC(){
        return new Wallets\LitecoinWallet();
    }

    public function getXRP(){
        return new Wallets\RippleWallet();
    }

    public function getBalance(){
        $balance = auth()->user()->deposits()->where("status","active")->sum("value");
        if (!empty(auth()->user()->withdraws()->get())){
            foreach(auth()->user()->withdraws()->get() as $obj){
                if($obj->status!="negative")
                    $balance -= $obj->value;
            }
        }   
        $trans = Transaction::where("seller_id",auth()->user()->id)->get();
        if (isset($trans)){
            foreach($trans as $obj){                
                $balance+=$obj->vendedor_vai_receber;
            }
        }
        if (!empty(auth()->user()->orders()->get())){
            foreach(auth()->user()->orders()->get() as $obj){
                
                if($obj->status!="negative" && $obj->type=="buy"){
                    $balance -= $obj->value*$obj->original;
                }
                    
            } 
        }     
        return $balance;
    }
}

?>