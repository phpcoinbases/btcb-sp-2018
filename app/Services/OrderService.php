<?php

namespace App\Services;
use Illuminate\Http\Request;

use App\Models\Cryptocoin;
use App\Models\Order;
use App\Models\Extract;
use App\Models\Sell;
use App\Models\Buy;
use App\Models\Tax;
use App\Models\Transaction;
use App\Models\Wallet;
use App\Services\WalletService;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;
use Carbon\Carbon;
class OrderService{

    private $wallet,$compras,$vendas,$ac_coin,$ordem,$coin,$ordens;
    public $type;

    public function __construct(){
        $this->wallet = new WalletService();
    }

    public function setOrder($orderId){
        $this->ordem = Order::find($orderId);        
        
        $this->coin = $this->ordem->coin->symbol;
        if ($this->ordem->type=="sell"){
            $this->ordens = $this->getOrderBuy($this->coin);
        }else{
            $this->ordens = $this->getOrderSell($this->coin);
        }
        if (count($this->ordens)==0){
            return false;
        }         
        $this->processaLista();
    }

    public function createOrder($obj){
        
        $ws = new WalletService();
        $valor = ($obj["value"]*$obj["qntd"]);
        if ($obj["type"]=="buy"){
            if ($ws->getBalance()<$valor){
                return (["error"=>'Você não tem fundos suficientes para realizar essa operacao']);
            } 
        }

        $coin = Cryptocoin::where("id",$obj["coin"])->first();
        if ($obj["type"]=="sell"){
            $symbol = "get".$coin->symbol;
            $balance = $ws->$symbol()->getBalance();
            if ($balance<$obj["qntd"]){
                return ["error"=>'Você não tem fundos suficientes para realizar essa operacao'];
            } 
        }
        if (!isset($obj["tax"])) {
             $tax = TaxUser::where("user_id", auth()->user()->id)->with("tx.taxes")->latest()->first();
             $obj["tax"] = $tax->tx->taxes->where("type","ative")->first()->id;
        }

        $obj["value"] = (number_format((float)$obj["value"],10,".",""));
        $obj["qntd"]  = (number_format((float)$obj["qntd"],10,".",""));
        return $this->create($obj);
    }

    private function create($obj){
        try{
            auth()->user()->orders()->create([
                "value"=>$obj["value"],
                "qntd"=>$obj["qntd"],
                "original"=>$obj["qntd"],
                "type"=>$obj["type"],
                "status"=>"pending",
                "tax_id"=>$obj["tax"],
                "cryptocoin_id"=>$obj["coin"]
            ]);
            return auth()->user()->orders()->latest()->first();    
        }catch(Exception $e){
            return ["error"=>"Ocorreu um erro ao gravar a ordem"];
        }
    }

    public function delete($obj){        
        $order = Order::where("uuid",$obj["uuid"])->where("user_id",auth()->user()->id)->first();
        if (Transaction::where("id_ordem_venda",$order->id)->whereOr("id_ordem_compra",$order->id)->count()){
            return ["error"=>"Essa ordem já gerou uma transação"];
        }
        if(!isset($order))
            return ["error"=>"Não foi possivel excluir essa Ordem (Codigo incorreto ou Essa ordem nao pertence ao usuario)"];
        else    
            return $order->delete();
    }

    public function getOrderBuy($coin){
        
        return (Order::where("type","buy")
                                    ->whereHas('coin' , function ($query) use ($coin) {
                                        $query->where('symbol', $coin);
                                    })
                                    ->with("tax")
                                    //->others()
                                    ->where("qntd",">",0)
                                    ->where("status","pending")
                                    ->whereOr("status","active")
                                    ->orderBy('id', 'ASC')
                                    ->get());
    }

    public function getOrderSell($coin){                
        return (Order::where("type","sell")
                                    ->whereHas('coin' , function ($query) use ($coin) {
                                        $query->where('symbol', $coin);
                                    })
                                    ->with("tax")
                                    //->others()
                                    ->where("qntd",">",0)
                                    ->where("status","pending")
                                    ->whereOr("status","active")
                                    ->orderBy('id', 'ASC')
                                    ->get());
    }

    private function processaLista(){
        
        foreach($this->ordens as $obj){
            // Start transaction!
            
            //SAME USER CONTINUE
            if ($obj->user_id==$this->ordem->user_id){ 
              //  continue;
            }        
            //ZEROU A QUANTIDADE
            if ($obj->qntd<=0 || $this->ordem->qntd<=0){
                continue;
            }
            if ($this->ordem->type=="sell"){
                $sell   = $this->ordem;
                $buyobj = $obj;                                
            }else{
                $buyobj         = $this->ordem;
                $sell           = $obj;                
            }
            if ($sell->value > $buyobj->value){
                continue;
            }            
                                    
            $valueFinal     = $sell->value; 
            //POSSUE ORDEM PARA REALIZAR
            //venda maior que a compra   
                $nowInSP = Carbon::now('America/Sao_Paulo');         
                $obj = [
                    "vendendo"=>$sell->qntd,
                    "comprando"=>$buyobj->qntd,
                    "taxa_compra"=>$buyobj->tax->our_tax,
                    "taxa_venda"=>$sell->tax->our_tax,
                    "origem_qntd_sell"=>$sell->original,
                    "origem_qntd_buy"=>$buyobj->original,
                    "vendedor_vai_receber"=>(($buyobj->qntd - $buyobj->tax->our_tax)*$valueFinal),
                    "comprador_vai_receber"=>($buyobj->qntd) - $buyobj->tax->our_tax,
                    "valor_venda"=>$sell->value,
                    "valor_compra"=>$buyobj->value,
                    "id_ordem_compra"=>$buyobj->id,
                    "id_ordem_venda"=>$sell->id,
                    "seller_id"=>$sell->user->id,
                    "buyer_id"=>$buyobj->user->id,
                    "type_order"=>$this->ordem->type,
                ];
                

                if ($sell->qntd>$buyobj->qntd){
                    $obj["value_coin"] = $buyobj->qntd;
                    $obj["value_money"] = $buyobj->qntd * $sell->value;
                    $sell->qntd = $sell->qntd-$buyobj->qntd;
                    $buyobj->qntd=0;
                    $buyobj->status = "final";
                    $obj["soubrou_vendedor"]= $sell->qntd;
                    $obj["soubrou_comprador"]= 0;
                    
                }elseif ($sell->qntd<$buyobj->qntd){           
                    $obj["value_coin"] = $sell->qntd;
                    $obj["value_money"] = $sell->qntd * $sell->value;
                    $buyobj->qntd = $buyobj->qntd-$sell->qntd;
                    $sell->qntd=0;
                    $sell->status="final";
                    $obj["soubrou_vendedor"]= 0;
                    $obj["soubrou_comprador"]= $buyobj->qntd;
                }else{
                    $obj["value_coin"] = $sell->qntd;
                    $obj["value_money"] = $sell->qntd * $sell->value;                    
                    $sell->qntd=0;
                    $buyobj->qntd = 0;
                    $sell->status="final";
                    $buyobj->status = "final";
                    $obj["soubrou_vendedor"]= 0;
                    $obj["soubrou_comprador"]= 0;
                }
                //TRANSFERIR MOEDAS
                $seller_address = ($this->getWalletAddress($sell->user->id));
                $buyer_address = ($this->getWalletAddress($buyobj->user->id));
                //TRANSFERIR PARA AS CONTAS DO ADMIN AS TAXAS TODO
                if ($this->ac_coin=="ETH"){
                    //$tx = $this->wallet->getETH()->transfer($seller_address,$buyer_address,$obj["comprador_vai_receber"]);
                }elseif ($this->ac_coin=="BTC"){
                    $tx = $this->wallet->getBTC()->transfer($seller_address,$buyer_address,$obj["comprador_vai_receber"]);
                }elseif ($this->ac_coin=="BCH"){
                    $tx = $this->wallet->getBCH()->transfer($seller_address,$buyer_address,$obj["comprador_vai_receber"]);
                }elseif ($this->ac_coin=="LTC"){
                    $tx = $this->wallet->getLTC()->transfer($seller_address,$buyer_address,$obj["comprador_vai_receber"]);
                }elseif ($this->ac_coin=="XPR"){
                    //$tx = $this->wallet->getXPR()->transfer($seller_address,$buyer_address,$obj["comprador_vai_receber"]);
                }      
                          
                $sell->save();
                $buyobj->save();
                $transation = Transaction::create($obj);   
        }
    }

    public function getWalletAddress($userid){
        $coin = (Cryptocoin::where("symbol",$this->ac_coin)->first()->id);
        return Wallet::where("user_id",$userid)->where("cryptocoin_id",$coin)->first()->address;    
    }

}