<?php

namespace App\Services\Wallets;

use Illuminate\Http\Request;
use Denpa\Bitcoin\Client as BitcoinClient;
use App\Models\Cryptocoin;
use App\Models\Order;
use App\Models\Transaction;
use App\Models\Wallet;
use App\Services\WalletInterface;
use PhpParser\Node\Stmt\TryCatch;
use phpDocumentor\Reflection\Types\This;
use GuzzleHttp\Ring\Exception\ConnectException;
class BitcoinWallet extends BTCBasic implements WalletInterface{

    public $client;

    public function __construct()
    {    
        $this->client = new BitcoinClient('http://'.env('RPCUSER').':'.env("RPCPASSWORD").'@'.env("RPCHOST").':'.env("RPCPORT").'/');
        $this->coinSymbol = "BTC";            
    }
   
}