<?php

namespace App\Services\Wallets;

use Illuminate\Http\Request;
use Denpa\Bitcoin\Client as BitcoinClient;
use App\Models\Cryptocoin;
use App\Models\Order;
use App\Models\Transaction;
use App\Models\Wallet;
use App\Services\WalletInterface;

class BCashWallet extends BTCBasic implements WalletInterface{

    public $client;

    public function __construct()
    {
        $this->client = new BitcoinClient('http://'.env('RPCUSERBCH').':'.env("RPCPASSWORDBCH").'@'.env("RPCHOSTBCH").':'.env("RPCPORTBCH").'/');        
        $this->coinSymbol = "BCH";
    }

}