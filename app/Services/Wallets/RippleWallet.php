<?php

namespace App\Services\Wallets;

use Illuminate\Http\Request;
use Denpa\Bitcoin\Client as BitcoinClient;
use App\Models\Cryptocoin;
use App\Models\Order;
use App\Models\Transaction;
use App\Models\Wallet;
use App\Services\WalletInterface;
use App\Services\Wallets\RippleTopRpcClient;
use Illuminate\Support\Facades\Crypt;

class RippleWallet implements WalletInterface{

    public $client;
    public function __construct()
    {
        $this->client = new RippleTopRpcClient(env("RIPPLE_SERVER"));
        $this->coinSymbol = "XRP";        
    }

    public function newAddress(){
        return $this->client->wallet_propose();
    }

    public function saveWallet(){
        $obj = $this->newAddress();
        $master = Crypt::encrypt($obj["master_key"]);
        $seed   = Crypt::encrypt($obj["master_seed"]);
        $public = Crypt::encrypt($obj["public_key"]);
        $wallet = Wallet::create([
            'cryptocoin_id'=>Cryptocoin::where("symbol","XRP")->first()->id,
            'user_id'=>auth()->user()->id,
            'address'=>$obj["account_id"],
            'public'=>$public,
            'seed'=>$seed,
            'master'=>$master
        ]);
    }

    public function getWallet()
    {
        $btc = (Cryptocoin::where("symbol",$this->coinSymbol)->first()->id);
        return Wallet::where("user_id",auth()->user()->id)->where("cryptocoin_id",$btc)->first()->address;        
    }

    public function getBalance()
    {   
        $balance = 0;
        $balance = $this->client->account_info(["account"=>$this->getWallet()])["account_data"]["Balance"];
        if (empty($balance)){
            return "0 (N.A)";
        }else{
            $balance /= 1000000; 
        }
        if (!empty(auth()->user()->orders()->where('type', "sell")->where("cryptocoin_id",Cryptocoin::where("symbol",$this->coinSymbol)->first()->id)->get())){
            foreach(auth()->user()->orders()->where('type', "sell")->where("cryptocoin_id",Cryptocoin::where("symbol",$this->coinSymbol)->first()->id)->get() as $obj){
                $balance -= $obj->qntd;
            }            
        }
        
        return $balance;
    }

    public function validateAddress($address)
    {
        return true;
    }

    public function transfer($from,$to,$value)
    {
        return $this->client->move($from,$to,$value)->result();
    }
}