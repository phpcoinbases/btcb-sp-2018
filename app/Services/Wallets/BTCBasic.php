<?php

namespace App\Services\Wallets;

use Illuminate\Http\Request;
use Denpa\Bitcoin\Client as BitcoinClient;
use App\Models\Cryptocoin;
use App\Models\Order;
use App\Models\Transaction;
use App\Models\Wallet;
use App\Services\WalletInterface;

class BTCBasic{

    public function getWallet()
    {
        $btc = (Cryptocoin::where("symbol",$this->coinSymbol)->first()->id);
        return Wallet::where("user_id",auth()->user()->id)->where("cryptocoin_id",$btc)->first()->address;        
    }

    public function getUnconfirmed(){
        $balance=0;
        //nao confirmadas
        $key =$this->client->getaccount($this->getWallet())->get();
        $unconfirmed = $this->client->listtransactions($key)->get();        
        if (!empty($unconfirmed) && count($unconfirmed)>0){
            foreach($unconfirmed as $chave => $value){
                if ($value["category"]=="receive" && $value["confirmations"]<6) {
                    $balance += $value["amount"];
                }
            }
        }
        return $balance;
    }

    public function getBalance()
    {
        //confirmadas
        $balance = $this->client->getbalance(auth()->user()->uuid)->get();
        
        if (!empty(auth()->user()->withdraws()->get())){
            foreach(auth()->user()->withdraws()->get() as $obj){
                if($obj->status!="negative")
                    $balance -= $obj->valuebtc;
            }
        }
     
        if (!empty(auth()->user()->orders()->where('type', "sell")->where("cryptocoin_id",Cryptocoin::where("symbol",$this->coinSymbol)->first()->id)->get())){
            foreach(auth()->user()->orders()->where('type', "sell")->where("cryptocoin_id",Cryptocoin::where("symbol",$this->coinSymbol)->first()->id)->get() as $obj){
                $balance -= $obj->qntd;
            }            
        }         
        
        return $balance;
    }

    public function validateAddress($address)
    {
        return $this->client->validateaddress($address)->get()["isvalid"];
    }

    public function transfer($from,$to,$value)
    {
        return $this->client->move($from,$to,$value)->result();
    }

    public function withdraw($from,$to,$value)
    {
        return $this->client->sendfrom($from,$to,$value)->result();
    }

}