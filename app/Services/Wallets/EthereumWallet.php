<?php

namespace App\Services\Wallets;

use Illuminate\Http\Request;
use EthereumPHP\EthereumClient;
use EthereumPHP\Types\BlockHash;
use EthereumPHP\Types\BlockNumber;
use EthereumPHP\Types\Address;
use EthereumPHP\Types\Transaction as ETHTransaction;
use EthereumPHP\Types\Ether;
use App\Models\Cryptocoin;
use App\Models\Order;
use App\Models\Transaction;
use App\Models\Wallet;
use App\Services\WalletInterface;

class EthereumWallet implements WalletInterface{

    public $client;

    public function __construct()
    {
        $this->client = new EthereumClient('http://'.env('ETHHOST').':'.env("ETHPORT"));   
        $this->coinSymbol = "ETH";
    }

    public function getWallet()
    {
        $eth = (Cryptocoin::where("symbol",$this->coinSymbol)->first()->id);
        $ethAddress = Wallet::where("user_id",auth()->user()->id)->where("cryptocoin_id",$eth)->first()->address;
        return $ethAddress;      
    }

    public function getBalance()
    {
        $Address = new Address($this->getWallet());

        $ethcoin = $this->client->eth()->getBalance($Address,new BlockNumber())->toEther();
        $GasPrice = $this->client->eth()->gasPrice()->toEther();
        if (!empty(auth()->user()->withdraws()->get())){
            foreach(auth()->user()->withdraws()->get() as $obj){
                if($obj->status!="negative")
                    $ethcoin -= $obj->valueeth;
            }
        }

        if (!empty(auth()->user()->orders()->where('type', "sell")->where("cryptocoin_id",Cryptocoin::where("symbol",$this->coinSymbol)->first()->id)->get())){
            foreach(auth()->user()->orders()->where('type', "sell")->where("cryptocoin_id",Cryptocoin::where("symbol",$this->coinSymbol)->first()->id)->get() as $obj){
                $ethcoin -= $obj->qntd;
            }            
        }

        return ($ethcoin);
    }

    public function validateAddress($address)
    {
        if (!preg_match('/^(0x)?[0-9a-f]{40}$/i',$address)) {
            // check if it has the basic requirements of an address
            return false;
        } elseif (!preg_match('/^(0x)?[0-9a-f]{40}$/',$address) || preg_match('/^(0x)?[0-9A-F]{40}$/',$address)) {
            // If it's all small caps or all all caps, return true
            return true;
        } else {
           return true;
        }
    }

    public function transfer($from,$to,$value)
    {
        $this->eth->personal()->unlockAccount(new Address($from),Wallet::where("address",$from)->with("user")->first()->user->email, 20);
             
        $transaction = new ETHTransaction(
            new Address($from),
            new Address($to),
            null,
            null,
            $this->client->eth()->gasPrice(),
            (new \EthereumPHP\Types\Ether($value))->toWei()->amount()
        );
        try{
            return $this->client->eth()->sendTransaction($transaction)->toString();
        }catch(Exception $e){
            return "";
        }
    }
  
    public function transferBase($to,$value){

        $this->client->personal()->unlockAccount(new Address(env("ETHCOINBASE")),env("ETHCOINPASS"), 20);
       // $this->eth->personal()->unlockAccount(new Address($to), Wallet::where("address",$to)->with("user")->first()->user->email, 20);        
                
        $transaction = new ETHTransaction(
            new Address(env("ETHCOINBASE")),
            new Address($to),
            null,
            null,
            $this->client->eth()->gasPrice(),
            (new \EthereumPHP\Types\Ether(1))->toWei()->amount()
        );

        $coinbalance = ($this->client->eth()->getBalance(new Address(env("ETHCOINBASE")),new BlockNumber())->amount());
        if ($coinbalance>(new \EthereumPHP\Types\Ether(1))->toWei()->amount()) {
            try {
                return $this->client->eth()->sendTransaction($transaction)->toString();
            } catch (Exception $e) {
                return "";
            }
        }
        return "";

    }

}