<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\OrderService;

class OrderTransaction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:transaction';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'A cada 1minuto faz a atualizacao das transacoes';


    protected $order;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(OrderService $dt)
    {
        parent::__construct();
        $this->order = $dt;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Verificando transacao BTC');
        $this->order->verify("BTC");
        $this->order->verify("ETH");
        $this->order->verify("LTC");
        $this->order->verify("BCH");
    }
}
