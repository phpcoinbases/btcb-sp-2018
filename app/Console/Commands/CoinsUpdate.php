<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client as Client;
use App\Models\Cryptocoin;
use App\Models\Price;
use App\Models\Coin;
use App\Models\Exchange as ExchangeModel;
use Carbon\Carbon;
use ccxt\Exchange;
class CoinsUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coins:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Atualiza a base de valores a cada 20minutos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $client = new Client();
        $res = $client->request('GET', 'https://api.coinmarketcap.com/v1/ticker/?convert=BRL');
        if ($res->getStatusCode()==200){
            foreach(json_decode($res->getBody()) as $value){                                    
                if (!Cryptocoin::where("uuid",$value->id)->count()){
                    $model = new Cryptocoin();
                    $model->uuid = $value->id;
                    $model->name = $value->name;
                    $model->symbol = $value->symbol;
                    $model->save();
                }
                
                //Gravar as Stocks
                $coin = Cryptocoin::where("uuid",$value->id)->first();
                if (!$coin->prices()->where("last_updated",$value->last_updated)->count()){                    
                    $price = new Price([
                        "price_usd" => (float)$value->price_usd,
                        "price_btc" => (float)$value->price_btc,                        
                        "market_cap_usd" => (float)$value->market_cap_usd,
                        "volume_usd" => (float) $value->{"24h_volume_usd"},
                        "available_supply" => (float)$value->available_supply,
                        "total_supply" =>(float) $value->total_supply,
                        "max_supply" =>(float) $value->max_supply,
                        "percent_change_1h" => (float)$value->percent_change_1h,
                        "percent_change_24h" =>(float) $value->percent_change_24h,
                        "percent_change_7d" => (float)$value->percent_change_7d,
                        "last_updated" =>$value->last_updated,
                        "price_brl" =>(float) $value->price_brl,
                        "volume_brl" => (float)$value->{"24h_volume_brl"},
                        "market_cap_brl" => (float)$value->market_cap_brl,
                    ]);
                    $coin->prices()->save($price);
                }     
            }
        }    

        $this->atualizaCambio("USD");
        $this->atualizaCambio("EUR");
    }

    public function atualizaCambio($moeda)
    {
        $hoje = Carbon::now()->format("m-d-Y");
        $client = new Client();
        $res = $client->request('GET', 'https://olinda.bcb.gov.br/olinda/servico/PTAX/versao/v1/odata/CotacaoMoedaPeriodo(moeda=@moeda,dataInicial=@dataInicial,dataFinalCotacao=@dataFinalCotacao)?%40moeda=%27'.$moeda.'%27&%40dataInicial=%27'.$hoje.'%27&%40dataFinalCotacao=%27'.$hoje.'%27&%24format=json');
        $coin = Coin::where("symbol",$moeda)->first();
        if ($res->getStatusCode()==200){
            foreach(json_decode($res->getBody())->value as $value){    
                //Gravar as Stocks
                $dataFormat = (Carbon::createFromFormat("Y-m-d H:i:s.u",$value->dataHoraCotacao)->format("Y-m-d H:i:s"));
                if (ExchangeModel::where("coin_id", $coin->id)->where("time", $dataFormat)->count()==0) {
                    $exchange = ExchangeModel::create([
                        "buy"=>$value->cotacaoCompra,
                        "sell"=>$value->cotacaoVenda,
                        "time"=>$dataFormat,
                    ]);
                    
                    $coin->exchanges()->save($exchange);
                }
            }
        }       
    }
}
