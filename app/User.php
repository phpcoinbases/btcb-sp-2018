<?php

namespace App;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Webpatser\Uuid\Uuid;
class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use HasApiTokens;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','confirmation_code','confirmed',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','confirmation_code','confirmed',
    ];

    /** 
     * 2Factory Security Google
     * @var Model PasswordSecurity
    */

    public function passwordSecurity()
    {
        return $this->hasOne('App\Models\PasswordSecurity');
    }

    /**
     * Get the depositos.
     * @return array[App\Deposit]
     */
    public function deposits()
    {
        return $this->HasMany('App\Models\Deposit');
    }

    /**
     * Get the saques.
     * @return array[App\Withdraw]
     */
    public function withdraws()
    {
        return $this->HasMany('App\Models\Withdraw');
    }
    /**
     * Get the extrato.
     * @return array[App\Order]
     */
    public function orders()
    {
        return $this->HasMany('App\Models\Order');
    }

    /**
     * Get the extrato.
     * @return array[App\Extract]
     */
    public function extracts()
    {
        return $this->HasMany('App\Models\Extract');
    }
    /**
     * Get the prices.
     * @return array[App\Wallet]
     */
    public function wallets()
    {
        return $this->HasMany('App\Models\Wallet');
    }
    /**
     * Get the prices.
     * @return array[App\Tax]
     */
    public function taxs()
    {
        return $this->HasMany('App\Models\TaxUser');
    }    
   /** 
     * User complementos
     * @var Model UserInfo
    */

    public function info()
    {
        return $this->hasOne('App\Models\UserInfo');
    }

    public function scopeActive($query)
    {
        return $query->where('confirmed', 1);
    }


    public function get_limit(){
        $limit = 0;        
        if ( auth()->check() )
        {
            $limit = 1000;
            //Email Confirmado
            if (auth()->user()->confirmed){
                $limit += 2000;
            }  
            //2FA GOOGLE ATIVO 
            if (auth()->user()->passwordSecurity->google2fa_enable){
                $limit += 3000;
            }
            //Preencheu todo cadastro            
            if (auth()->user()->info()->count()){
                $limit += 4000;
            }
        }

        return $limit;
    }

    /**
     *  Setup model event hooks
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string) Uuid::generate()->string;
        });
    }
}
