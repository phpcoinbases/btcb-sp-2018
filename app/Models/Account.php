<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{

    /**
     * Get the cryptocoin.
     * @return App\Bank
     */
    public function bank()
    {
        return $this->belongsTo('App\Models\Bank');
    }
}
