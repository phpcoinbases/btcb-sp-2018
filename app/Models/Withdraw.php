<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;
class Withdraw extends Model
{
    public $guarded = [];
    
    
    /**
     * Get the User.
     * @return App\User
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }


    /**
     *  Setup model event hooks
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string) Uuid::generate()->string;
        });
    }
}
