<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Webpatser\Uuid\Uuid;
class Deposit extends Model implements HasMedia
{

    use HasMediaTrait;

    public $fillable = ["user_id","account_id","wallet_id","value","type","status","comprovante"];


   /**
     * Get the cryptocoin.
     * @return App\User
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the cryptocoin.
     * @return App\Account
     */
    public function account()
    {
        return $this->belongsTo('App\Models\Account');
    }

    /**
     * Get the cryptocoin.
     * @return App\Wallet
     */
    public function wallet()
    {
        return $this->belongsTo('App\Models\Wallet');
    }

    /**
     *  Setup model event hooks
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string) Uuid::generate()->string;
        });
    }
}
