<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cryptocoin extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid', 'name','symbol',
    ];

    /**
     * Get the prices.
     * @return array[App\Price]
     */
    public function prices()
    {
        return $this->HasMany('App\Models\Price');
    }

    /**
     * Get the orders.
     * @return array[App\Order]
     */
    public function orders()
    {
        return $this->HasMany('App\Models\Order');
    }

    /**
     * Get the wallets.
     * @return array[App\Wallet]
     */
    public function wallets()
    {
        return $this->belongsToMany('App\Models\Wallet');
    }


}
