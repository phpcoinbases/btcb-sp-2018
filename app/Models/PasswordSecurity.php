<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PasswordSecurity extends Model
{
 
    protected $guarded = [];
 

    /** 
     * 2Factory Security Google
     * @var Model User
    */

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}