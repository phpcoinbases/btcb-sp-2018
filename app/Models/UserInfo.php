<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserInfo extends Model
{
    public $table="users_info";
    public $fillable = [
        "perfil_file","phone","address","number","city","uf","country",
        "document_file","cep","complemento","uuid","comprovante"
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    

}
