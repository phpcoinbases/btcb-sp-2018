<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;
class Transaction extends Model
{
    protected $guarded = [];

    public function ordem_vendedor(){
        return $this->belongsTo("App\Models\Order","id_ordem_venda");
    }

    public function ordem_comprador(){
        return $this->belongsTo("App\Models\Order","id_ordem_compra");
    }

    public function comprador(){
        return $this->belongsTo("App\User","buyer_id");
    }

    public function vendedor(){
        return $this->belongsTo("App\User","seller_id");
    }

    /**
     *  Setup model event hooks
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string) Uuid::generate()->string;
        });
    }

}
