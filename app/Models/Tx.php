<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tx extends Model
{
   protected $guarded = [];


       /**
     * Get the taxes.
     * @return App\Tax
     */
    public function taxes()
    {
        return $this->hasMany('App\Models\Tax');
    }

}
