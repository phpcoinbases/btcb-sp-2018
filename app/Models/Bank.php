<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    /**
     * Get the prices.
     * @return array[App\Models\Price]
     */
    public function accounts()
    {
        return $this->HasMany('App\Models\Account');
    }
}
