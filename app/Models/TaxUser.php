<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaxUser extends Model
{
    protected $guarded = [];

    public function tx(){
        return $this->belongsTo("App\Models\Tx","tx_id");
    }

    public function user(){
        return $this->belongsTo("App\User","user_id");
    }

}
