<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;
class Order extends Model
{
    public $fillable = ["user_id","value","original","type","qntd","status","tax_id","cryptocoin_id","uuid","type_order"];
    /**
     * Get the User.
     * @return App\User
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }


    public function scopeOthers($query){
        return $query->where("user_id","!=",auth()->user()->id);
    }
    /**
     * Get the Tax.
     * @return App\Tax
     */
    public function tax()
    {
        return $this->belongsTo('App\Models\Tax',"tax_id");
    }
    /**
     * Get the Cryptocoin.
     * @return App\Cryptocoin
     */
    public function coin()
    {
        return $this->belongsTo('App\Models\Cryptocoin',"cryptocoin_id");
    }
    /**
     * Get the Extract.
     * @return App\Extract
     */
    public function extrato()
    {
        return $this->HasMany('App\Models\Extract');
    }    


    /**
     *  Setup model event hooks
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string) Uuid::generate()->string;
        });
    }

}
