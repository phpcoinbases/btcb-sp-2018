<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exchange extends Model
{
    protected $guarded = [];
    public function coin()
    {
        return $this->belongsTo('App\Models\Coin');
    }    
}
