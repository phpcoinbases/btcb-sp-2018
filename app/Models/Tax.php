<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
    public $fillable = ["our_tax","fixed","type"];


    public function getOurTax(){
        return $this->our_tax / 1000;
    }

    public function tx()
    {
        return $this->belongsTo('App\Models\Tx');
    }

}
