<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Extract extends Model
{
    public $fillable = ["type","value","comprovante","tax_id","deposit_id","user_id","order_id","cryptocoin_id","wallet_id","withdraw_id"];
   /**
     * Get the User.
     * @return App\User
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the Tax.
     * @return App\Tax
     */
    public function tax()
    {
        return $this->belongsTo('App\Models\Tax');
    }

    /**
     * Get the Wallet.
     * @return App\Wallet
     */
    public function wallet()
    {
        return $this->belongsTo('App\Models\Wallet');
    }

    /**
     * Get the cryptocoin.
     * @return App\Cryptocoin
     */
    public function cryptocoin()
    {
        return $this->belongsTo('App\Models\Cryptocoin');
    }

    /**
     * Get the Deposit.
     * @return App\Deposit
     */
    public function deposit()
    {
        return $this->belongsTo('App\Models\Deposit');
    }    

    /**
     * Get the Orders.
     * @return App\Order
     */
    public function orders()
    {
        return $this->belongsTo('App\Models\Order',"order_id");
    }

        /**
     * Get the Withdraw.
     * @return App\Withdraw
     */
    public function withdraw()
    {
        return $this->belongsTo('App\Models\Withdraw');
    }    
}
