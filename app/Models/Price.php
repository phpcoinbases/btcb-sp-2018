<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'price_usd', 'price_btc','volume_usd',
        'market_cap_usd', 'available_supply','total_supply',
        'percent_change_1h', 'percent_change_24h','percent_change_7d',
        'price_brl', 'volume_brl','market_cap_brl','last_updated',
    ];

    /**
     * Get the cryptocoin.
     * @return App\Cryptocoin
     */
    public function coin()
    {
        return $this->belongsTo('App\Models\Cryptocoin');
    }

}
