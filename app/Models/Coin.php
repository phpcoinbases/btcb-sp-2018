<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coin extends Model
{
    protected $guarded = [];

    public function exchanges()
    {
        return $this->hasMany('App\Models\Exchange');
    }    

}
