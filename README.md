## TODO
- [x] Criar o projeto na base do Laravel
- [x] Instanciar as classes de BTC e ETH
- [x] Instanciar a estrutura do Front com VueJs e Vue-Router
- [x] Instanciar a CCXT Package
- [x] Instanciar o 2 Factory Auth com Google
- [x] Instaciar as Schedules
- [x] Criar evento para ler os valores das Cryptomoedas
- [x] Ajustar o FAQ com Lorem Ipsum
- [x] Modelar as migrations do banco Cryptocoins e Prices stocks
- [x] Chamada Guzzler para a API
- [x] Adicionado a classe para tratamento de Moedas - cknow/laravel-money
- [x] Adicionado o Twilio para futura integracao com SMS
- [x] Login via Socialite Package
- [x] Adicionar o segundo passo no cadastro do usuario... (Endereço, documentos) validando a conta
- [x] Incluir o layout
- [x] Gerar ordem de compra
- [x] Gerar ordem de venda
- [x] Eventos de frequencia
- [ ] Inclusão dos testes de unit/featured/browser 

## License
This is not open source project.

Sentry
https://sentry.io/coinbold/php-coinbold/dashboard/
