<?php

/**
 * ROUTES GROUP 
 * URL : ADMIN SYS
 */
//Route::domain('admin.'.env("APP_SHORTURL"))->middleware('web')->group(function () {
    Route::get('/sys/login', 'Admin\AdminController@login')->name("admin.login");
    Route::post('/sys/entrar', 'Admin\AdminController@postLogin')->name("admin-entrar");
    Route::get('/sys/dashboard', 'Admin\AdminController@index')->name("admin.index");     
    Route::get('/sys/landing-precadastro', 'Admin\AdminController@landingCadastro')->name("admin.landing.cadastro");     
    Route::get('/sys/landing-contato', 'Admin\AdminController@landingContato')->name("admin.landing.contato");     
    Route::get('/sys/clientes', 'Admin\AdminController@clientes')->name("admin.clientes");
    
    Route::get('/sys/news', 'Admin\AdminController@news')->name("admin.news");     
    Route::post('/sys/save-news', 'Admin\AdminController@saveNews')->name("admin.save-news");     
    Route::get('/sys/delete-news/{id}', 'Admin\AdminController@deleteNews')->name("admin.news.delete");     

    Route::get('/sys/wallets', 'Admin\AdminController@wallets')->name("admin.wallets");     
    Route::post('/sys/wallets', 'Admin\AdminController@wallets')->name("admin.save.wallets");     

    Route::get('/sys/deposits', 'Admin\AdminController@deposits')->name("admin.deposits");

    Route::post('/sys/deposits/accept', 'Admin\AdminController@deposits_accept')->name("admin.deposits.accept");
    Route::post('/sys/deposits/denied', 'Admin\AdminController@deposits_denied')->name("admin.deposits.denied");

    Route::get('/sys/saques', 'Admin\AdminController@saques')->name("admin.saques");
    Route::get('/sys/orders', 'Admin\AdminController@orders')->name("admin.ordens");
    Route::get('/sys/transacoes', 'Admin\AdminController@transacoes')->name("admin.transacoes");
   


    Route::get('/sys/taxes', 'Admin\AdminController@tx')->name("admin.taxas");     
    Route::get('/sys/tx/{id}/taxas', 'Admin\AdminController@txTaxas')->name("admin.tx.taxas");     
    Route::post('/sys/tx', 'Admin\AdminController@saveTx')->name("admin.tx.save");     
    Route::post('/sys/clients/taxa', 'Admin\AdminController@saveTxUser')->name("admin.user.taxas.save");     
    Route::post('/sys/tx/{id}/taxas', 'Admin\AdminController@saveTaxas')->name("admin.tx.taxas.save");         
    Route::delete('/sys/tx/{id}', 'Admin\AdminController@deleteTx')->name("admin.tx.delete");     

//});