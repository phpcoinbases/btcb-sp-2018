<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
    // Ignores notices and reports all other kinds... and warnings
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
    // error_reporting(E_ALL ^ E_WARNING); // Maybe this is enough
}
/**
 * Routes
 * Landing Page
 */


Route::get('/', function(Request $request){
    return redirect()->route("dashboard");
});
/*Route::get('/quem-somos', 'HomeController@landingQuemPage')->name('landing.quem');
Route::get('/termos', 'HomeController@landingTermosPage')->name('landing.termos');
Route::post('/saveuser', 'HomeController@saveUser')->name('landing.save.user');
Route::post('/savecontact', 'HomeController@saveContact')->name('landing.save.contact');
Route::get('/getcoins', 'HomeController@returnCoins')->name('landing.get.coins');
Route::get('/getBTC', 'HomeController@returnCoinsBTC')->name('landing.get.bitcoin');
*/




//SEO ====================================================
Route::get('robots.txt', 'SeoController@robots');
Route::get('sitemap.xml', 'SeoController@sitemap');


/**
 * Routes to Login
**/

Auth::routes();
Route::get('/verifica/email/{confirmation_code}', 'Auth\PasswordSecurityController@confirm')->name('confirmation_code');
Route::get('/logout/',function(){ Auth::logout(); return redirect()->to("login");});
Route::get('/login/{social}','Auth\LoginController@socialLogin')->where('social','instagram|live|twitter|facebook|linkedin|google|github|bitbucket');
Route::get('/login/{social}/callback','Auth\LoginController@handleProviderCallback')->where('social','instagram|live|twitter|facebook|linkedin|google|github|bitbucket');
Route::get("/confirmed",function(Request $request){  return view("auth.confirmed"); });

/**
 * Routes to Google Authenticator
**/
Route::get('/2fa','Auth\PasswordSecurityController@show2faForm')->name('googlefa');
Route::post('/generate2faSecret','Auth\PasswordSecurityController@generate2faSecret')->name('generate2faSecret');
Route::post('/2fa','Auth\PasswordSecurityController@enable2fa')->name('enable2fa');
Route::post('/disable2fa','Auth\PasswordSecurityController@disable2fa')->name('disable2fa');
Route::post('/2faVerify', function () {return redirect(URL()->previous());})->name('2faVerify')->middleware('2fa');

Route::get('check-session', 'Auth\LoginController@checkSession');

/**
 * 
 * ROUTES TO AUTHORIZE DEVICE
 */

Route::group(['middleware' => ['auth']], function () {
    Route::get('/authorize/{token}', [
        'name' => 'Authorize Login',
        'as' => 'authorize.device',
        'uses' => 'Auth\AuthorizeController@verify',
    ]);
    Route::post('/authorize/resend', [
        'name' => 'Authorize',
        'as' => 'authorize.resend',
        'uses' => 'Auth\AuthorizeController@resend',
    ]);
});


Route::get('/withdraw/coin/mail/uuid/{uuid}', 'ActionController@liberar_saque')->name('withdraw.authorize');

Route::group(['middleware' => ['auth',/*"checkconfirmed","authorize",*/"2fa",'checkwallets']], function () {
    
    Route::get('/json/get/wallets', 'Dashboard\WalletController@getWallets')->name('json.get.wallets');

    Route::get('/dashboard', 'Dashboard\DashboardController@index')->name('dashboard');

    Route::get('/dashboard/api', 'Dashboard\DashboardController@api')->name('dashboard.api');
    Route::post('/dashboard/generate', 'Dashboard\DashboardController@generateApi')->name('generate.access');

    Route::get('/perfil', 'Dashboard\AccountController@perfil')->name('perfil');
    Route::get('/account/type', 'Dashboard\AccountController@type')->name('account.type');

    Route::post('/account/perfil', 'Dashboard\AccountController@save')->name('perfil.upload');
    Route::get('/account/perfil/delete/{type}', 'Dashboard\AccountController@delete')->name('perfil.delete');
    
    Route::get('/deposit', 'Dashboard\DepositController@index')->name('deposit');
    Route::post('/deposit/upload', 'Dashboard\DepositController@saveDeposit')->name('deposit.upload');

    Route::get('/withdraw', 'Dashboard\WithdrawController@index')->name('withdraw');
    Route::post('/withdraw/money', 'Dashboard\WithdrawController@money')->name('withdraw.money');
    Route::post('/withdraw/coin', 'Dashboard\WithdrawController@coin')->name('withdraw.coin');


    Route::get('/extrato', 'Dashboard\ExtratoController@index')->name('extrato');

    Route::get('/ordem', 'Dashboard\OrderController@index')->name('ordem');
    Route::post('/ordem/buy', 'Dashboard\OrderController@buy')->name('order.buy');
    Route::post('/ordem/sell', 'Dashboard\OrderController@sell')->name('order.sell');

    Route::get('/list', 'Dashboard\OrderController@lista')->name('list.order');

});


/*
 * 

/*

$ws = new  App\Services\WalletService();
$key =$ws->getBTC()->client->getaccount("3LjdfR28hJWTdSjjnHxUfLJJaceeZCnjkv")->get();
$unconfirmed = $ws->getBTC()->client->listtransactions($key)->get();

if (!empty($unconfirmed) && count($unconfirmed)>0){
    foreach($unconfirmed as $chave => $value){
        if ($value["category"]=="receive" && $value["confirmations"]<6) {
             $balance += $value["amount"];
        }
    }
}
dd($balance);

$ws = new  App\Services\WalletService();
$key =$ws->getBTC()->client->getaccount("3LjdfR28hJWTdSjjnHxUfLJJaceeZCnjkv")->get();
$unconfirmed = $ws->getBTC()->client->listtransactions($key)->get();

if (!empty($unconfirmed) && count($unconfirmed)>0){
    foreach($unconfirmed as $chave => $value){
        $balance += $value["amount"];
    }
}

$ws = new  App\Services\WalletService();
$key =$ws->getXRP()->newAddress();
dd($key);
$ripple = new App\Services\Wallets\RippleTopRpcClient("http://rippled.boldcoins.com.br:5010/");
$resp = $ripple->wallet_propose(["passphrase" => "snoPBrXtMeMyMHUVTgbuqAfg1SUTb"]);
dd($resp);

use Illuminate\Support\Facades\Crypt;

// Encrypt the message 'Hello, Universe'.
$encrypted = Crypt::encrypt('Hello, Universe');
// Decrypt the $encrypted message.
$message   = Crypt::decrypt($encrypted);

dd($message);

$ws = new  App\Services\WalletService();
$blockHash = '000000000019d6689c085ae165831e934ff763ae46a2a6c172b3f1b60a8ce26f';
//$blockInfo = $ws->getBTC()->transfer(env("BTCBASE"),"37e3nau71BJ6Xh9ke4t2YWzg1i23nijxqL",1);

 */