<?php

use Illuminate\Http\Request;


//Route::get('/public/coin/{type}/{limit}', 'Api\WSCoinCrontroller@getCoin')->name('ws.get.bitcoin.values');
Route::get('/home', 'Api\WSCoinCrontroller@getCoin');
Route::get('/public/coin/{type}/ticker', 'Api\WSCoinCrontroller@getTicker');
Route::get('/public/coin/{type}/book', 'Api\WSCoinCrontroller@getBook');
Route::get('/public/coin/{type}/trades', 'Api\WSCoinCrontroller@getTrades');
Route::get('/public/coin/{type}/summary/{year}/{month}/{day}', 'Api\WSCoinCrontroller@getSumarry');
Route::get('/public/coin/{type}/summary/{year}/{month}', 'Api\WSCoinCrontroller@getSumarry');
Route::get('/public/coin/{type}/summary/{year}/', 'Api\WSCoinCrontroller@getSumarry');
Route::get('/public/coin/{type}/summary/', 'Api\WSCoinCrontroller@getSumarry');

Route::post('/private/orders/', 'Api\WSPrivateCrontroller@getMyOrders')->middleware('auth:api');
Route::post('/private/balance/', 'Api\WSPrivateCrontroller@myBalance')->middleware('auth:api');
Route::post('/private/order/create', 'Api\WSPrivateCrontroller@setOrder')->middleware('auth:api');
Route::post('/private/order/delete', 'Api\WSPrivateCrontroller@deleteOrder')->middleware('auth:api');


