@extends('admin.layouts.main')

@push("body-class") horizontal-menu @endpush

@section('content')
  @include("admin.partials.head")
  <div class="page-container row-fluid">
    <!-- BEGIN PAGE CONTAINER-->
    <div class="page-content">
      @include("admin.partials.menu") 
    <div class="clearfix"></div>
    <div class="content">
        <ul class="breadcrumb">
            <li>
            <p>VOCÊ ESTÁ AQUI</p>
            </li>
            @each('admin.partials.breadcrumb', [['name'=>'Home','active'=>false,'route'=>route('admin.index')],['name'=>'Landing','active'=>true,'route'=>route('admin.landing.cadastro')]], 'list')
        </ul>
        <div class="page-title"> <i class="icon-custom-left"></i>
            <h3>Landing Page - <span class="semi-bold">Contato</span></h3>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="grid simple ">
                <div class="grid-title no-border">
                    <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="remove"></a>
                    </div>
                </div>
                <div class="grid-body no-border">
                    
                    <h3>Lista  <span class="semi-bold"></span></h3>
                    
                    <br>
                    <table class="table table-bordered no-more-tables">
                    <thead>
                        <tr>
                        <th class="text-center" style="width:12%">Nome</th>
                        <th class="text-center" style="width:12%">Telefone</th>
                        <th class="text-center" style="width:12%">Celular</th>
                        <th class="text-center" style="width:12%">Msg</th>
                        </tr>
                    </thead>
                    <tbody>
                        @inject("users","App\Models\Contact")
                        @foreach($users->get() as $user)
                        <tr>
                            <td class="text-center">{{$user->firstname." ".$user->lastname}}</td>
                            <td class="text-center">{{$user->phone}}</td>
                            <td class="text-center">{{$user->cel}}</td>
                            <td class="text-center">{{$user->msg}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>



    </div>
</div>
</div>
@endsection
