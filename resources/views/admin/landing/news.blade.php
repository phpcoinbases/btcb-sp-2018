@extends('admin.layouts.main')

@push("body-class") horizontal-menu @endpush

@section('content')
  @include("admin.partials.head")
  <div class="page-container row-fluid">
    <!-- BEGIN PAGE CONTAINER-->
    <div class="page-content">
      @include("admin.partials.menu") 
    <div class="clearfix"></div>
    <div class="content">
        <ul class="breadcrumb">
            <li>
            <p>VOCÊ ESTÁ AQUI</p>
            </li>
            @each('admin.partials.breadcrumb', [['name'=>'Home','active'=>false,'route'=>route('admin.index')],['name'=>'News','active'=>true,'route'=>route('admin.news')]], 'list')
        </ul>
        <div class="page-title"> <i class="icon-custom-left"></i>
            <h3>Noticias - <span class="semi-bold"></span></h3>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="grid simple ">
                <div class="grid-title no-border">
                    <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="remove"></a>
                    </div>
                </div>
                <div class="grid-body no-border">
                    <div class="row-fluid">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif                        
                        <form action="{{route("admin.save-news")}}" method="POST">
                            {{ csrf_field() }}
                            <h3>Formulario <span class="semi-bold">cadastro</span></h3>
                            <br>
                            <div class="input-group transparent">
                                <span class="input-group-addon primary">		                            
                                        <i class="fa fa-align-justify"></i>
                                </span>
                                <textarea name="description" class="form-control" placeholder="Noticia"></textarea>
                            </div>
                            <br>
                            <div class="input-group">
                                <input type="text" name="link" placeholder="Link Url completo (http://....)" class="form-control">
                                <span class="input-group-addon primary">		                            
                                    <i class="fa fa-align-justify"></i>
                                </span>
                            </div>
                            <br>
                            <div class="input-group">
                                <button type="submit" class="btn btn-primary btn-cons"><i class="fa fa-check"></i>Cadastrar</button>
                            </div>
                        </form>
                    </div>
                    <h3>Lista  <span class="semi-bold"></span></h3>
                    
                    <br>
                    <table class="table table-bordered no-more-tables">
                    <thead>
                        <tr>
                        <th class="text-center" >Noticia</th>
                        <th class="text-center" style="width:10%">Excluir</th>
                        </tr>
                    </thead>
                    <tbody>
                        @inject("news","App\Models\Magazine")
                        @foreach($news->orderBy("id","Desc")->get() as $new)
                        <tr>
                            <td class="text-left">{{$new->description}}</td>
                            <td><a href="{{route("admin.news.delete",$new->id)}}" onclick="return confirm('Tem certeza que deseja excluir este item?');" class="btn btn-danger btn-cons">Excluir</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>



    </div>
</div>
</div>
@endsection
