@extends('admin.layouts.main')

@push("body-class") horizontal-menu @endpush

@section('content')
  @include("admin.partials.head")
  <div class="page-container row-fluid">
    <!-- BEGIN PAGE CONTAINER-->
    <div class="page-content">
      @include("admin.partials.menu") 
    <div class="clearfix"></div>
    <div class="content">
        <ul class="breadcrumb">
            <li>
            <p>VOCÊ ESTÁ AQUI</p>
            </li>
            @each('admin.partials.breadcrumb', [['name'=>'Home','active'=>false,'route'=>route('admin.index')],['name'=>'Depositos','active'=>true,'route'=>route('admin.deposits')]], 'list')
        </ul>
        <div class="page-title"> <i class="icon-custom-left"></i>
            <h3>Depositos - <span class="semi-bold">Conferir e Aprovar</span></h3>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="grid simple ">
                <div class="grid-title no-border">
                    <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="remove"></a>
                    </div>
                </div>
                <div class="grid-body no-border">
                    
                    <h3>Lista  <span class="semi-bold"></span></h3>
                    
                    <br>
                    <table class="table table-bordered no-more-tables">
                    <thead>
                        <tr>
                        <th class="text-center" style="width:22%">Nome</th>
                        <th class="text-center" style="width:12%">Valor</th>
                        <th class="text-center" style="width:12%">Data</th>
                        <th class="text-center" style="width:2%">Imagem</th>
                        <th class="text-center" style="width:2%">Aceitar</th>
                        <th class="text-center" style="width:2%">Negar</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($deposits as $obj)
                        <tr>
                            
                            <td class="text-center">{{$obj->user->name}}</td>
                            <td class="text-center">{{$obj->value}}</td>
                            <td class="text-center">{{$obj->created_at->format("d/m - H:i")}}</td>
                            <td class="text-center"><a target="_blank" href="{{url('https://s3.us-east-2.amazonaws.com/coinbold/'.$obj->comprovante)}}">Link</a></td>


                            <td class="text-center">
                                <form action="{{route('admin.deposits.accept')}}" method="post">
                                    @csrf
                                    <input type="hidden" name="deposit_id" value="{{$obj->id}}">                        
                                    <button class="btn btn-success btn-cons" type="submit"><i class="icon-ok"></i> Aceitar</button>
                                </form>    
                            </td>                                
                            <td class="text-center">
                                <form action="{{route('admin.deposits.denied')}}" method="post">
                                    @csrf
                                    <input type="hidden" name="deposit_id" value="{{$obj->id}}">                        
                                    <button class="btn btn-danger btn-cons" type="submit"><i class="icon-ok"></i> Recusar</button>
                                </form>
                            </td>   
                                   
                            </form>
                        </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>



    </div>
</div>
</div>
@endsection
