<div class="bar">
  <div class="bar-inner">
    <ul>
      <li>
        <a href="javascript:;">Dash<span class="semi-bold">board </span></a>
      </li>
      <li class="classic">
          <a href="javascript:;">
            Landing<span class="semi-bold">Page</span> <i class="fas fa-angle-down"></i>
          </a>
          <ul class="classic">
            <li>
              <a href="{{route("admin.landing.cadastro")}}">Clientes
                <span class="description">
                  Lista dos clientes no pré-cadastro
                </span>
              </a>
            </li>
            <li>
                <a href="{{route("admin.landing.contato")}}">Formulario Contato
                  <span class="description">
                    Formulario de Contato 
                  </span>
                </a>
              </li>
            <li>
                <a href="{{route("admin.news")}}">Noticias
                  <span class="description">
                    Administrar as noticias da landing page
                  </span>
                </a>
            </li>              
          </ul>
      </li>               
      <li class="classic">
          <a href="javascript:;">
            Usu<span class="semi-bold">arios</span> <i class="fas fa-angle-down"></i>
          </a>
          <ul class="classic">
            <li>
              <a href="{{route("admin.clientes")}}">Clientes
                <span class="description">
                  Lista dos clientes da plataforma CoinBold
                </span>
              </a>
            </li>
        
          </ul>
      </li>              
      <li class="classic">
        <a href="javascript:;">
          Paga<span class="semi-bold">mentos</span> <i class="fas fa-angle-down"></i>
        </a>
        <ul class="classic">
          <li>
            <a href="{{route("admin.deposits")}}">Depositos
              <span class="description">
                Verifique os depositos dos usuários e libere o pagamento
              </span>
            </a>
          </li>                 
          <li>
              <a href="{{route("admin.saques")}}">Saques
                <span class="description">
                  Autorize os saques em Criptomoedas e Dinheiro
                </span>
              </a>
            </li>                 
                            
        </ul>
      </li>
      <li class="classic">
          <a href="javascript:;">
            Transa<span class="semi-bold">ções</span> <i class="fas fa-angle-down"></i>
          </a>
          <ul class="classic">
            <li>
              <a href="{{route("admin.ordens")}}">Ordens abertas
                <span class="description">
                  Lista de ordens em aberto.
                </span>
              </a>
            </li>                 
            <li>
                <a href="{{route("admin.transacoes")}}">Transações realizadas
                  <span class="description">
                    Lista de vendas/compras realizadas pelos usuarios
                  </span>
                </a>
              </li>                               
          </ul>
        </li>              
      <li class="classic">
          <a href="javascript:;">
            Sist<span class="semi-bold">ema</span> <i class="fas fa-angle-down"></i>
          </a>
          <ul class="classic">
            <li>
              <a href="{{route("admin.taxas")}}">Taxas
                <span class="description">
                  Gerencie as taxas do sistema
                </span>
              </a>
            </li>
            <li>
                <a href="{{route("admin.wallets")}}">Carteiras
                  <span class="description">
                    Escolha as carteiras que irão receber as taxas
                  </span>
                </a>
            </li> 
            <li>
                <a href="index.html">Administradores
                  <span class="description">
                    Gerencie os Administradores do sistema
                  </span>
                </a>
            </li>                                     
          </ul>
      </li>                   
      
    </ul>
  </div>
</div>

