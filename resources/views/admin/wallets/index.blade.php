@extends('admin.layouts.main')

@push("body-class") horizontal-menu @endpush

@section('content')
  @include("admin.partials.head")
  <div class="page-container row-fluid">
    <!-- BEGIN PAGE CONTAINER-->
    <div class="page-content">
      @include("admin.partials.menu") 
    <div class="clearfix"></div>
    <div class="content">
        <ul class="breadcrumb">
            <li>
            <p>VOCÊ ESTÁ AQUI</p>
            </li>
            @each('admin.partials.breadcrumb', [['name'=>'Home','active'=>false,'route'=>route('admin.index')],['name'=>'Carteiras','active'=>true,'route'=>route('admin.wallets')]], 'list')
        </ul>
        <div class="page-title"> <i class="icon-custom-left"></i>
            <h3>Sistema - <span class="semi-bold">Carteiras</span></h3>
        </div>

        <div class="row">
            @inject("coins","App\Models\Cryptocoin")

            
            <div class="col-md-12">
                <div class="grid simple form-grid">
                    <div class="grid-title no-border">
                        <h4>Cadastro das <span class="semi-bold">Carteiras</span></h4>
                    </div>
                    <div class="grid-body no-border">
                        <form action="{{route("admin.save.wallets")}}" method="POST" id="form_traditional_validation" name="form_traditional_validation" role="form" autocomplete="off" class="validate" novalidate="novalidate">
                        @csrf
                        <div class="form-group">
                            <label class="form-label">Bitcoin</label> <span class="help">e.g. "Endereço da carteira do bitcoin"</span>
                            <input value="{{auth()->user()->with('wallets')->first()->wallets->where("cryptocoin_id",$coins->where("symbol","BTC")->first()->id)->first()->address}}" class="form-control" id="" name="wallet[BTC]" type="text" required="" aria-required="true">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Bitcoin Cash</label> <span class="help">e.g. "Endereço da carteira do bitcoin cash"</span>
                            <input value="{{auth()->user()->with('wallets')->first()->wallets->where("cryptocoin_id",$coins->where("symbol","BCH")->first()->id)->first()->address}}" class="form-control" id="" name="wallet[BCH]" type="text" required="" aria-required="true">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Ethereum</label> <span class="help">e.g. "Endereço da carteira do ethereum"</span>
                            <input value="{{auth()->user()->with('wallets')->first()->wallets->where("cryptocoin_id",$coins->where("symbol","ETH")->first()->id)->first()->address}}" class="form-control" id="" name="wallet[ETH]" type="text" required="" aria-required="true">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Litecoin</label> <span class="help">e.g. "Endereço da carteira do litecoin"</span>
                            <input value="{{auth()->user()->with('wallets')->first()->wallets->where("cryptocoin_id",$coins->where("symbol","LTC")->first()->id)->first()->address}}" class="form-control" id="" name="wallet[LTC]" type="text" required="" aria-required="true">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Ripple</label> <span class="help">e.g. "Endereço da carteira do rippled"</span>
                            <input value="{{auth()->user()->with('wallets')->first()->wallets->where("cryptocoin_id",$coins->where("symbol","XRP")->first()->id)->first()->address}}" class="form-control" id="" name="wallet[XRP]" type="text" required="" aria-required="true">
                        </div>
                        
                       
                        <div class="form-actions">
                            <div class="pull-right">
                                <button class="btn btn-success btn-cons" type="submit"><i class="icon-ok"></i> Save</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    


    </div>
</div>
</div>
@endsection
