@extends('admin.layouts.main')

@push("body-class") horizontal-menu @endpush

@section('content')
  @include("admin.partials.head")
  <div class="page-container row-fluid">
    <!-- BEGIN PAGE CONTAINER-->
    <div class="page-content">
      @include("admin.partials.menu") 
      <div class="clearfix"></div>
      <div class="content">
        <ul class="breadcrumb">
          <li>
            <p>VOCÊ ESTÁ AQUI</p>
          </li>
          @each('admin.partials.breadcrumb', [['name'=>'Home','active'=>true,'route'=>route('admin.index')]], 'list')
        </ul>
        <div class="page-title"> <i class="icon-custom-left"></i>
          <h3>Dashboard - <span class="semi-bold">Informações</span></h3>
        </div>

        <div class="row">
          <div class="col-md-4 col-vlg-3 col-sm-6">
            <div class="tiles green m-b-10">
              <div class="tiles-body">
                <div class="controller">
                  <a href="javascript:;" class="reload"></a>
                  <a href="javascript:;" class="remove"></a>
                </div>
                <div class="tiles-title text-black">OVERALL SALES </div>
                <div class="widget-stats">
                  <div class="wrapper transparent">
                    <span class="item-title">Overall Visits</span> <span class="item-count animate-number semi-bold" data-value="2415" data-animation-duration="700">2,415</span>
                  </div>
                </div>
                <div class="widget-stats">
                  <div class="wrapper transparent">
                    <span class="item-title">Today's</span> <span class="item-count animate-number semi-bold" data-value="751" data-animation-duration="700">751</span>
                  </div>
                </div>
                <div class="widget-stats ">
                  <div class="wrapper last">
                    <span class="item-title">Monthly</span> <span class="item-count animate-number semi-bold" data-value="1547" data-animation-duration="700">1,547</span>
                  </div>
                </div>
                <div class="progress transparent progress-small no-radius m-t-20" style="width:90%">
                  <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="64.8%" style="width: 64.8%;"></div>
                </div>
                <div class="description"> <span class="text-white mini-description ">4% higher <span class="blend">than last month</span></span>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-vlg-3 col-sm-6">
            <div class="tiles blue m-b-10">
              <div class="tiles-body">
                <div class="controller">
                  <a href="javascript:;" class="reload"></a>
                  <a href="javascript:;" class="remove"></a>
                </div>
                <div class="tiles-title text-black">OVERALL VISITS </div>
                <div class="widget-stats">
                  <div class="wrapper transparent">
                    <span class="item-title">Overall Visits</span> <span class="item-count animate-number semi-bold" data-value="15489" data-="" animation-duration="700">15,489</span>
                  </div>
                </div>
                <div class="widget-stats">
                  <div class="wrapper transparent">
                    <span class="item-title">Today's</span> <span class="item-count animate-number semi-bold" data-value="551" data-animation-duration="700">551</span>
                  </div>
                </div>
                <div class="widget-stats ">
                  <div class="wrapper last">
                    <span class="item-title">Monthly</span> <span class="item-count animate-number semi-bold" data-value="1450" data-animation-duration="700">1,450</span>
                  </div>
                </div>
                <div class="progress transparent progress-small no-radius m-t-20" style="width:90%">
                  <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="54%" style="width: 54%;"></div>
                </div>
                <div class="description"> <span class="text-white mini-description ">4% higher <span class="blend">than last month</span></span>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-vlg-3 col-sm-6">
            <div class="tiles purple m-b-10">
              <div class="tiles-body">
                <div class="controller">
                  <a href="javascript:;" class="reload"></a>
                  <a href="javascript:;" class="remove"></a>
                </div>
                <div class="tiles-title text-black">SERVER LOAD </div>
                <div class="widget-stats">
                  <div class="wrapper transparent">
                    <span class="item-title">Overall Load</span> <span class="item-count animate-number semi-bold" data-value="5695" data-animation-duration="700">5,695</span>
                  </div>
                </div>
                <div class="widget-stats">
                  <div class="wrapper transparent">
                    <span class="item-title">Today's</span> <span class="item-count animate-number semi-bold" data-value="568" data-animation-duration="700">568</span>
                  </div>
                </div>
                <div class="widget-stats ">
                  <div class="wrapper last">
                    <span class="item-title">Monthly</span> <span class="item-count animate-number semi-bold" data-value="12459" data-animation-duration="700">12,459</span>
                  </div>
                </div>
                <div class="progress transparent progress-small no-radius m-t-20" style="width:90%">
                  <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="90%" style="width: 90%;"></div>
                </div>
                <div class="description"> <span class="text-white mini-description ">4% higher <span class="blend">than last month</span></span>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-vlg-3 visible-xlg visible-sm col-sm-6">
            <div class="tiles red m-b-10">
              <div class="tiles-body">
                <div class="controller">
                  <a href="javascript:;" class="reload"></a>
                  <a href="javascript:;" class="remove"></a>
                </div>
                <div class="tiles-title text-black">OVERALL SALES </div>
                <div class="widget-stats">
                  <div class="wrapper transparent">
                    <span class="item-title">Overall Sales</span> <span class="item-count animate-number semi-bold" data-value="5669" data-animation-duration="700">5,669</span>
                  </div>
                </div>
                <div class="widget-stats">
                  <div class="wrapper transparent">
                    <span class="item-title">Today's</span> <span class="item-count animate-number semi-bold" data-value="751" data-animation-duration="700">751</span>
                  </div>
                </div>
                <div class="widget-stats ">
                  <div class="wrapper last">
                    <span class="item-title">Monthly</span> <span class="item-count animate-number semi-bold" data-value="1547" data-animation-duration="700">1,547</span>
                  </div>
                </div>
                <div class="progress transparent progress-small no-radius m-t-20" style="width:90%">
                  <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="64.8%" style="width: 64.8%;"></div>
                </div>
                <div class="description"> <span class="text-white mini-description ">4% higher <span class="blend">than last month</span></span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
            <div class="col-md-12 ">
              <div class="tiles white">
                <div class="row">
                  <div class="sales-graph-heading">
                    <div class="col-md-5 col-sm-5">
                      <h5 class="no-margin">You have earned</h5>
                      <h4><span class="item-count animate-number semi-bold" data-value="21451" data-animation-duration="700">21,451</span> USD</h4>
                    </div>
                    <div class="col-md-3 col-sm-3">
                      <p class="semi-bold">TODAY</p>
                      <h4><span class="item-count animate-number semi-bold" data-value="451" data-animation-duration="700">451</span> USD</h4>
                    </div>
                    <div class="col-md-4 col-sm-3">
                      <p class="semi-bold">THIS MONTH</p>
                      <h4><span class="item-count animate-number semi-bold" data-value="8514" data-animation-duration="700">8,514</span> USD</h4>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                <h5 class="semi-bold m-t-30 m-l-30">LAST SALE</h5>
                <table class="table no-more-tables m-t-20 m-l-20 m-b-30">
                  <thead style="display:none">
                    <tr>
                      <th style="width:9%">Project Name</th>
                      <th style="width:22%">Description</th>
                      <th style="width:6%">Price</th>
                      <th style="width:1%"> </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="v-align-middle bold text-success">25601</td>
                      <td class="v-align-middle"><span class="muted">Redesign project template</span> </td>
                      <td><span class="muted bold text-success">$4,500</span> </td>
                      <td class="v-align-middle"></td>
                    </tr>
                    <tr>
                      <td class="v-align-middle bold text-success">25601</td>
                      <td class="v-align-middle"><span class="muted">Redesign project template</span> </td>
                      <td><span class="muted bold text-success">$4,500</span> </td>
                      <td class="v-align-middle"></td>
                    </tr>
                  </tbody>
                </table>
                
              </div>
            </div>    
      
        </div>


        </div>
      </div>
  </div>
@endsection
