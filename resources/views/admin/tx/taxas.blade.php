@extends('admin.layouts.main')

@push("body-class") horizontal-menu @endpush

@section('content')
  @include("admin.partials.head")
  <div class="page-container row-fluid">
    <!-- BEGIN PAGE CONTAINER-->
    <div class="page-content">
      @include("admin.partials.menu") 
    <div class="clearfix"></div>
    <div class="content">
        <ul class="breadcrumb">
            <li>
            <p>VOCÊ ESTÁ AQUI</p>
            </li>
            @each('admin.partials.breadcrumb', [['name'=>'Home','active'=>false,'route'=>route('admin.index')],['name'=>'Tx','active'=>true,'route'=>route('admin.taxas')]], 'list')
        </ul>
        <div class="page-title"> <i class="icon-custom-left"></i>
            <h3>Sistema - <span class="semi-bold">Taxas | {{$tx->name}}</span></h3>
        </div>

        <div class="row">
                <div class="col-md-12">
                  <div class="grid simple form-grid">
                    <div class="grid-title no-border">

                    </div>
                    <div class="grid-body no-border">
                       <form action="{{route("admin.tx.taxas.save",[$tx->id])}}" method="POST" id="form_traditional_validation" name="form_traditional_validation" role="form" autocomplete="off" class="validate" novalidate="novalidate">
                        @csrf
                        
                        <div class="row column-seperation">
                            <div class="col-md-4">
                                <h4>Taxas de Deposito</h4>
                                <div class="row form-row">
                                <div class="col-md-6">
                                    <label class="form-label">Em Dinheiro (%)</label> 
                                    <input name="deposit_cash" value="{{$tx->taxes->firstWhere('type', 'deposit_cash')->our_tax}}"  type="text" class="form-control" placeholder="0.000">
                                </div>
                                <div class="col-md-6">
                                        <label class="form-label">Em AltCoins (%)</label> 
                                        <input name="deposit_btc" value="{{$tx->taxes->firstWhere('type', 'deposit_btc')->our_tax}}"  type="text" class="form-control" placeholder="0.000">
                                    </div>
                                </div>                            
                            </div>
                            <div class="col-md-4">
                                <h4>Taxas de Saque</h4>
                                <div class="row form-row">
                                    <div class="col-md-6">
                                        <label class="form-label">Saque (%)</label> 
                                        <input name="withdraw_cash[our_tax]"  value="{{$tx->taxes->firstWhere('type', 'withdraw_cash')->our_tax}}"  type="text" class="form-control" placeholder="0.000">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="form-label"> + Fixo (R$)</label> 
                                        <input name="withdraw_cash[fixed]" value="{{$tx->taxes->firstWhere('type', 'withdraw_cash')->fixed}}"  type="text" class="form-control" placeholder="0.000">
                                    </div>
                                </div>
                                <div class="row form-row">
                                    <div class="col-md-6">
                                        <label class="form-label">Não Convenio (%)</label> 
                                        <input name="withdraw_not_bank[our_tax]" value="{{$tx->taxes->firstWhere('type', 'withdraw_not_bank')->our_tax}}" type="text" class="form-control" placeholder="0.000">
                                    </div>
                                    <div class="col-md-6">
                                            <label class="form-label">+ Fixo (%)</label> 
                                            <input name="withdraw_not_bank[fixed]" value="{{$tx->taxes->firstWhere('type', 'withdraw_not_bank')->fixed}}"  type="text" class="form-control" placeholder="0.000">
                                    </div>
                                </div>                             
                                <div class="row form-row">
                                    <div class="col-md-6">
                                        <label class="form-label">Em AltCoin (%)</label> 
                                        <input name="withdraw_btc[our_tax]" value="{{$tx->taxes->firstWhere('type', 'withdraw_btc')->our_tax}}"  type="text" class="form-control" placeholder="0.000">
                                    </div>
                                    <div class="col-md-6">
                                            <label class="form-label">+ Fixo (%)</label> 
                                            <input name="withdraw_btc[fixed]" value="{{$tx->taxes->firstWhere('type', 'withdraw_btc')->fixed}}"  type="text" class="form-control" placeholder="0.000">
                                    </div>
                                </div>                                  
                            </div>
                            <div class="col-md-4">
                                <h4>Taxas das Ordens</h4>
                                <div class="row form-row">
                                    <div class="col-md-6">
                                        <label class="form-label">Ordem Executada</label> 
                                        <input name="passive"  value="{{$tx->taxes->firstWhere('type', 'passive')->our_tax}}"    type="text" class="form-control" placeholder="0.000">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="form-label">Ordem Executora</label> 
                                        <input name="ative"   value="{{$tx->taxes->firstWhere('type', 'ative')->our_tax}}" type="text" class="form-control" placeholder="0.000">
                                    </div>
                                </div>                                                                  
                            </div>
                        </div>
                        <div class="form-actions">
                          <div class="pull-right">
                            <button class="btn btn-danger btn-cons" type="submit"><i class="icon-ok"></i> Save</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>

        


    </div>
</div>
</div>
@endsection
