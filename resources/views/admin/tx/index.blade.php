@extends('admin.layouts.main')

@push("body-class") horizontal-menu @endpush

@section('content')
  @include("admin.partials.head")
  <div class="page-container row-fluid">
    <!-- BEGIN PAGE CONTAINER-->
    <div class="page-content">
      @include("admin.partials.menu") 
    <div class="clearfix"></div>
    <div class="content">
        <ul class="breadcrumb">
            <li>
            <p>VOCÊ ESTÁ AQUI</p>
            </li>
            @each('admin.partials.breadcrumb', [['name'=>'Home','active'=>false,'route'=>route('admin.index')],['name'=>'Clientes','active'=>true,'route'=>route('admin.clientes')]], 'list')
        </ul>
        <div class="page-title"> <i class="icon-custom-left"></i>
            <h3>Sistema - <span class="semi-bold">Grupo de Taxas</span></h3>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="grid simple form-grid">
                    <div class="grid-title no-border">
                        <h4>Cadastro de Grupo de  <span class="semi-bold">Taxas</span></h4>
                    </div>
                    <div class="grid-body no-border">
                        <form action="{{route("admin.tx.save")}}" method="POST" id="form_traditional_validation" name="form_traditional_validation" role="form" autocomplete="off" class="validate" novalidate="novalidate">
                        @csrf
                        <div class="form-group">
                            <label class="form-label">Nome</label> <span class="help">e.g. "Usuarios da BitConf - 0%"</span>
                            <input class="form-control" id="name" name="name" type="text" required="" aria-required="true">
                        </div>
                       
                        <div class="form-actions">
                            <div class="pull-right">
                                <button class="btn btn-success btn-cons" type="submit"><i class="icon-ok"></i> Save</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="grid simple ">
                <div class="grid-title no-border">
                    <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="remove"></a>
                    </div>
                </div>

                <div class="grid-body no-border">
                    
                    <h3>Lista  <span class="semi-bold"></span></h3>
                    
                    <br>
                    <table class="table table-bordered no-more-tables">
                    <thead>
                        <tr>
                        <th class="text-center" style="width:80%">Nome</th>
                        <th class="text-center" style="width:10%">Taxas</th>
                        <th class="text-center" style="width:10%">Deletar?</th>
                        </tr>
                    </thead>
                    <tbody>
                        @inject("tx","App\Models\Tx")
                        @foreach($tx->get() as $obj)
                        <tr>
                            <td class="text-center">{{$obj->name}}</td>
                            <td class="text-center">
                                <a href="{{ route('admin.tx.taxas', ['id' => $obj->id]) }}" class="btn btn-default">Taxas</a>
                            </td>       
                            <td class="text-center">
                                <form action="{{ route('admin.tx.delete', ['id' => $obj->id]) }}" method="post">
                                        {{ method_field('delete') }}
                                        {!! csrf_field() !!}
                                        <button class="btn btn-default" type="submit">Deletar</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>



    </div>
</div>
</div>
@endsection
