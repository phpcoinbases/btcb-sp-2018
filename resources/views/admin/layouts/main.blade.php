<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <title>@yield('title') Coinbold</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.11/css/all.css" integrity="sha384-p2jx59pefphTFIpeqCcISO9MdVfIm4pNnsL08A6v5vaQc4owkQqxMV8kg4Yvhaw/" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> 
        <link rel="stylesheet" href="{{ mix('/css/admin/webarch.css') }}">
        <link rel="stylesheet" href="{{ url('/css/fa-regular.min.css') }}">
        <link rel="stylesheet" href="{{ url('/css/fa-brands.min.css') }}">
        <link rel="stylesheet" href="{{ url('/css/fa-solid.min.css') }}">
        <link rel="stylesheet" href="{{ url('/css/cryptocoins.css') }}">
        <link rel="stylesheet" href="{{ url('/css/cryptocoins-colors.css') }}">

        <script type="application/json" data-settings-selector="settings-json">
            {!! json_encode([
                'auth' => auth()->check(),
                'csrfToken' => csrf_token(),
                'locale' => app()->getLocale(),
                'appName' => config('app.name')
            ]) !!}
        </script>

    </head>
    <body class="@stack('body-class')">   
        <div class="page-container row-fluid">
          @yield('content')
        </div>
        @stack('pre-scripts')  
        <!-- Latest compiled and minified JavaScript -->
        <script src="{{mix('/js/admin/webarch.js')}}"></script>
        <!-- scripts Contents -->
        @stack('scripts')        
    </body>
</html>
