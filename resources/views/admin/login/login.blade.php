@extends('admin.layouts.main')

@push("body-class") error-body no-top @endpush

@section('content')

<div class="container">
        <div class="row login-container column-seperation">
          <div class="col-md-5 col-md-offset-1">
            <h2>Entrar no sistema da CoinBold.</h2>
            <p>
              Lembre que o sistema pode pedir mais de uma vez o codigo de autenticação e o usuario precisa ter
              sempre acesso Admin para conseguir controlar.
              Depois de 5 tentativas erradas o sistema irá bloquear o seu IP e apenas o suporte poderá liberar.
            </p>            
          </div>
          <div class="col-md-5">
            <form method="POST" action="{{ route('admin-entrar') }}">
                @csrf
              <div class="row">
                <div class="form-group col-md-10">
                  <label class="form-label">Email</label>
                  <input id="email" type="email" placeholder="Email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-10">
                  <label class="form-label">Password</label> <span class="help"></span>
                  <input id="password" type="password"  placeholder="Senha" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                </div>
              </div>

              <div class="row">
                <div class="col-md-10">
                  <button class="btn btn-primary btn-cons pull-right" type="submit">Login</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>

@endsection
