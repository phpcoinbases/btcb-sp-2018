@extends('admin.layouts.main')

@push("body-class") horizontal-menu @endpush

@section('content')
  @include("admin.partials.head")
  <div class="page-container row-fluid">
    <!-- BEGIN PAGE CONTAINER-->
    <div class="page-content">
      @include("admin.partials.menu") 
    <div class="clearfix"></div>
    <div class="content">
        <ul class="breadcrumb">
            <li>
            <p>VOCÊ ESTÁ AQUI</p>
            </li>
            @each('admin.partials.breadcrumb', [['name'=>'Home','active'=>false,'route'=>route('admin.index')],['name'=>'Clientes','active'=>true,'route'=>route('admin.clientes')]], 'list')
        </ul>
        <div class="page-title"> <i class="icon-custom-left"></i>
            <h3>Clientes - <span class="semi-bold">Nossos Usuarios</span></h3>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="grid simple ">
                <div class="grid-title no-border">
                    <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="remove"></a>
                    </div>
                </div>
                <div class="grid-body no-border">
                    
                    <h3>Lista  <span class="semi-bold"></span></h3>
                    
                    <br>
                    <table class="table table-bordered no-more-tables">
                    <thead>
                        <tr>
                        <th class="text-center" style="width:12%">Nome</th>
                        <th class="text-center" style="width:22%">Email</th>
                        <th class="text-center" style="width:2%">Taxa Especial</th>
                        <th class="text-center" style="width:2%">Ação</th>
                        </tr>
                    </thead>
                    <tbody>
                        @inject("users","App\User")
                        @foreach($users->get() as $user)
                        <tr>
                            <form action="{{route('admin.user.taxas.save')}}" method="post">
                            @csrf
                            <td class="text-center">{{$user->name}}</td>
                            <td class="text-center">{{$user->email}}</td>
                            <td class="text-center">
                                @inject('taxas',"App\Models\Tx")
                                <select name="taxa_user" id="">
                                        <option value="">Selecione a Taxa do Usuario</option>
                                        @foreach ($taxas->get() as $tx)                                            
                                            <option @if($tx->id==$user->taxs->last()->tx_id) selected @endif value="{{$tx->id}}">{{$tx->name}}</option>
                                        @endforeach
                                </select>
                            </td>
                            <td class="text-center">
                                <input type="hidden" name="user_id" value="{{$user->id}}">                        
                                <button class="btn btn-danger btn-cons" type="submit"><i class="icon-ok"></i> Salvar</button>
                            </td>                                
                            </form>
                        </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>



    </div>
</div>
</div>
@endsection
