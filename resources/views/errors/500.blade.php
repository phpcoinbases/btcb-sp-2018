@extends('layouts.main')


@section('content')
    <div class="error-page">
        <h2 class="headline text-info"> 500</h2>
        <div class="error-content">
            <h3><i class="fa fa-warning text-yellow"></i> Oops! Error.</h3>
            
            @if(!empty(Sentry::getLastEventID()))
                <p>Please send this ID with your support request: {{ Sentry::getLastEventID() }}.</p>
            @endif            
        </div>
    </div>
@endsection