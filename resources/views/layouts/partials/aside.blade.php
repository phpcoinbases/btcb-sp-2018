@inject('metrics', 'App\Services\UserService')
<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!--// Route::currentRouteName())}}-->
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div style="color:aliceblue">
              <p>{{auth()->user()->name}}</p>
              <a href="{{route("perfil")}}"><i class="fa fa-circle text-success"></i>Sou cliente {{$metrics->getType()}}</a>
            </div>
          </div>
          <!-- search form -->
          
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MENU PRINCIPAL</li>
            <li class="{{ Route::currentRouteName() == 'list.order' ? 'active ' : ''}}"><a href="{{route("list.order")}}"><i class="fa fa-comments"></i> <span>Lista de Ordem</span></a></li>
            <li class="{{ Route::currentRouteName() == 'ordem' ? 'active ' : ''}}"><a href="{{route("ordem")}}"><i class="fa fa-book"></i> <span>Negociar</span></a></li>
            <li class="{{ Route::currentRouteName() == 'extrato' ? 'active ' : ''}}"><a href="{{route("extrato")}}"><i class="fa  fa-list"></i> <span>Extrato</span></a></li>
            <li class="{{ Route::currentRouteName() == 'deposit' ? 'active ' : ''}}" ><a href="{{route("deposit")}}"><i class="fa fa-balance-scale"></i> <span>Depositar</span></a></li>
            <li class="{{ Route::currentRouteName() == 'withdraw' ? 'active ' : ''}}"><a href="{{route("withdraw")}}"><i class="fa fa-credit-card"></i> <span>Sacar</span></a></li>
            


          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>