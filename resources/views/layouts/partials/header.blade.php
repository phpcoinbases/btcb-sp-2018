<!-- Logo -->

<a href="{{url("/")}}" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>B</b>TC</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>BTC</b>SP</span>
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">
    <div class="container-fluid">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <ul class="nav navbar-nav navbar-right">
              @guest
              
                  <li><a href="{{route("login")}}">Entrar/Cadastrar</a></li>  
              @else
                  <li><a href="{{route("perfil")}}">Seu Perfil</a></li>                    
                  <li><a href="{{route('googlefa')}}">Google 2FA</a></li>
                  <li>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Sair</a>                      
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                  </li>
              @endguest
            </ul>  
        </ul>
      </div>
    </div>
  </nav>
