<div class="form-group {{ ($errors->first($name)) ? 'has-error'  :''}}">
        <label for="{{ $name }}" class="col-sm-4 control-label">{{$label}}</label>
        <div class="col-sm-8">
            <select  class="form-control" name="{{$name}}" id="{{$id}}">
                    <option>Selecione o Banco</option>
                @foreach($value as $obj)
                    <option value="{{$obj->id}}">{{$obj->code}} - {{$obj->name}}</option>
                @endforeach
            </select>    
            <p class="help-block">{{$help or ""}}</p>           
            @include('layouts.subviews.alerts.errors', ['field' => $name])
        </div>
    </div>