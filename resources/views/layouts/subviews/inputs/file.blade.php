<div class="form-group {{ ($errors->first($name)) ? 'has-error'  :''}}">
        <label for="{{ $name }}" class="col-sm-4 control-label">{{$label}}</label>
        <div class="col-sm-8">
            <input type="file" class="form-control" name="{{$name}}" id="{{$id}}" value="{{$data  or ""}}" placeholder="{{$label}}">
            <p class="help-block">{{$help or ""}}</p>           
            @include('layouts.subviews.alerts.errors', ['field' => $name])
        </div>
    </div>