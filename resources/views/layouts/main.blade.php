<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <title>BCP SP - @yield('title')</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW"> 
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <!-- Bootstrap 3.3.2 -->
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- Font Awesome Icons -->
        <!--link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
         Ionicons -->
        <link href="https://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->        
        <link rel="stylesheet" href="{{ mix('/css/AdminLTE.css') }}">
        <link rel="stylesheet" href="{{ url('/css/_all-skins.min.css') }}">
        <link rel="stylesheet" href="{{ url('/css/fontawesome.min.css') }}">
        <link rel="stylesheet" href="{{ url('/css/fa-regular.min.css') }}">
        <link rel="stylesheet" href="{{ url('/css/fa-brands.min.css') }}">
        <link rel="stylesheet" href="{{ url('/css/fa-solid.min.css') }}">
        <!-- JS settings -->
        <script type="application/json" data-settings-selector="settings-json">
            {!! json_encode([
                'auth' => auth()->check(),
                'csrfToken' => csrf_token(),
                'locale' => app()->getLocale(),
                'appName' => config('app.name')
            ]) !!}
        </script>

    </head>
    <body class="hold-transition skin-green sidebar-mini">
        <div class="wrapper" id="">
            <header class="main-header">
                @include("layouts.partials.header")
            </header>
            
            <aside>
                @include("layouts.partials.aside")
            </aside>

            <main class="content-wrapper">
                @yield("content")
            </main>
            
            <footer>
                @include("layouts.partials.footer")
            </footer>
        </div>
        <!-- Latest compiled and minified JavaScript -->
        <script src="{{mix('/js/app.js')}}"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script src="{{url('/js/adminlte.min.js')}}"></script>
        <!-- scripts Contents -->
        @stack('scripts')        
    </body>
</html>