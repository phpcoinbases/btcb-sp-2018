@extends('dashboard.layouts.main')

@push("body-class") horizontal-menu @endpush

@section('content')
  @include("dashboard.partials.head")

  <div class="page-container row-fluid">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="page-content">
          @include("dashboard.partials.menu") 
          <div class="clearfix"></div>
          <div class="content">
            <ul class="breadcrumb">
              <li>
                <p>VOCÊ ESTÁ AQUI</p>
              </li>
              @each('admin.partials.breadcrumb', [['name'=>'Home','active'=>true,'route'=>route('admin.index')]], 'list')
            </ul>
            <div class="page-title"> <i class="icon-custom-left"></i>
              <h3>Dashboard - <span class="semi-bold">Informações</span></h3>
            </div>
  
      <div class="row">
           <div class="col-md-12 ">
               <div class="grid simple">
                   <div class="panel-heading"><strong>Autentificação em 2 Passos</strong></div>
                      <div class="grid-title no-border">
                          <p>A autenticação de dois fatores (2FA) fortalece a segurança de acesso, exigindo dois métodos (também chamados de fatores) para verificar sua identidade. A autenticação de dois fatores protege contra ataques de phishing, engenharia social e força bruta de senha e protege seus logins contra credenciais fracas ou roubadas de invasores.</p>
                          <br/>
                          <p>Para ativar a autenticação de dois fatores na sua conta, você precisa seguir os seguintes passos</p>
                          <strong>
                          <ol>
                              <li>Clique no botão Generate Secret, para gerar um código QR secreto exclusivo para o seu perfil</li>
                              <li>Verificar o OTP do Google Authenticator Mobile App</li>
                          </ol>
                          </strong>
                          <br/>

                      @if (session('error'))
                           <div class="alert alert-danger">
                               {{ session('error') }}
                           </div>
                       @endif
                       @if (session('success'))
                           <div class="alert alert-success">
                               {{ session('success') }}
                           </div>
                       @endif


                           @if(!count($data['user']->passwordSecurity))
                              <form class="form-horizontal" method="POST" action="{{ route('generate2faSecret') }}">
                                  {{ csrf_field() }}
                                   <div class="form-group">
                                       <div class="col-md-6 col-md-offset-4">
                                           <button type="submit" class="btn btn-primary">
                                              Gerar a chave secreta para ativar o 2FA
                                           </button>
                                       </div>
                                   </div>
                              </form>
                           @elseif(!$data['user']->passwordSecurity->google2fa_enable)
                              <strong>1. Digitalize este código de barras com o seu aplicativo Google Authenticator:</strong><br/>
                              <img src="{{$data['google2fa_url'] }}" alt="">
                              
                          <br/><br/>
                              <strong>2. Digite o pin o código para ativar 2FA</strong><br/><br/>
                              <form class="form-horizontal" method="POST" action="{{ route('enable2fa') }}">
                              {{ csrf_field() }}

                              <div class="form-group{{ $errors->has('verify-code') ? ' has-error' : '' }}">
                                  <label for="verify-code" class="col-md-4 control-label">Codigo Authenticator</label>

                                  <div class="col-md-6">
                                      <input id="verify-code" type="password" class="form-control" name="verify-code" required>

                                      @if ($errors->has('verify-code'))
                                          <span class="help-block">
                                       <strong>{{ $errors->first('verify-code') }}</strong>
                                   </span>
                                      @endif
                                  </div>
                              </div>
                                  <div class="form-group">
                                      <div class="col-md-6 col-md-offset-4">
                                          <button type="submit" class="btn btn-primary">
                                              Ativar 2FA
                                          </button>
                                      </div>
                                  </div>
                              </form>
                          @elseif($data['user']->passwordSecurity->google2fa_enable)
                              <div class="alert alert-success">
                                  2FA esta atualmente <strong>Ativado</strong> para sua conta.
                              </div>
                              <p>Se você estiver procurando desativar a autenticação de dois fatores. Por favor, confirme sua senha e clique em Desativar o botão 2FA.</p>
                              <form class="form-horizontal" method="POST" action="{{ route('disable2fa') }}">
                              
                              <div class="col-md-6 col-md-offset-5">

                                      {{ csrf_field() }}
                                  <button type="submit" class="btn btn-primary ">Desativar 2FA</button>
                              </div>
                              </form>
                           @endif
                       </form>
                   </div>
               </div>
           </div>
       </div>
   </div>
@endsection