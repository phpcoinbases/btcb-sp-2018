@extends('admin.layouts.main')
@section("title","Acesso bloqueado! | ")
@push("body-class") error-body no-top @endpush

@section('content')
<div class="container">
        <div class="row login-container column-seperation">
          <div class="col-md-6 col-md-offset-3">
            <h2 class="text-center">Acesso <b>Bloqueado</b> ao Dashboard CoinBold.</h2>
              <p class="text-center">Seu acesso está bloqueado ao sistema até que você realize o desbloquei no seu email.</p>
              <p class="text-center">Verifique se o email não está no Spam, caso não tenha recebido o email <a href="">clique aqui</a>
            </p>            
          </div>
        </div>
    </div>
@endsection
