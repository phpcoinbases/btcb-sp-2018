@extends('admin.layouts.main')
@section("title","Acesso bloqueado! | ")
@push("body-class") error-body no-top @endpush

@section('content')
<div class="container">
        <div class="row login-container column-seperation">
          <div class="col-md-6 col-md-offset-3">
            <h2 class="text-center">Dispositivo <b>Bloqueado</b> ao Dashboard CoinBold.</h2>
              <p class="text-center">Parece que um dispotivo desconhecido está tendando acessar a sua conta. Acesse seu email e autorize esse disposiivel.</p>
              <p class="text-center">Verifique se o email não está no Spam, caso não tenha recebido o email </p>
              <p class="text-center">
                    <form action="{{ route('authorize.resend') }}" method="POST">
                            {{ csrf_field() }}                        
                            <button type="submit" class="btn btn-primary">Reenviar o Email</button>
                    </form>
            </p>            
          </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="authorize_holder">
            <div class="authorize__holder--section">
                <div class="text">
                    <h3>Authorize New Device</h3>

                    <p>It looks like you're signing in to <a href="{{ url('/') }}">{{ url('/') }}</a> from a computer or device we haven't seen before, or for some time.</p>
                    <p>
                        Please <strong>click the confirmation link in the email we just sent you.</strong> This is a process that protects the security of your account.
                    </p>
                    <p>Note that you need to access this email with the same device that you are confirming.</p>
                </div>
                <div class="authorize__resend">
                       
                </div>
            </div>
        </div>
    </div>
@endsection