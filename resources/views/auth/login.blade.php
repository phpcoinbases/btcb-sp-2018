@extends('dashboard.layouts.main')

@push("body-class") error-body no-top @endpush

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm m-3">
            <h1 class="text-center login-box-msg">Entre ou Registre-se no sistema:</h1>
            <p class="text-center">
                Ao entrar no sistema serão geradas suas carteiras e você estará concordando com nossos
                termos e nossas diretrizes
            </p>
        </div>
    </div>    
    
    <div class="social-auth-links text-center">
    
        <a href="{{ url('login/facebook') }}"  class="btn btn-warning"><i class="fab fa-facebook-square"></i> 
            Facebook</a>
        <a href="{{ url('login/google') }}" class="btn btn-info"><i class="fab fa-google-plus"></i> 
            Google+</a>
        <a href="{{ url('login/live') }}" class="btn btn-success"><i class="fab fa-microsoft"></i> 
            Hotmail</a>            
        <a href="{{ url('login/instagram') }}"class="btn btn-info"><i class="fab fa-instagram"></i> 
            Instagram</a>                        
    </div>
</div>
@endsection
