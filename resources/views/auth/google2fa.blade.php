@extends('dashboard.layouts.main')

@push("body-class") horizontal-menu @endpush

@section('content')
  @include("dashboard.partials.head")

  <div class="page-container row-fluid">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="page-content">
          
          <div class="clearfix"></div>
          <div class="content">
            <ul class="breadcrumb">
              <li>
                <p>VOCÊ ESTÁ AQUI</p>
              </li>
              @each('admin.partials.breadcrumb', [['name'=>'Home','active'=>true,'route'=>route('admin.index')]], 'list')
            </ul>
            <div class="page-title"> <i class="icon-custom-left"></i>
              <h3>Validação em <span class="semi-bold">2 etapas</span></h3>
            </div>
  
            <div class="row">
                <div class="col-md-12 ">
                    <div class="grid simple">
                        <div class="panel-heading"><strong>Autentificação em 2 Passos</strong></div>
                           <div class="grid-title no-border">
                               <p>A autenticação de dois fatores (2FA) fortalece a segurança de acesso, exigindo dois métodos (também chamados de fatores) para verificar sua identidade. A autenticação de dois fatores protege contra ataques de phishing, engenharia social e força bruta de senha e protege seus logins contra credenciais fracas ou roubadas de invasores.</p>
                               <br/>
                               <p>Para ativar a autenticação de dois fatores na sua conta, você precisa seguir os seguintes passos</p>
                                    @if (session('error'))
                                        <div class="alert alert-danger">
                                            {{ session('error') }}
                                        </div>
                                    @endif
                                    @if (session('success'))
                                        <div class="alert alert-success">
                                            {{ session('success') }}
                                        </div>
                                    @endif

                        
                          <form class="form-horizontal" action="{{ route('2faVerify') }}" method="POST">
                              {{ csrf_field() }}
                              <div class="form-group{{ $errors->has('one_time_password-code') ? ' has-error' : '' }}">
                                  <div class="col-md-12">
                                      <input name="one_time_password"  class="col-md-12 form-control" placeholder="Codigo do Aplicativo"  type="text"/>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <div class="col-md-12 text-right">
                                       <button class="btn btn-primary" type="submit">Validar acesso</button>
                                  </div>
                              </div>
                          </form>

                   </div>
               </div>
           </div>
       </div>
   </div>
@endsection