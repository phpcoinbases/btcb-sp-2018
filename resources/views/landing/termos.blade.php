@extends("landing.layout")
@section('title', 'Termos e condições')
@section("content")
<div id="cabecalhoTermos" class="cabecalho">
        <div id="logo"><a href="{{route('home')}}"><img width="206" height="89" src="{{url('landing/img/coinbold.png')}}" /></a></div>
        <div id="menuMobile"><a id="linkMenuMobile" href="#"><img src="{{url('landing/img/icoMenu.png')}}" width="40" height="59"></a></div>
        <div id="menu">
            <ul>
                    <li><a href="{{route('landing.quem')}}">Quem somos</a></li>
                    <li class="separadorMenu">•</li>
                    <li><a href="{{route('landing.termos')}}">Termos e condições</a></li>
                <!--<li class="separadorMenu">•</li>
                <li><a href="">Segurança</a></li>
                <li>•</li>
                <li><a href="">Mais</a></li>-->
            </ul>
        </div>
        <!--<div id="ctaLogin"><a href="#">Login</a></div>-->
    </div>
    
    
    <div id="bannerTermos">
        <h2>Termos de <span>Uso</span></h2>
        <p>Termos e Condições Coinbold</p>     
    </div>
    
    <div id="termos">
        <div id="bgBranco">
            <h3>Termos e condições - Coinbold</h3>
            <p>Este contrato descreve os termos e condições (os "Termos e Condições") aplicáveis ao uso dos serviços oferecidos pela COINBOLD SERVICOS DIGITAIS LTDA, dentro do website: www.coinbold.com.br</p>
            <p>"VOCÊ DEVE LER E CONCORDAR COM O PRESENTE TERMO DE USO E COM NOSSA POLÍTICA DE PRIVACIDADE ANTES DE UTILIZAR QUAISQUER SERVIÇOS DO SITE COINBOLD (www.coinbold.com.br).</p>
            <p>AO CADASTRAR NO SITE (coinbold.com.br), VOCÊ ESTARÁ CONCORDANDO COM TODOS OS TERMOS DE USO, CONFORME ADIANTE DESCRITOS.</p>
            <p>ANTES DE ACEITAR OS PRESENTES TERMOS DE USO, CERTIFIQUE-SE DE QUE VOCÊ TENHA COMPREENDIDO E CONCORDADO COM SEU INTEIRO TEOR."</p>
            
            <h3>Sobre o Website</h3>
            
            <p>O website COINBOLD, acessado através do endereço https://coinbold.com.br, (doravante referido simplesmente como "website") é mantido pela COINBOLD SERVIÇOS DIGITAIS LTDA (doravante referido como COINBOLD)., empresa inscrita no CNPJ sob n.° xxxxxxxxxx</p>
            
            <p>O website oferece a seus usuários uma plataforma para a compra e venda de Bitcoin e outras criptomoedas dos próprios usuários.</p>
            
            <p>Através do website, o usuário pode vender suas criptomoedas a outros usuários, ou comprar criptomoedas de outros usuários.</p>
            
            <p>A COINBOLD não compra, nem vende bitcoins e criptomoedas. O website apenas possibilita a aproximação entre usuários que desejam comprar ou vender seus próprios Bitcoins e criptomoedas, e oferece funcionalidades para facilitar e dar segurança a essas transações.</p>
            
            <p>A COINBOLD NÃO É UMA INSTITUIÇÃO FINANCEIRA OU INSTITUIDORA DE ARRANJO DE PAGAMENTOS. Nos termos do Comunicado n. 25.306 do Banco Central do Brasil, a titularidade de uma Criptomoeda não é garantia de conversão para moeda oficial, sendo que sua conversão depende do interesse de outros usuários da plataforma em converter os direitos creditórios relacionados com determinada criptomoeda em moeda oficial. Para a realização de conversões entre os usuários, que são de responsabilidade única e exclusiva dos usuários, a COINBOLD disponibiliza soluções de pagamento de terceiros, como instituições financeiras, sendo essas transações de exclusiva responsabilidade dessas empresas. A COINBOLD NÃO CUSTODIA E/OU TRANSACIONA DIRETAMENTE QUAISQUER VALORES FINANCEIROS.</p>
           
            
            <h3>Cookies</h3>
            
            <p>Os cookies são pequenos arquivos que podem ou não ser adicionados no seu terminal e que permitem armazenar e reconhecer dados da sua navegação.</p>
            
            <p>Em sua navegação na plataforma poderão ser utilizados 04 (quatro) tipos de cookies:</p>
            
            <p>(i) cookies de autenticação: servem para reconhecer um determinado Usuário, possibilitando o acesso e utilização do website e/ou aplicativo com conteúdo e/ou serviços restritos e proporcionando experiências de navegação mais personalizadas;</p>
            
            <p>(ii) cookies de segurança: são utilizados para ativar recursos de segurança da plataforma, com a finalidade de auxiliar o monitoramento e/ou detecção de atividades maliciosas ou vedadas por este “Termos de Uso”, bem como de proteger as informações do usuário do acesso por terceiros não autorizados;</p>

            <p>(iii) cookies de pesquisa, análise e desempenho: a finalidade deste tipo de cookie é ajudar a entender o desempenho da plataforma, medir sua audiência, verificar os hábitos de navegação dos usuários, bem como a forma pela qual chegou na página do website (por exemplo, através de links de outros sites, buscadores ou diretamente pelo endereço);</p>

            <p>(iv) cookies de propaganda: são usados para apresentar publicidade relevante ao usuário, tanto dentro quanto fora da plataforma ou em websites de parceiros, bem como para saber se os usuários que visualizaram a publicidade visitaram o website após terem visto a publicidade. Os cookies de propaganda também podem ser utilizados para lembrar eventuais pesquisas realizadas pelos usuários na plataforma e, com base nestas pesquisas, apresentar anúncios relacionados aos seus interesses.</p>
            
            <p>Para os fins descritos no item anterior, a COINBOLD poderá coletar, armazenar, tratar, processar e utilizar as seguintes informações a respeito da navegação do usuário na plataforma, que integram os “registros de navegação”:</p>

            <p>(i) localização geográfica;</p>
            
            <p>(ii) sistema operacional utilizado pelo usuário;</p>
            
            <p>(iii) navegador e suas respectivas versões;</p>
            
            <p>(iv) resolução de tela;</p>
            
            <p>(v) java script;</p>
            
            <p>(vi) reprodutor de flash instalado;</p>
            
            <p>(vii) endereço IP;</p>
            
            <p>(viii) código ID (IMEI) do aparelho mobile pelo qual o usuário acessou a plataforma;</p>
            
            <p>(ix) informações referentes à data e hora de uso da plataforma por um determinado usuário, a partir de um determinado endereço IP;</p>
            
            <p>(x) informações referentes às quantidades de cliques e tentativas de uso da plataforma, bem como de páginas acessadas pelo usuário.</p>
            
            <p>O usuário poderá desabilitar os cookies por meio das opções de configuração do seu respectivo navegador. Contudo, ao decidir pela proibição dos cookies, o usuário está ciente e reconhece que é possível que a plataforma não desempenhe todas as suas funcionalidades.</p>


            <h3>Cadastro</h3>
            
            <p>A COINBOLD não revisa as informações oferecidas pelo usuário, não se responsabilizando, em hipótese alguma, por tais informações.</p>
            
            <p>O nome do usuario e respectiva senha não devem conter qualquer semelhanca com o nome da plataforma COINBOLD</p>

            <p>Para comprar ou vender criptomoedas através de nosso website, cada usuário deve aceitar os presentes termos de uso e nossa política de privacidade e realizar seu cadastro no website. O cadastro é realizado através do preenchimento do formulário on-line disponível na opção "cadastre-se" na página inicial do website. Além do preenchimento do formulário on-line, caso o usuário queira ter integral acesso aos serviços e funcionalidades disponíveis no website, ele deverá fornecer informações adicionais e verídicas, como: CPF, nome completo, endereço completo e comprovante de origem dos recursos (moeda fiduciaria). A COINBOLD poderá solicitar ao usuário e cliente o envio de documentos comprobatórios como RG e CPF para validação das informações fornecidas pelos usuários e clientes. Os usuários que optarem por fornecer todas informações e não enviar a cópia de seu documento de identidade poderão ter restrições para realização de compra ou venda de criptomoedas através do website, recebimento e envio de valores em reais.</p>

            <p>Se você tem menos de 18 anos de idade, não deve enviar dados pessoais próprios ou de amigos, nem se inscrever em nenhum dos serviços on-line da COINBOLD, nem clicar em botões de solicitação ou consentimento neste website.</p>

            <p>O cadastro de pessoas jurídicas como usuário deverá ser realizado necessariamente por um representante legal devidamente autorizado para tanto.</p>

            <p>O usuário obriga-se a fornecer informações verdadeiras, atuais e completas quando de seu cadastro e deverá manter sempre atualizadas as referidas informações. A COINBOLD não se responsabiliza pelas informações prestadas por cada usuário no momento de seu cadastro, sendo tais informações fornecidas sob exclusiva responsabilidade do usuário.</p>

            <p>O fornecimento de informação falsa ou a utilização indevida de dados de terceiros em nome próprio constitui crime tipificado pelo Código Penal Brasileiro. Em qualquer desses casos, a COINBOLD poderá suspender ou cancelar definitivamente o acesso do usuário em questão a todos os serviços e funcionalidades do website, sem prejuízo das demais medidas que lhe sejam asseguradas pela legislação em vigor.</p>

            <p>A COINBOLD poderá, a seu exclusivo critério, requisitar documentos e informações adicionais para confirmar ou manter o cadastro de qualquer usuário. Caso a COINBOLD decida exercer essa faculdade em relação a qualquer usuário, o cadastro desse usuário poderá ser suspenso ou definitivamente cancelado caso o usuário se recuse a prestar as informações ou a enviar os documentos requeridos ou, caso, ainda, se constate, a partir da análise de tais informações e documentos, que o usuário inseriu informação falsa ou incompleta no momento de seu cadastro.</p>
            
            <p>Cada usuário poderá manter apenas um único cadastro no website. Caso seja constatada a existência de mais de um cadastro relativo a uma mesma pessoa, um ou todos esses cadastros poderão ser suspensos ou cancelados, a critério da COINBOLD</p>

            <p>A COINBOLD se reserva o direito de recusar qualquer solicitação de cadastro e de suspender ou cancelar um cadastro previamente aceito nos casos (i) de violação de qualquer das disposições destes termos de uso, (ii) impossibilidade de verificação da identidade do usuário ou constatação de falsidade em qualquer das informações por ele fornecidas, (iii) prática pelo usuário de atos fraudulentos ou dolosos ou a adoção de qualquer comportamento que, a critério da COINBOLD, seja incompatível com os objetivos do website, com a urbanidade com outros usuários ou que possam, de qualquer modo, causar danos a terceiros ou ao website. Caso o cadastro de qualquer usuário seja suspenso ou cancelado por quaisquer das razões previstas nestes termos de uso, todas as suas ordens de compra ou de venda de Bitcoins ainda não executadas serão automaticamente canceladas.</p>

            <p>A carteira virtual poderá ser acessada pelo usuário mediante a utilização de seu login e senha, que também serão utilizados para o usuário possa realizar consulta de saldos e transações. Para utilização dos serviços de saque de valores, o usuário deverá apresentar uma imagem legível de seu comprovante residência, com vencimento de até 2 (dois) meses anteriores, bem como uma imagem legível contendo cópia de um documento de identificação pessoal com foto. O usuário também precisará fornecer seus dados bancários obrigatoriamente vinculados ao seu cadastro de pessoa física (CPF).</p>

            <p>O simples cadastro, sem o envio de documentos pessoais, confere ao usuário o direito de uso no perfil “light”, e nesta condição o internauta deixará de ter acesso a várias funcionalidades da plataforma, como também terá significativas restrições nas operações “trading” que realizar.</p>

            <p>Para a realização do cadastro com o perfil “Bold”, o qual permite integral acesso aos serviços e funcionalidades disponíveis na plataforma, o usuário deverá enviar, através do próprio “website”, a imagem com ótima resolução e nitidez dos seguintes documentos:</p>

            <p>(i) documento de identidade, com foto e válido em todo o território nacional;</p>

            <p>(ii) Comprovante de origem dos recursos financeiros;</p>

            <p>(iii) comprovante de residência; e</p>

            <p>(iv) contrato social, com última alteração e consolidado, para o caso de pessoas jurídicas.</p>

            <p>O usuario está ciente de que é proibido manter mais de um cadastro no website e que, em caso de descumprimento, a COINBOLD poderá cancelar ou suspender todos os cadastros e ORDENS DE COMPRA E VENDA vinculados.</p>

            <p>Contas inativas por mais de 6 meses serao descadastradas do website COINBOLD</p>

            <p>O usuário concorda que, caso a sua conta seja cancelada, por iniciativa do usuário ou pela COINBOLD: (i) todas as taxas pagas, mesmo que antecipadamente, não serão reembolsadas ao usuário; (ii) todas as informações armazenadas na plataforma relativas as suas transações não poderão ser acessadas ou resgatadas, não possuindo a COINBOLD qualquer dever em armazenar ou repassar essas informações ao usuário; (iii) todas as obrigações assumidas pelos suários nestes termos de uso permanecerão em vigor até que sejam sanadas, incluindo, sem se limitar às responsabilidades do usuário.</p>

            <h3>Login e senha</h3>
            
            <p>Ao efetuar seu cadastro, o usuário deverá criar um "nome de usuário" (log-in) e uma senha para acesso à sua conta no website. O log-in e senha são pessoais e intransferíveis e não deverão ser informados pelo usuário a quaisquer terceiros. Caso o usuário tenha conhecimento de que qualquer terceiro teve acesso a seu log-in e senha, ou da ocorrência de qualquer uso ou acesso não autorizado de sua conta no website por terceiros, o usuário deverá solicitar imediatamente a alteração de senha no próprio website, através das configuracoes dentro do perfil do proprio usuario e comunicar o fato à COINBOLD. O usuário será o único responsável pelas operações de compra e venda de criptmoedas efetuadas em sua conta no website.</p>

            <p>Em caso de esquecimento de senha, há um campo no site para reenvio da mesma no e-mail cadastrado (confirmar este campo)</p>
            
            <p>É de  responsabilidade do usuario modificar a senha  periodicamente para também evitar acessos indevidos por terceiros.</p>

            <h3>Compra e venda de bitcoin e outras criptomoedas através do website</h3>
            

            <p>Para comprar ou vender criptomoedas através do website, o usuário deve, previamente, ter efetuado seu cadastro com sucesso e ter aceitado todas as disposições dos presentes termos de uso e de nossa política de privacidade.</p>

            <p>O Usuário que desejar vender suas criptomoedas a outros usuários deverá publicar no website uma ordem de venda, através do preenchimento do formulário disponível no website na opção "ordens". Antes de publicar sua ordem de venda, o usuário deve ter saldo na exchange ou transferido as criptomoedas que pretende vender para sua conta no website. O usuário declara-se ciente de que cada ordem de venda por ele publicada no website representa uma oferta firme de venda das criptomoedas referidos na ordem de venda, pelo preço nela indicado pelo próprio usuário. Assim, o usuário declara-se ciente de que, após a aceitação de sua ordem de venda por outro usuário, a venda das criptomoedas objeto da ordem de venda será automaticamente realizada pelo website e não poderá ser desfeita ou modificada. Uma vez aceita a ordem de venda por qualquer usuário, as criptomoedas objeto da ordem de venda serão automaticamente transferidos para a conta do usuário aceitante. O usuário poderá cancelar qualquer ordem de venda apenas antes de sua aceitação por outro usuário.</p>

            <p>O usuário ou cliente não poderá em qualquer hipótese solicitar ao banco ou instituição financeira o estorno de transferência bancária ou depósito feito em nossa conta, tendo em vista que o valor creditado em nossa conta é enviado instantaneamente para o outro usuário vendedor de criptomoedas, impossibilitando a COINBOLD de cobrir tal ressarcimento, uma vez, que as criptomoedas relativas a esta negociação serão enviados ao usuário comprador. O usuário que desejar comprar criptomoedas de outros usuários deverá publicar no website uma ordem de compra, através do preenchimento do formulário disponível no website na opção "ordens". Antes de publicar sua ordem de compra, o usuário deve ter efetuado a transferência do valor da ordem de compra, em Reais, para a COINBOLD, através das formas de pagamento disponibilizadas no website.</p>

            <p>O usuário declara-se ciente de que cada ordem de compra por ele publicada no website representa uma oferta firme de compra das criptomoedas referidos na ordem de compra, pelo preço nela indicado pelo próprio usuário. Assim, o usuário declara-se ciente de que, após a aceitação de sua ordem de compra por outro usuário, a compra das criptomoedas objeto da ordem de compra será automaticamente realizada pelo website e não poderá ser desfeita ou modificada. Uma vez aceita a ordem de compra por qualquer usuário, o valor, em Reais, da referida compra será automaticamente transferido pelo website para a conta do usuário aceitante. O usuário poderá cancelar qualquer ocompra apenas antes de sua aceitação por outro usuário.</p>

            <p>O usuário deve estar ciente de que qualquer ordem de venda ou ordem de compra por ele publicada poderá ser parcialmente aceita por outro usuário. Assim, por exemplo, se o usuário publica uma ordem de compra de 10 Bitcoins, outro usuário poderá aceitar parcialmente essa ordem e concordar com a venda de 5 Bitcoins pelo preço nela especificado. Nesse caso, o restante da ordem de compra poderá ser cancelado pelo usuário que a publicou, ou ele poderá mantê-la vigente, aguardando que outros usuários também aceitem vender as criptomoedas restantes pelo preço da ordem de compra, até o limite da quantidade remanescente dessa ordem.</p>

            <p>Caso o usuário deseje cancelar uma ordem de venda por ele publicada e ainda não aceita por outro usuário, o usuário emitente da ordem cancelada poderá efetuar, através do próprio website, a retirada de suas criptomoedas e sua transferência para outro local de armazenamento.</p>

            <p>Caso o usuário deseje cancelar uma ordem de compra por ele publicada e ainda não aceita por outro usuário, o usuário emitente da ordem cancelada poderá solicitar, através do próprio website, a devolução do valor em Reais por ele transferido à COINBOLD. Essa devolução será realizada pela COINBOLD necessariamente para uma conta bancária de mesma titularidade do usuário, informada em seu cadastro, descontadas as taxas.</p>

            <p>A COINBOLD não é parte nem deverá ser considerada parte em qualquer das transações de compra e venda realizadas pelos usuários através do website e não será responsável pelo efetivo cumprimento das obrigações assumidas pelos usuários nessas transações.</p>

            <p>O usuário reconhece e aceita que, ao realizar negociações com outros usuários, o faz por sua conta e risco, reconhecendo a COINBOLD apenas como fornecedora de serviços de disponibilização de espaço virtual para negociação entre os usuários. Em nenhum caso, a COINBOLD será responsável por quaisquer perdas, danos, prejuízos ou lucros cessantes que o usuário possa sofrer devido às negociações realizadas ou não realizadas através do website.</p>

            <p>O usuário deve certificar-se de que cadastrou corretamente seus dados bancários para a transferência de valores em Reais e o endereço de seu local de armazenamento de criptomoedas (wallet) para a futura retirada de criptomoedas do website.</p>

            <p>A COINBOLD não se responsabiliza por eventuais erros cometidos pelo usuário no cadastro de seus dados bancários ou endereço de wallet.</p>

            <p>A COINBOLD recomenda que toda transação seja realizada com cautela, bom senso e adequado nível de compreensão pelo usuário que dela participa.</p>

            <p>Double-Spending. É vedado ao usuário emitir mais de uma ordem de venda simultaneamente para a venda da mesma criptomoeda armazenada em sua carteira virtual. A tentativa de utilizar a mesma criptomoeda em duas ou mais transações (double-spending) é proibida e caso seja constatada pela COINBOLD será considerada fraude e acarretará no cancelamento da conta do usuário e esses termos de uso serão automaticamente rescindidos.</p>

            <p>A COINBOLD não é uma consultoria de investimentos, tampouco seus servicos implicam em qualquer tipo de consultoria sobre investimentos e/ou aplicação em criptomoedas, sendo que o usuario o faz por sua conta e risco. A COINBOLD sugere que seus usuarios se informem sobre o funcionamento de criptomoedas, sua valorização, oscilação e formas de investimentos em canais especificos do website COINBOLD ou em outras fontes de informacao.</p>

            <p>É  vedado ao usuário utilizar os serviços para atos,  fins diretos ou  indiretos que desobedeçam leis, regulamentos ou os presentes termos e condicoes;  que atentem contra à moral e aos bons costumes; que tenham  o intuito ou corroborem  com a lavagem de dinheiro;  ou que financie atividades ilícitas e / ou organizações que envolvam terrorismo, máfia, tráfico de drogas, órgãos, pessoas ou animais.</p>
            
            <p>Poderá solicitar, através do próprio website, a devolução do valor em Reais por ele transferido à COINBOLD. Essa devolução será realizada pela COINBOLD necessariamente para uma conta bancária de mesma titularidade do usuário, informada em seu cadastro, descontadas as taxas.</p>

            <p>A COINBOLD não é parte nem deverá ser considerada parte em qualquer das transações de compra e venda realizadas pelos usuários através do website e não será responsável pelo efetivo cumprimento das obrigações assumidas pelos usuários nessas transações.</p>

            <p>O usuário reconhece e aceita que, ao realizar negociações com outros usuários, o faz por sua conta e risco, reconhecendo a COINBOLD apenas como fornecedora de serviços de disponibilização de espaço virtual para negociação entre os usuários. Em nenhum caso, a COINBOLD será responsável por quaisquer perdas, danos, prejuízos ou lucros cessantes que o usuário possa sofrer devido às negociações realizadas ou não realizadas através do website.</p>

            <p>O usuário deve certificar-se de que cadastrou corretamente seus dados bancários para a transferência de valores em Reais e o endereço de seu local de armazenamento de criptomoedas (wallet) para a futura retirada de criptomoedas do website.</p>

            <p>A COINBOLD não se responsabiliza por eventuais erros cometidos pelo usuário no cadastro de seus dados bancários ou endereço de wallet.</p>

            <p>A COINBOLD recomenda que toda transação seja realizada com cautela, bom senso e adequado nível de compreensão pelo usuário que dela participa.</p>
            
            <p>Double-Spending. É vedado ao usuário emitir mais de uma ordem de venda simultaneamente para a venda da mesma criptomoeda armazenada em sua carteira virtual. A tentativa de utilizar a mesma criptomoeda em duas ou mais transações (double-spending) é proibida e caso seja constatada pela COINBOLD será considerada fraude e acarretará no cancelamento da conta do usuário e esses termos de uso serão automaticamente rescindidos.</p>

            <p>A COINBOLD não é uma consultoria de investimentos, tampouco seus servicos implicam em qualquer tipo de consultoria sobre investimentos e/ou aplicação em criptomoedas, sendo que o usuario o faz por sua conta e risco. A COINBOLD sugere que seus usuarios se informem sobre o funcionamento de criptomoedas, sua valorização, oscilação e formas de investimentos em canais especificos do website COINBOLD ou em outras fontes de informacao.</p>

            <p>É  vedado ao usuário utilizar os serviços para atos,  fins diretos ou  indiretos que desobedeçam leis, regulamentos ou os presentes termos e condicoes;  que atentem contra à moral e aos bons costumes; que tenham  o intuito ou corroborem  com a lavagem de dinheiro;  ou que financie atividades ilícitas e / ou organizações que envolvam terrorismo, máfia, tráfico de drogas, órgãos, pessoas ou animais.</p>

            <h3>Taxas e comissões</h3>
            

            <p>Para comprar ou vender criptomoedas através do website, o usuário deve, previamente, ter efetuado seu cadastro com sucesso e ter aceitado todas as O Cadastro no Site é gratuito.</p>

            <p>O Usuário pagará as comissões cobradas pelo website apenas para realizar as seguintes operações:</p>

            <p>(a) Depósito de valores em Reais para compra de criptomoedas. Quando o usuário efetua uma depósito em Reais para compra de criptomoedas através do website não é cobrada nenhuma taxa ou comissão, sendo assim, depósitos em Reais são totalmente grátis.</p>

            <p>(b) Retirada de valores em Reais. Tanto o usuário que vende suas criptomoedas através do website, quanto o usuário que, ao ter depositado valores em Reais, não os utiliza integralmente para a compra de criptomoedas, pagarão comissão ao website para retirada dos valores em Reais. Assim, no momento da retirada, será transferido ao usuário o saldo em Reais, já descontado o valor da comissão.</p>

            <p>(c) Execução de ordem de venda. A publicação de uma ordem de venda de criptomoedas no website é gratuita. No entanto, caso essa ordem de venda seja aceita, total ou parcialmente, por outro usuário, será cobrada uma comissão do usuário que publicou a ordem. Assim, o saldo em reais disponível para retirada por esse usuário será o valor de venda de suas criptomoedas, já descontado o valor da comissão prevista neste item (c) e o valor da comissão prevista no item (b), além disso, ha diferencas de cobranca de taxas entre as ordens ativas e passivas do usuário.</p>

            <p>(d) Execução de ordem de compra. A publicação de uma ordem de compra de criptomoedas no website é gratuita. No entanto, caso essa ordem de compra seja aceita, total ou parcialmente, por outro usuário, será cobrada uma comissão do usuário que publicou a ordem, além disso, ha diferencas de cobranca de taxas entre as ordens ativas e passivas do usuário.</p>

            <p>As solicitações de retirada realizada  pelo usuário podem sofrer atrasos por diversos motivos alheios a boa  vontade da COINBOLD como:  instabilidade na  Plataforma COINBOLD; sobrecarga na  rede Bitcoin;  esgotamento da  hotwallet (a  reposição é  feita manualmente e nesse caso e pode demorar até algumas horas); entre outros.</p>


            <h3>Privacidade</h3>


            <p>Todos os dados pessoais informados pelo usuário no momento de seu cadastro são armazenados em servidores ou meios magnéticos de alta segurança. A COINBOLD adota todas as medidas possíveis para manter a confidencialidade e a segurança das informações prestadas por seus usuários. No entanto, os usuários declaram-se cientes da possibilidade de violação da segurança dos servidores do website por ataques de hackers ou outras circunstâncias a que sistemas conectados à internet estejam sujeitos, casos em que, apesar dos melhores esforços da COINBOLD, as informações e dados dos usuários poderão eventualmente ser ilegalmente acessadas por terceiros, bem como alguma falha operacional da COINBOLD.</p>

            <p>A COINBOLD poderá também revelar dados e informações de qualquer usuário, caso isso seja requerido por lei ou por ordem de uma autoridade competente. Para obter maiores informações sobre a proteção de seus dados pessoais, consulte nossa política de privacidade.</p>

            <h3>Restrições e problemas de acesso</h3>

            <p>Os serviços do website estão disponíveis para acesso exclusivamente através da internet. Para acessar nossos serviços, os usuários deverão estar providos dos equipamentos necessários e apropriados, tais como computador com programa de navegação (browser) devidamente instalado e licenciado, modem, bem como dos serviços de provimento e infraestrutura de acesso à internet.</p>

            <p>Os serviços do website estão sujeitos a interrupções, atrasos e problemas inerentes ao uso da internet. A COINBOLD não será responsável por qualquer dessas interrupções, atrasos ou problemas, nem por eventuais defeitos ou limitações dos equipamentos ou do programa de navegação utilizados pelos usuários ou, ainda, por eventuais defeitos ou limitações dos serviços de provimento de acesso ou infraestrutura de acesso à Internet contratados pelos usuários. A COINBOLD também não será responsável por qualquer vírus que possa atacar o equipamento do usuário em decorrência do acesso, utilização ou navegação na internet ou como consequência da transferência de dados, arquivos, imagens, textos ou áudio.</p>

            <p>A COINBOLD utiliza serviços de terceiros para manter o funcionamento da plataforma (por exemplo, serviços de hospedagem e serviços de meios de pagamento), podendo, portanto, eventualmente ocorrer falhas em tais serviços. A COINBOLD não será responsável por quaisquer perdas, danos (diretos ou indiretos) e lucros cessantes decorrentes de falha dos serviços destes terceiros, mas, na medida do possível, manterá o usuário informado sobre prazos e providências tomadas para sanear referida falha.</p>

            <p>TENDO EM VISTA AS CARACTERÍSTICAS INERENTES AO AMBIENTE DA INTERNET, O USUÁRIO RECONHECE QUE A COINBOLD NÃO SE RESPONSABILIZA PELAS FALHAS NA PLATAFORMA DECORRENTES DE CIRCUNSTÂNCIAS ALHEIAS À SUA VONTADE E CONTROLE, SEJAM OU NÃO OCASIONADAS POR CASO FORTUITO OU FORÇA MAIOR, COMO POR EXEMPLO, INFORMAÇÕES PERDIDAS, INCOMPLETAS, INVÁLIDAS OU CORROMPIDAS; INTERVENÇÕES DE HACKERS E SOFTWARE MALICIOSOS; FALHAS TÉCNICAS DE QUALQUER TIPO, INCLUINDO, FALHAS NO ACESSO OU NA NAVEGAÇÃO DO SITE DECORRENTES DE FALHAS NA INTERNET EM GERAL, QUEDAS DE ENERGIA, MAU FUNCIONAMENTO ELETRÔNICO E/OU FÍSICO DE QUALQUER REDE, INTERRUPÇÕES OU SUSPENSÕES DE CONEXÃO E FALHAS DE SOFTWARE E/OU HARDWARE DO USUÁRIO; PARALISAÇÕES PROGRAMADAS PARA MANUTENÇÃO, ATUALIZAÇÃO E AJUSTES DE CONFIGURAÇÃO DA PLATAFORMA; QUALQUER FALHA HUMANA DE QUALQUER OUTRO TIPO, QUE POSSA OCORRER DURANTE O PROCESSAMENTO DAS INFORMAÇÕES NO SITE, EXIMINDO A COINBOLD, POR CONSEGUINTE, DE QUALQUER RESPONSABILIDADE PROVENIENTE DE TAIS FATOS E/OU ATOS.</p>

            <p>Sujeita a estes termos de uso, a COINBOLD concede ao usuário uma licença limitada, temporária, não exclusiva, não transferível e revogável, para usar a plataforma somente naquilo que seja estritamente necessário para o cumprimento das obrigações e exercício dos direitos previstos nestes termos de uso. É proibido ao usuário ceder, sublicenciar, vender, doar, alienar, alugar, distribuir, transmitir ou transferir os seus direitos obrigações relacionados à plataforma a terceiros, total ou parcialmente, sob quaisquer modalidades, a qualquer título, bem como é vedado criar cópias, digitais ou físicas, modificar, adaptar, traduzir, descompilar, desmontar ou executar engenharia reversa da plataforma, de forma que viole os direitos da COINBOLD.</p>

            <p>Todos os direitos relativos ao website e à plataforma, incluindo suas funcionalidades, são de propriedade exclusiva da COINBOLD, inclusive no que diz respeito aos seus textos, imagens, layouts, códigos, bases de dados, gráficos, artigos, vídeos, fotografias, ilustrações e demais conteúdos produzidos direta ou indiretamente pela COINBOLD (o “conteúdo”). O conteúdo é protegido pela lei de direitos autorais e de propriedade industrial. É proibido usar, copiar, reproduzir, modificar, traduzir, publicar, transmitir, distribuir, executar, exibir, licenciar, vender ou explorar, para qualquer finalidade, o conteúdo, sem o consentimento prévio da COINBOLD, de forma a violar os seus direitos. É expressamente proibida a utilização indevida do conteúdo ou das marcas apresentadas no website</p>

            <p>Qualquer utilização do conteúdo só poderá ser feita mediante prévia e expressa autorização da COINBOLD. O usuário assume toda e qualquer responsabilidade, de caráter civil e/ou criminal, pela utilização indevida e não autorizada do conteúdo. Nenhuma cópia, distribuição, engenharia reversa, exibição ou divulgação dos serviços oferecidos pela plataforma deve ser entendida como restrição ou renúncia dos direitos da COINBOLD sobre o website e a plataforma.</p>

            <p>A COINBOLD, também poderá, a seu exclusivo critério, deixar de comerciar a plataforma ou até mesmo descontinuá-la, a qualquer momento. Nesse caso, a COINBOLD garante que irá comunicar os usuários sobre a sua descontinuidade e manterá a plataforma funcionando pelo período mínimo de 3 (três) meses, contados da data da notificação.</p>

            <p>Todos os anúncios, ofertas, promoções, marcas, textos e conteúdos de terceiros veiculados através do website são de propriedade de seus respectivos titulares. É expressamente proibida a utilização indevida de quaisquer conteúdos ou marcas apresentadas no website.</p>

            <h3>Alteração nos termos de uso</h3>

            <p>A COINBOLD poderá alterar, a qualquer tempo, os presentes termos de uso, visando a seu aprimoramento e à melhoria dos serviços oferecidos pelo website. Eventuais alterações nestes termos de uso entrarão em vigor imediatamente após sua publicação no website. Caso o usuário não concorde com quaisquer alterações implementadas nestes termos de uso, deverá imediatamente cancelar seu cadastro. Caso o usuário não efetue esse cancelamento, ele estará concordando com os novos termos de uso.</p>

            <h3>Disposições finais</h3>

            <p>No caso de descumprimento, pelo usuário, de qualquer disposição dos presentes termos de uso, a COINBOLD poderá declará-lo resolvido em relação a esse usuário, independentemente de qualquer aviso, notificação ou de qualquer outra formalidade, interrompendo, de imediato, o acesso do usuário ao website, sem prejuízo de quaisquer outros direitos assegurados à COINBOLD, por lei ou pelos presentes termos de uso. É parte integrante e inseparável deste termo de uso, e nele se consideram incorporados:</p>

            <p>- Tabela de comissões, prazos e limites das operacoes</p>

            <p>Qualquer dúvida ou solicitação relacionada a estes termos de uso ou aos serviços oferecidos pela plataforma no website deverá ser enviada à COINBOLD por meio do e-mail suporte@coinbold.com.br</p>

            <p>No  caso de  perda do acesso  à senha, como  por exemplo  esquecimento de senha,  ou a necessidade urgente  de retirada  acima do  limite, a COINBOLD  adotará o  procedimento de exclusão da conta do  usuário.  A COINBOLD entrará em contato com  o usuário pelo telefone cadastrado previamente na plataforma e poderá solicitar informações para a verificação das mesmas.  Poderá ainda  ser  solicitado  uma videoconferência com  o usuário. Em seguida  realizará a  retirada  para um  dos endereços  ou contas  bancárias previamente cadastrados. Caso o usuário não tenha mais acesso a conta bancária ou endereço previamente cadastrado na plataforma, será necessário informar um novo  endereço ou conta bancária, que deverá  estar no mesmo país  de origem dos fundos a  serem transferidos, que deve ser válida no  momento da transferência e de titularidade  do usuário. Para validação do novo endereço ou conta bancária serão realizadas extensivas verificações de segurança.</p>

            <p>A COINBOLD não é uma empresa de assessoria financeira de aplicações ou de consultoria de investimentos, mas apenas uma empresa que disponibiliza uma plataforma para aproximar usuários interessados em transacionar criptomoedas entre si. Logo, a COINBOLD não se responsabiliza por quaisquer práticas realizadas pelos usuários por intermédio da plataforma, e decisões tomadas pelos usuários com base nas informações obtidas através do website ou da plataforma, sejam elas de ordem administrativa, negocial, gerencial ou de outra natureza.</p>

            <p>Todas as comunicações realizadas pela COINBOLD com o usuário serão feitas através da plataforma ou do e-mail indicado pelo usuário no momento do cadastro. É dever do usuário deixar os sistemas de anti-spam configurados de modo que não interfiram no recebimento dos comunicados da COINBOLD. A responsabilidade pelo recebimento e visualização dos comunicados é exclusiva do usuário. A COINBOLD não se responsabiliza por comunicados enviados pela própria COINBOLD e ignorados ou não recebidos pelos usuários em decorrência do mau uso da ferramenta de seu e-mail.</p>

            <p>Os presentes termos de uso são regidos pelas leis da República Federativa do Brasil e fica eleito o foro da Capital do Estado de São Paulo, com exclusão de qualquer outro, por mais privilegiado que seja ou possa vir a ser, para dirimir quaisquer litígios ou controvérsias oriundas dos presentes termos de uso. O USUÁRIO RECONHECE QUE AS PECULIARIDADES DE USO DO SITE E DOS SERVIÇOS NELE DISPONIBILIZADOS FORAM SUFICIENTEMENTE DESCRITAS NESTE TERMO E QUE A COINBOLD CUMPRIU DEVIDAMENTE O DEVER DE INFORMAÇÃO. APÓS LER TODAS AS CONDIÇÕES QUE REGULAM O USO DO SITE E DE SEUS SERVIÇOS, O USUÁRIO CONSENTE COM ESTES TERMOS DE USO E ACEITA TODAS AS SUAS DISPOSIÇÕES.</p>


        </div>
    </div>
    
@endsection