<meta name="description" content="A Coinbold Exchange é um mercado de compra e venda de moedas digitais. Trabalhamos com Bitcoin, Bitcoin Cash, Ethereum, Litecoin e Ripple, sempre respeitando os melhores padrões de segurança das transações e dados. Porém, vamos além de apenas garantir um ambiente seguro e conveniente para nossos investidores.">

<meta property="og:url" content="{{url()->full()}}">
<meta property="og:type" content="website">
<meta property="og:title" content="@yield('title') Coinbold Exchange - Conhecimento gera confiança">
<meta property="og:image" content="{{url('images/icon-open.png')}}">
<meta property="og:description" content="A Coinbold Exchange é um mercado de compra e venda de moedas digitais. Trabalhamos com Bitcoin, Bitcoin Cash, Ethereum, Litecoin e Ripple, sempre respeitando os melhores padrões de segurança das transações e dados. Porém, vamos além de apenas garantir um ambiente seguro e conveniente para nossos investidores.">
<meta property="og:site_name" content="Coinbold Exchange">
<meta property="og:locale" content="pt_BR">
<meta property="og:image:width" content="200">
<meta property="og:image:height" content="200">


<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@coinbold">
<meta name="twitter:url" content="{{url()->full()}}">
<meta name="twitter:title" content="@yield('title') Coinbold Exchange">
<meta name="twitter:description" content="A Coinbold Exchange é um mercado de compra e venda de moedas digitais. Trabalhamos com Bitcoin, Bitcoin Cash, Ethereum, Litecoin e Ripple, sempre respeitando os melhores padrões de segurança das transações e dados. Porém, vamos além de apenas garantir um ambiente seguro e conveniente para nossos investidores.">
<meta name="twitter:image" content="{{url('images/icon-open.png')}}">

<!-- Launch Icon (180x180px or larger) -->
<link rel="apple-touch-icon" href="{{url('images/icon-open.png')}}">

<!-- Launch Screen Image -->
<link rel="apple-touch-startup-image" href="{{url('images/icon-open.png')}}">
