<!DOCTYPE html>
<html>
    <head>            
        <meta charset="utf-8"/>
        <link rel="shortcut icon" type="image/x-icon" href="{{url('landing/favicon.ico')}}" />
        <link rel="shortcut icon" type="image/x-icon" href="{{url('landing/favicon.png')}}" />
        
        <title>@yield('title') Coinbold exchange</title>
        @include("landing.social")
        
        <meta name="robots" content="index,follow"><!-- All Search Engines -->
        <meta name="googlebot" content="index,follow"><!-- Google Specific -->        

        <meta name="_token" content="{!! csrf_token() !!}" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="{{url('landing/css/style.css')}}">
        <script src="{{url('landing/js/jquery.js')}}"></script>
        <script src="{{url('landing/js/fixar.js')}}"></script>
    </head>
    <body>
        
        <div id="site">
            @yield("content")
        </div>
        
        <div id="rodape">
                <div id="rodapeRedesSociais">
                    <h2>REDES SOCIAIS</h2>
                    <ul>
                        <li><a href="#"><img width="40" height="40" src="{{url('landing/img/icoLinkedIn.png')}}"/>Linkedin</a></li>
                        <li><a href="#"><img width="40" height="40" src="{{url('landing/img/icoYouTube.png')}}"/>YouTube</a></li>
                    </ul>
                </div>
                <div id="rodapeEmpresa">
                    <h2>EMPRESA</h2>
                    <ul>
                        <li><a href="{{url('quem-somos')}}">Quem somos</a></li>
                        <!--<li><a href="#">FAQ</a></li>
                        <li><a href="#">Fórum</a></li>-->
                    </ul>
                </div>
             <!--   <div id="rodapeTecnologia">
                    <h2>TECNOLOGIA</h2>
                    <ul>
                        <li><a href="#">Avisos</a></li>
                        <li><a href="#">Recrutamento</a></li>
                        <li><a href="#">Segurança</a></li>
                    </ul>
                </div>-->
                <div id="rodapeDocumentosLegais">
                    <h2>DOCUMENTOS LEGAIS</h2>
                    <ul>
                        <!--<li><a href="#">Certificações</a></li>
                        <li><a href="#">Política de privacidade</a></li>-->
                        <li><a href="{{url('termos')}}">Termos e condições</a></li>
                    </ul>
                </div>
            </div>
        </div>
        @stack("scripts")

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script src="https://www.googletagmanager.com/gtag/js?id=UA-118776298-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
        
            gtag('config', 'UA-118776298-1');
        </script>
    </body>
</html>        
