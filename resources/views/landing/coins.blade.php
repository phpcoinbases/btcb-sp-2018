<?php setlocale(LC_MONETARY, 'pt_BR'); ?>
<!--COINS-->
@inject('coins', 'App\Services\CoinService')
    <div id="bitcoinBCH">
        <p class="nomeCotacoes">Bitcoin Cash</p>
        <img class="imgCotacoes" width="48" height="48" src="{{url('landing/img/icoBitcoinBCH.png')}}" />        
        <p class="valorCotacoes">R$ {{number_format($coins->getCoin("BCH",1)->prices[0]->price_brl, 2, ',', '.')}} </p>
    </div>    
    <div id="ethereum">
        <p class="nomeCotacoes">Ethereum</p>
        <img class="imgCotacoes" width="48" height="48" src="{{url('landing/img/icoEthereum.png')}}" />        
        <p class="valorCotacoes">R$ {{number_format($coins->getCoin("ETH",1)->prices[0]->price_brl, 2, ',', '.')}} </p>
    </div>
    <div id="litecoin">
        <p class="nomeCotacoes">Litecoin</p>
        <img class="imgCotacoes" width="48" height="48" src="{{url('landing/img/icoLitecoin.png')}}" />        
        <p class="valorCotacoes">R$ {{number_format($coins->getCoin("LTC",1)->prices[0]->price_brl, 2, ',', '.')}}</p>
    </div>
    
    <div id="ripple">
        <p class="nomeCotacoes">Ripple</p>
        <img class="imgCotacoes" width="48" height="48" src="{{url('landing/img/icoRipple.png')}}" />        
        <p class="valorCotacoes">R$ {{number_format($coins->getCoin("XRP",1)->prices[0]->price_brl, 2, ',', '.')}} </p>
    </div>

<!--COINS-->
