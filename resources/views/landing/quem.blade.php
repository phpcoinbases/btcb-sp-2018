@extends("landing.layout")
@section('title', 'Quem somos')
@section("content")

    <div id="cabecalhoQuemSomos" class="cabecalho">
        <div id="logo"><a href="{{route('home')}}"><img width="206" height="89" src="{{url('landing/img/coinbold.png')}}" /></a></div>
        <div id="menuMobile"><a id="linkMenuMobile" href="#"><img src="{{url('landing/img/icoMenu.png')}}" width="40" height="59"></a></div>
        <div id="menu">
            <ul>
                <li><a href="{{route('landing.quem')}}">Quem somos</a></li>
                <li class="separadorMenu">•</li>
                <li><a href="{{route('landing.termos')}}">Termos e condições</a></li>
                <!--<li class="separadorMenu">•</li>
                <li><a href="">Segurança</a></li>
                <li>•</li>
                <li><a href="">Mais</a></li>-->
            </ul>
        </div>
        <!--<div id="ctaLogin"><a href="#">Login</a></div>-->
    </div>
    
    
    <div id="bannerQuemSomos">
        <h2>Quem Somos <br><span>BE BOLD</span></h2>        
    </div>
    
    <div id="qs">
        <div id="bgBranco">
            <div>
                <img src="{{url('landing/img/icoSeguranca.png')}}">
                <p>A Coinbold Exchange é um mercado de compra e venda de moedas digitais. Trabalhamos com Bitcoin, Bitcoin Cash, Ethereum, Litecoin e Ripple, sempre respeitando os melhores padrões de segurança das transações e dados. Porém, vamos além de apenas garantir um ambiente seguro e conveniente para nossos investidores.</p>
            </div> 
            <div>
                <img src="{{url('landing/img/icoProfissionalismo.png')}}">
                <p>Como parte de nossa filosofia de que conhecimento gera confiança, oferecemos cotações atualizadas, curadoria de notícias e análises fundamentalistas das criptomoedas disponíveis na Coinbold Exchange.</p>
            </div> 
            <div>
                <img src="{{url('landing/img/icoAgilidade.png')}}">
                <p>Com estas informações, facilidade de transação e as melhores taxas do mercado, atendemos as necessidades de todos os perfis de investidores, dos iniciantes aos <i>traders</i> mais experientes que buscam uma plataforma avançada para trabalhar com diversas criptomoedas.</p>
            </div> 
        </div>
    </div>

    @endsection