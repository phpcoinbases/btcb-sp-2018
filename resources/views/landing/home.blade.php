@extends("landing.layout")

@section("content")
            <div id="cabecalho" class="cabecalho">
                <div id="logo"><img width="206" height="89" src="{{url('landing/img/coinbold.png')}}" /></div>
                <div id="menuMobile"><a id="linkMenuMobile" href="#"><img src="{{url('landing/img/icoMenu.png')}}" width="40" height="59"></a></div>
                <div id="menu">
                    <ul>
                        <li><a href="{{route('landing.quem')}}">Quem somos</a></li>
                        <li class="separadorMenu">•</li>
                        <li><a href="{{route('landing.termos')}}">Termos e condições</a></li>
                        <!--
                            <li class="separadorMenu">•</li>
                            <li><a href="">Segurança</a></li>
                            <li>•</li>
                            <li><a href="">Mais</a></li>
                        -->
                    </ul>
                </div>
                <!--<div id="ctaLogin"><a href="#">Login</a></div>-->
            </div>
            <div id="banner1" style="overflow:hidden">
                <h2>Conhecimento gera Confiança <br><span>BE BOLD</span></h2>
                <div id="bitcoinBTC">
                    <p class="nomeCotacoes"><span>Bitcoin</span></p>
                    <img class="imgCotacoes" width="68" height="68" src="{{url('landing/img/icoBitcoinBTC.png')}}" />
                    @inject('coins', 'App\Services\CoinService')
                    <p class="valorCotacoes valorBTCajax" style="color:white">R$ {{number_format($coins->getCoin("BTC",1)->prices[0]->price_brl, 2, ',', '.')}}</p>
                </div>
                
                <div id="ctrCadastreSe"><a href="#contato">Cadastre-se agora!</a></div>
            </div>
            <div id="cotacoes">
                @include("landing.coins")
            </div>
            <div id="mailing">
                <form id="mailform">
                    Deixe seu e-mail para <span>mais informações</span>
                    <input type="text" id="mail-email" name="email" placeholder="EMAIL:">
                    <input type="submit" id="sendUser" value="OK">
                </form>
            </div>
            <div id="gadgets">
                <h2>Informe-se e acesse sua carteira de criptomoedas sempre que necessário</h2>
            </div>
            <div id="noticias" style="height:auto !important">
                    <div id="bgBranco">
                        <h2>Últimas Notícias</h2>
                        @inject("news","App\Models\Magazine")
                        @foreach($news->orderBy("id","DESC")->limit(5)->get() as  $key =>  $new)
                            <div id="noticia1" class="noticias @if($loop->iteration  % 2 == 0) noticiaRight @else noticiaLeft @endif ">
                                <div class="noticiaImg">
                                    @if($loop->iteration  % 2 == 0)
                                        <img src="{{url('landing/img/noticia1.png')}}" /> 
                                    @else 
                                        <img src="{{url('landing/img/noticia2.png')}}" />
                                    @endif                                    
                                </div>                                
                                <div class="noticiasInfo">
                                    <p>{{$new->description}}</p>
                                    <a href="{{$new->link}}">ACESSE</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            <!--        
            <div id="cadastro">
                <h2>Começe a negociar <span>agora mesmo</span>!</h2>
                <p>Com confiança, segurança e profissionalismo.</p>
                <a href="#contato">Cadastre-se</a>
            </div> 
             --> 
            <div id="qualidades">
                <div id="qualidadeSeguranca">
                    <img src="{{url('landing/img/icoSeguranca.png')}}" />
                    <h3>Segurança</h3>
                    <p>A segurança, privacidade dos nossos clientes e de suas carteiras são uma das nossas principais preocupações, sendo garantidas por tecnologias de ponta e certificadas por padrões nacionais e internacionais.</p>
                </div>
                <div id="qualidadeAgilidade">
                    <img src="{{url('landing/img/icoAgilidade.png')}}" />
                    <h3>Agilidade</h3>
                    <p>Usamos uma plataforma própria desenvolvida com o objetivo de gerar transações ágeis e eficazes, reduzindo o tempo de processamento de depósitos, saques, ordens de compra e venda.</p>
                </div>
                <div id="qualidadeMobile">
                    <img src="{{url('landing/img/icoMobile.png')}}" />
                    <h3>Mobile</h3>
                    <p>Nossa plataforma pode ser acessada por qualquer dispositivo, facilitando operações a qualquer momento. Por consequência, nossos clientes podem aproveitar as oportunidades de mercado com maior conveniência.</p>
                </div>
                <div id="qualidadeProfissionalismo">
                    <img src="{{url('landing/img/icoProfissionalismo.png')}}" />
                    <h3>Profissionalismo</h3>
                    <p>Com dinheiro não se brinca. Por isso, trabalhamos com SLAs rígidos baseados em termos e condições objetivos. Desta forma, nossa equipe garante o profissionalismo e seriedade no relacionamento com nossos usuários.</p>
                </div>
            </div>
            <div id="contato">
                <h2><span>Entre em contato</span> com a gente!</h2>
                <p>E entenda mais sobre as vantagens das criptomoedas.</p>
                <form id="contact">
                    <input type="text" id="firstname" name="firstname" placeholder="nome">
                    <input type="text" id="lastname" name="lastname" placeholder="sobreNome:">
                    <input type="text" id="phone" name="phone" placeholder="Email:">
                    <input type="text" id="cel" name="cel" placeholder="celular:">
                    <textarea id="msg" name="msg">mensagem:</textarea>
                    <input type="submit" id="sendContact" value="Enviar">
                </form>
            </div>
@endsection
@push("scripts")
<script>
    var i=0;
    

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    $(document).ready(function(){


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        window.setInterval(function(){
            $.ajax({
                type: "GET",
                url: "{{route('landing.get.coins')}}",                        
                success: function (data) {
                    $(".coinslist").empty().html(data);
                }
            });
            $.ajax({
                type: "GET",
                url: "{{route('landing.get.bitcoin')}}",                        
                success: function (data) {
                    $(".valorBTCajax").empty().html(data);
                }
            });

        }, 10000);


        $("#msg").blur(function(){
            if ($("#msg").val()==""){
                $("#msg").val("mensagem:");
            }
        });

        $("#msg").focus(function(){
            if ($("#msg").val()=="mensagem:"){
                $("#msg").val("");
            }
        });
        $("#sendContact").click(function (e) {
            e.preventDefault()
            
            if ($("#msg").val()==""){
                $("#msg").val("mensagem:");
            }

            if (!validateEmail($("#phone").val())){
                alert("Insira um e-mail valido!");
                return;
            }


            if (($("#firstname").val()=="") || ($("#lastname").val()=="")){
                alert("Preencha o nome e sobrenome.");
                return;
            }

            $.ajax({
                type: "POST",
                url: "{{route('landing.save.contact')}}",
                data: {
                    firstname:$("#firstname").val(),
                    lastname:$("#lastname").val(),
                    phone:$("#phone").val(),
                    cel:$("#cel").val(),
                    msg:$("#msg").val(),
                },
                dataType:"json",
                success: function (data) {
                    if (data.firstname!=""){
                        alert("Dados recebidos com sucesso!");
                        $("#firstname").val("");
                        $("#lastname").val("");
                        $("#phone").val("");
                        $("#cel").val("");
                        $("#msg").val("mensagem:");
                    }
                }
            });
        });

        $("#sendUser").click(function (e) {
            e.preventDefault()
            if (!validateEmail($("#mail-email").val())){
                alert("Insira um e-mail valido!");
                return;
            }
            $.ajax({
                type: "POST",
                url: "{{route('landing.save.user')}}",
                data: {email:$("#mail-email").val()},
                dataType:"json",
                success: function (data) {
                    if (data.email!=""){
                        alert("Dados recebidos com sucesso!");
                        $("#mail-email").val("");
                    }
                }
            });
        });
    });

</script>
@endpush
