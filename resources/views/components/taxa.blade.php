<tr>
    <td>{{$taxa}}</td>
    <td><span class="badge bg-red">{{number_format($tax->get($type)->our_tax,4)}}{{$unit or "%"}} </span></td>
    <td><span class="badge bg-red">R$ {{money_format('%.2n',$tax->get($type)->fixed)}} </span></td>
</tr>   