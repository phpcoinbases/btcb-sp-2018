<div class="row">
    <div class="col-md-12">
          
        <!-- /.box -->
        @inject('cointable', 'App\Cryptocoin')
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Valores das Criptomoedas</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body padding">
                <ul class="list-inline">
                @forelse ($cointable::with("prices")->get() as $coin)
                    <li  class="text-center">
                        <p class="text-center"><i class="cc {{$coin->symbol}}"></i></p>
                        <p class="text-center">{{$coin->name}}</p>                        
                    </li>
                @empty
                    <tr>
                            <td>1.</td>
                    </tr>
                @endforelse
             </ul>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </div>