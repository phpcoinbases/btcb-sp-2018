@inject("wallets","App\Services\WalletService")

        <div class="grid simple">
                <div class="grid-title no-border">
                  <h4>Suas <span class="semi-bold">Carteiras</span></h4>
                  <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="#grid-config" data-toggle="modal" class="config"></a>
                    <a href="javascript:;" class="reload"></a>
                    <a href="javascript:;" class="remove"></a>
                  </div>
                </div>
                <div class="grid-body no-border">
                  
                    <h5 class="widget-user-desc">Dinheiro / Bitcoin / Ethereum</h5>
                
                    <div class="box-footer no-padding">
                        <ul class="nav nav-stacked">                    
                            <table class="col-md-12">
                                <tr>
                                    <td style="width:20%">Moeda</td>
                                    <td style="width:70%">Seu Endereço</td>
                                    <td style="width:10%">Saldo</td>
                                </tr>
                                <tr>
                                    <td>Dinheiro</td>
                                    <td></td>
                                    <td>R$ {{ $wallets->getBalance() }}</td>
                                </tr>
                                <tr>
                                    <td>Bitcoin</td>
                                    <td>{{ $wallets->getBTC()->getWallet() }}</td>
                                    <td>{{ $wallets->getBTC()->getBalance() }}</td>
                                </tr>
                                <tr>
                                    <td>Ethereum</td>
                                    <td>{{ $wallets->getETH()->getWallet() }}</td>
                                    <td>{{ $wallets->getETH()->getBalance() }}</td>
                                </tr>
                                <tr>
                                    <td>Bitcoin Cash</td>
                                    <td>{{ $wallets->getBCH()->getWallet() }}</td>
                                    <td>{{ $wallets->getBCH()->getBalance() }}</td>
                                </tr>
                                <tr>
                                    <td>Litecoin</td>
                                    <td>{{ $wallets->getLTC()->getWallet() }}</td>
                                    <td>{{ $wallets->getLTC()->getBalance() }}</td>
                                </tr>
                                <tr>
                                    <td>Rippled</td>
                                    <td>{{ $wallets->getXRP()->getWallet() }}</td>
                                    <td>{{ $wallets->getXRP()->getBalance() }}</td>
                                </tr>                            
                            </table>
                        </ul>
                    </div>
                </div>
</div>