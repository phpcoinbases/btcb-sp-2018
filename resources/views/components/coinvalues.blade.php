<div class="col-md-4 col-sm-6 m-b-10">
  <div class="tiles {{($coins->getCoin($symbol,1)->prices[0]->percent_change_1h>=0) ? "green" : "red"}} ">
    <div class="tiles-body">      
      <div class="tiles-title"> {{$coins->getCoin($symbol,1)->name}} <i class="{{($coins->getCoin($symbol,1)->prices[0]->percent_change_1h>=0) ? "icon-custom-up" : "icon-custom-down"}}"></i>
        <span class="text-white mini-description ">&nbsp; {{($coins->getCoin($symbol,1)->prices[0]->percent_change_1h)}}% 
          <span class="blend"></span>
        </span></div>
      <div class="heading"> U$ <span class="animate-number" data-value="{{money_format('%.2n',$coins->getCoin($symbol,1)->prices[0]->price_usd)}}" data-animation-duration="1000">U$ {{money_format('%.2n',$coins->getCoin($symbol,1)->prices[0]->price_usd)}}</span> </div>
      <div class="progress transparent progress-small no-radius">
        <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="100%" style="width: 100%;"></div>
      </div>
      <div class="description">
        R$ {{money_format('%.2n',$coins->getCoin($symbol,1)->prices[0]->price_brl)}}        
      </div>
    </div>
  </div>
</div>
<!---->