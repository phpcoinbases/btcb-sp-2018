@extends('layouts.main')

@section('content')
  <section class="content-header">
      <h1>
          Bem vindo
          <small>Extrato</small>
      </h1>
      <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Extrato</li>
      </ol>
  </section>

  <section class="content">

    <div class="row">
        <div class="col-md-12">
            <ul class="timeline">
                    @isset($extratos)
                        @php 
                            $last_date = null;
                        @endphp
                        
                        @foreach($extratos as $extract)
                            @if($last_date !== \Carbon\Carbon::parse($extract->created_at)->format('d/m/Y'))
                                <li class="time-label">
                                    <span class="bg-red">
                                            {{ \Carbon\Carbon::parse($extract->created_at)->format('d/m/Y')}}                                        
                                    </span>
                                </li>
                                @php
                                    $last_date = \Carbon\Carbon::parse($extract->created_at)->format('d/m/Y');
                                @endphp                                 
                            @endif
                            <!-- /.timeline-label -->
                        
                            <!-- timeline item -->
                            <li>
                                @php
                                    $icon = null;
                                    $status = null;
                                @endphp    
                                <!-- timeline icon -->
                                @if(isset($extract->withdraw))
                                    @if($extract->withdraw->status=="pending")
                                        @php $icon = "fa-clock-o bg-blue"; $status = "Pendente"; @endphp
                                    @elseif ($extract->withdraw->status=="active")
                                        @php $icon = "fa-check bg-green"; $status = "Aprovado"; @endphp
                                    @else
                                        @php $icon = "fa-check bg-red"; $status = "Cancelado"; @endphp
                                    @endif
                                @elseif(isset($extract->deposit))
                                    @if($extract->deposit->status=="pending")
                                        @php $icon = "fa-clock-o bg-blue";$status = "Pendente"; @endphp
                                    @elseif ($extract->deposit->status=="active")
                                        @php $icon = "fa-check bg-green";$status = "Aprovado"; @endphp
                                    @else
                                        @php $icon = "fa-close bg-red";$status = "Cancelado"; @endphp
                                    @endif
                                @elseif(isset($extract->orders))
                                    
                                    @if($extract->orders->status=="pending")
                                        @php $icon = "fa-clock-o bg-blue";$status = "Pendente"; @endphp
                                    @elseif ($extract->orders->status=="active")
                                        @php $icon = "fa-check bg-green";$status = "Aprovado"; @endphp
                                    @else
                                        @php $icon = "fa-close bg-red";$status = "Cancelado"; @endphp
                                    @endif
                                @endif    
                                <i class="fa {{$icon}}"></i>
                                <div class="timeline-item">
                                    <span class="time"><i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse($extract->created_at)->format('H:i')}} </span>
                        
                                    <h3 class="timeline-header"><a href="#">{{$extract->type}}</a> - <small>{{$status}}</small></h3>
                        
                                    <div class="timeline-body">                                        
                                        @if($extract->type=="Saque")
                                            @if($extract->withdraw->type=="btc")
                                                Transferencia de {{$extract->value}} BTC para {{$extract->withdraw->address}}
                                            @elseif($extract->withdraw->type=="eth")
                                                Transferencia de {{$extract->value}} ETH para {{$extract->withdraw->address}}                                            
                                            @else
                                                Saque de R$ {{$extract->value}} para {{$extract->withdraw->agency}} - {{$extract->withdraw->account}}
                                            @endif
                                        @elseif($extract->type=="Deposito")
                                            @if($extract->deposit->type=="ted")
                                                Deposito no valor R$ {{$extract->value}}
                                            @else

                                            @endif
                                        @elseif(isset($extract->orders))
                                            <p> Ordem de {{$extract->type}}</p>
                                            <p> Moeda  {{$extract->cryptocoin->name}}</p>
                                            <p> Valor Unitario de R$ {{$extract->orders->value}}</p>
                                            <p> Quantidade {{$extract->orders->qntd}}</p>
                                        @endif
                                    </div>
                        
                                   
                                </div>
                            </li>
                            <!-- END timeline item -->
                        @endforeach
                    @endisset
                    
                    @empty($extratos)
                      
                    @endempty
                        
                    
                    
            </ul>
    
        </div>
    </div>

  </section>

@endsection