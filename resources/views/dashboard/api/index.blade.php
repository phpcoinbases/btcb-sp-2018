@extends('dashboard.layouts.main')

@push("body-class") horizontal-menu @endpush

@section('content')
  @include("dashboard.partials.head")

  <div class="page-container row-fluid">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="page-content">
          @include("dashboard.partials.menu") 
          <div class="clearfix"></div>
          <div class="content">
            <ul class="breadcrumb">
              <li>
                <p>VOCÊ ESTÁ AQUI</p>
              </li>
              @each('admin.partials.breadcrumb', [['name'=>'Home','active'=>true,'route'=>route('admin.index')]], 'list')
            </ul>
            <div class="page-title"> <i class="icon-custom-left"></i>
              <h3>API - <span class="semi-bold">Acesso Remoto a seus dados</span></h3>
            </div>

           
            <div class="row">
                <div class="col-md-12">
                  <div class="grid simple">
                    <div class="grid-body no-border">
                      <br/>
                      <div class="row">
                        <form action="{{route("generate.access")}}" method="post">
                          @csrf
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                              <label class="form-label">Nome da chave</label>
                              <span class="help">e.g. "Chave para Robô"</span>
                              <div class="controls">
                                <input type="text" name="app_name" required class="form-control">
                              </div>
                            </div>
                            <div class="row-fluid">
                                <label class="form-label">Permissões da chave:</label>
                                <div class="checkbox check-primary checkbox-circle">
                                  <input id="checkbox" name="scope" type="checkbox" value="1">
                                  <label for="checkbox">Permitir a chave gerar Ordens de Compra e Venda</label>
                                </div>                              
                            </div>
                            <div class="row text-right">
                                <input type="submit" class="btn btn-danger btn-cons btn-add" value="Gerar Chave">
                            </div>  
                          </div>
                        </form>
                      </div>
                      <div class="row">
                          @if ( session('accessToken') )
                          <div class="alert alert-block alert-error fade in">
                              <button type="button" class="close" data-dismiss="alert"></button>
                              <h4 class="alert-heading"><i class="icon-warning-sign"></i> CUIDADO! Sua chave de acesso gera total controle de suas carteiras</h4>
                              <p> COPIE E GUARDE <a href="#" class="link">AccessToken:</a> 
                              <div style="word-wrap:break-word">{{session('accessToken')}}</div>
                              </p>
                              
                            </div>
                          @endif  
                      </div>
                    </div>
                  </div>
                </div>
              </div>                
      
    

    

@endsection