@extends('dashboard.layouts.main')

@push("body-class") horizontal-menu @endpush

@section('content')
  @include("dashboard.partials.head")

  <div class="page-container row-fluid">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="page-content">
          @include("dashboard.partials.menu") 
          <div class="clearfix"></div>
          <div class="content">
            <ul class="breadcrumb">
              <li>
                <p>VOCÊ ESTÁ AQUI</p>
              </li>
              @each('admin.partials.breadcrumb', [['name'=>'Home','active'=>true,'route'=>route('admin.index')]], 'list')
            </ul>
            <div class="page-title"> <i class="icon-custom-left"></i>
              <h3>Dashboard - <span class="semi-bold">Informações</span></h3>
            </div>
            @include("components/wallets")
            <div class="row">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4>Enviar <span class="semi-bold"> comprovante de Deposito</span></h4>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>

                        </div>
                    </div>
                    <div class="grid-body no-border"> 
                      <div class="grid-title no-border">
                        @if(session()->has('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div>
                        @endif                   
                        <div class="tab-pane active" id="settings">
                            {!! Form::open(array('route' => 'deposit.upload',"class"=>"form-horizontal",'files'=>true)) !!}
                                {{ csrf_field() }}
                                {{-- COMPROVANTE TED--}}
                                @include("layouts.subviews.inputs.file",["name"=>"comprovante","label"=>"Comprovante TED","data"=>""])
                                {{-- VALOR COMPROVANTE TED--}}
                                @include("layouts.subviews.inputs.text",["name"=>"value","label"=>"Valor do TED","data"=>""])
                                
                                
                              <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                  <button type="submit" class="btn btn-success">Salvar</button>
                                </div>
                              </div>
                          </form>
                        </div>
                        <!-- /.tab-pane -->
                      </div>
              <!-- /.tab-content -->
                    </div>
            <!-- /.nav-tabs-custom -->
                </div>

            </div>
          </div>

  </section>

@endsection