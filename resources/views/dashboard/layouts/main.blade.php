<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <title>@yield('title') Coinbold</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.11/css/all.css" integrity="sha384-p2jx59pefphTFIpeqCcISO9MdVfIm4pNnsL08A6v5vaQc4owkQqxMV8kg4Yvhaw/" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> 
        <link rel="stylesheet" href="{{ url('/css/fa-regular.min.css') }}">
        <link rel="stylesheet" href="{{ url('/css/fa-brands.min.css') }}">
        <link rel="stylesheet" href="{{ url('/css/fa-solid.min.css') }}">
        <link rel="stylesheet" href="{{ url('/css/cryptocoins.css') }}">
        <link rel="stylesheet" href="{{ url('/css/cryptocoins-colors.css') }}">
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
        <script>
                window.Laravel = {!! json_encode([
                    'auth' => auth()->check(),
                    'csrfToken' => csrf_token(),
                    'locale' => app()->getLocale(),
                    'app_url' =>env("APP_URL"),
                    'json_get_wallets'=>route("json.get.wallets"),
                ]); !!}
        </script>
        </head>

    </head>
    <body class="@stack('body-class')">           
            @include("dashboard.partials.head")
            <div class="page-container row-fluid">
                <div class="row">
                    @if(Auth::check())
                        @include("dashboard.partials.menu")
                    @endif
                    @yield('content')
                </div>
            </div>
        </div>
        @stack('pre-scripts')  
        <script src="{{ mix('js/app.js') }}"></script>
        <!-- scripts Contents -->
        @stack('scripts')       
    
    </body>
</html>
