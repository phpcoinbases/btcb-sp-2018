@extends('dashboard.layouts.main')

@push("body-class") horizontal-menu @endpush

@section('content')


  <div class="page-container row-fluid">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="page-content">

          <div class="clearfix"></div>
          <div class="content">
            <ul class="breadcrumb">
              <li>
                <p>VOCÊ ESTÁ AQUI</p>
              </li>
              @each('admin.partials.breadcrumb', [['name'=>'Perfil','active'=>true,'route'=>route('perfil')]], 'list')
            </ul>
            <div class="page-title"> <i class="icon-custom-left"></i>
              <h3>Perfil do <span class="semi-bold">Usuario</span></h3>
            </div>

    @inject('metrics', 'App\Services\UserService')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-heading"><strong>Analise e limites do meu Perfil</strong></div>
                <div class="panel-body">
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                          <tbody><tr>
                            <th style="width: 10px">#</th>
                            <th>Tarefas</th>
                            <th>Tipo de conta</th>
                            <th style="width: 40px">Status</th>
                          </tr>
                          <tr>
                            <td>0.</td>
                            <td>Cadastro no site</td>
                            <td><span class="badge bg-black">Light</span></td>
                            <td><span class="badge bg-green">OK</span></td>
                          </tr>
                          <tr>
                            <td>1.</td>
                            <td>Validação do CPF/CNPJ feita por TED</td>
                            <td><span class="badge bg-yellow">Light X</span></td>
                            @if(!$metrics->type()["cpf_cnpj"])
                              <td><span class="badge bg-red">x</span></td>
                            @else
                              <td><span class="badge bg-green">OK</span></td>
                            @endif
                          </tr>
                          <tr>
                            <td>2.</td>
                            <td>Confirmação do E-mail</td>
                            <td><span class="badge bg-yellow">Light X</span></td>
                            @if(!$metrics->type()["confirmed"])
                              <td><span class="badge bg-red">x</span></td>
                            @else
                              <td><span class="badge bg-green">OK</span></td>
                            @endif
                          </tr>
                          <tr>
                            <td>3.</td>
                            <td>Comprovante de Endereço</td>
                            <td><span class="badge bg-yellow">Light X</span></td>
                            @if(!$metrics->type()["comprovante"])
                              <td><span class="badge bg-red">x</span></td>
                            @else
                              <td><span class="badge bg-green">OK</span></td>
                            @endif
                          </tr>
                          <tr>
                            <td>4.</td>
                            <td>Documento com Foto</td>
                            <td><span class="badge bg-yellow">Light X</span></td>
                            @if(!$metrics->type()["document_file"])
                              <td><span class="badge bg-red">x</span></td>
                            @else
                              <td><span class="badge bg-green">OK</span></td>
                            @endif
                          </tr>
                          <tr>
                            <td>5.</td>
                            <td>Foto Selfie</td>
                            <td><span class="badge bg-purple">Pro</span></td>
                            @if(!$metrics->type()["perfil_file"])
                              <td><span class="badge bg-red">x</span></td>
                            @else
                              <td><span class="badge bg-green">OK</span></td>
                            @endif
                          </tr>  
                          <tr>
                            <td>6.</td>
                            <td>Comprovante origem dos recurso</td>
                            <td><span class="badge bg-purple">Pro</span></td>
                            @if(!$metrics->type()["origem"])
                              <td><span class="badge bg-red">x</span></td>
                            @else
                              <td><span class="badge bg-green">OK</span></td>
                            @endif
                          </tr>                          
                          <tr>
                            <td>7.</td>
                            <td>Ata de deliberação</td>
                            <td><span class="badge bg-purple">Pro</span></td>
                            @if(!$metrics->type()["ata"])
                              <td><span class="badge bg-red">x</span></td>
                            @else
                              <td><span class="badge bg-green">OK</span></td>
                            @endif
                          </tr> 
                        </tbody></table>
                      </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-heading"><strong>Meu perfil</strong></div>
               <div class="panel-body">
                <div class="tab-pane active" id="settings">
                    {!! Form::open(array('route' => 'perfil.upload',"class"=>"form-horizontal",'files'=>true)) !!}
                        {{ csrf_field() }}
                        {{-- NOME DO USUARIO--}}
                        @include("layouts.subviews.inputs.text",["name"=>"name","label"=>"Nome","data"=>auth()->user()->name])
                        {{-- DOCUMENTO VALIDADO CPF--}}
                        @if(auth()->user()->info()->first()->cpf_cnpj)                        
                            @include("layouts.subviews.inputs.text",["name"=>"cpf_cnpj","label"=>"CPF / CNPJ","data"=>auth()->user()->info()->first()->cpf_cnpj])
                        @endif                         
                        {{-- COMPROVANTE END--}}
                        @if(!auth()->user()->info()->first()->comprovante)                        
                          @include("layouts.subviews.inputs.file",["name"=>"comprovante","label"=>"Comprovante Endereço","data"=>""])                         
                        @endif

                        {{-- DOCUMENTO FOTO--}}
                        @if(!auth()->user()->info()->first()->document_file)                        
                          @include("layouts.subviews.inputs.file",["name"=>"document_file","label"=>"Foto do documento","data"=>""])
                        @endif

                        {{-- SELFIE--}}

                        @if(!auth()->user()->info()->first()->perfil_file)                        
                          @include("layouts.subviews.inputs.file",["name"=>"perfil_file","label"=>"Foto Selfie","data"=>""])                                                 
                        @endif

                        @if(strlen($metrics->type()["cpf_cnpj"])>11 && !auth()->user()->info()->first()->origem)
                          @include("layouts.subviews.inputs.file",["name"=>"origem","label"=>"Comprovante de Origem Rendimentos","data"=>""])                                                 
                        @endif

                        @if(strlen($metrics->type()["cpf_cnpj"])>11 && !auth()->user()->info()->first()->ata)
                          @include("layouts.subviews.inputs.file",["name"=>"ata","label"=>"Ata de deliberação","data"=>""])                                                 
                        @endif


                        <div class="form-group ">
                            @if(auth()->user()->info()->first()->comprovante) 
                            <div class="col-sm-4 text-center">
                                <p>Comprovante de Endereço</p>
                                <img style="width:100px;height:100px" class="img-circle img-fluid img-bordered-sm" src="{{url('images')."/".auth()->user()->info()->first()->comprovante}}" alt="User Image">                              
                                <a href="{{route("perfil.delete","comprovante")}}" class="btn btn-danger pull-right btn-block btn-sm">Deletar</a>
                            </div>
                            @endif
                            @if(auth()->user()->info()->first()->document_file) 
                            <div class="col-sm-4 text-center">
                                <p>Seu documento (RG,CPF,Passaporte...)</p>
                                <img style="width:100px;height:100px" class="img-circle img-fluid img-bordered-sm" src="{{url('images')."/".auth()->user()->info()->first()->document_file}}" alt="User Image">                              
                                <a href="{{route("perfil.delete","document_file")}}" class="btn btn-danger pull-right btn-block btn-sm">Deletar</a>
                            </div>
                            @endif
                            @if(auth()->user()->info()->first()->perfil_file) 
                            <div class="col-sm-4 text-center">
                                <p>Foto Selfie</p>
                                <img style="width:100px;height:100px" class="img-circle img-fluid img-bordered-sm" src="{{url('images')."/".auth()->user()->info()->first()->perfil_file}}" alt="User Image">                              
                                <a href="{{route("perfil.delete","perfil_file")}}" class="btn btn-danger pull-right btn-block btn-sm">Deletar</a>
                            </div>
                            @endif
                            @if(auth()->user()->info()->first()->origem) 
                            <div class="col-sm-4 text-center">
                                <p>Origem Rendimentos</p>
                                <img style="width:100px;height:100px" class="img-circle img-fluid img-bordered-sm" src="{{url('images')."/".auth()->user()->info()->first()->origem}}" alt="User Image">                              
                                <a href="{{route("perfil.delete","origem")}}" class="btn btn-danger pull-right btn-block btn-sm">Deletar</a>
                            </div>
                            @endif
                            @if(auth()->user()->info()->first()->ata) 
                            <div class="col-sm-4 text-center">
                                <p>Ata de deliberação</p>
                                <img style="width:100px;height:100px" class="img-circle img-fluid img-bordered-sm" src="{{url('images')."/".auth()->user()->info()->first()->ata}}" alt="User Image">                              
                                <a href="{{route("perfil.delete","ata")}}" class="btn btn-danger pull-right btn-block btn-sm">Deletar</a>
                            </div>
                            @endif                            
                        </div>                         
                    
                    <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Salvar</button>
                      </div>
                    </div>
                  </form>
                </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>


    </div>

  </section>

@endsection