@extends('dashboard.layouts.main')

@push("body-class") horizontal-menu @endpush

@section('content')
  @include("dashboard.partials.head")

  <div class="page-container row-fluid">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="page-content">
          @include("dashboard.partials.menu") 
          <div class="clearfix"></div>
          <div class="content">
            <ul class="breadcrumb">
              <li>
                <p>VOCÊ ESTÁ AQUI</p>
              </li>
              @each('admin.partials.breadcrumb', [['name'=>'Home','active'=>true,'route'=>route('admin.index')]], 'list')
            </ul>
            <div class="page-title"> <i class="icon-custom-left"></i>
              <h3>Dashboard - <span class="semi-bold">Informações</span></h3>
            </div>

            <div class="row">
                    <div class="grid simple">
                        <div class="grid-title no-border">
                            <h4>Sacar <span class="semi-bold"> dinheiro</span></h4>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
    
                            </div>
                        </div>
                        <div class="grid-body no-border"> 
                        @if(session()->has('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div>
                        @endif                   
                        <div class="tab-pane active" id="settings">
                            {!! Form::open(array('route' => 'withdraw.money',"class"=>"form-horizontal",'files'=>true)) !!}
                                {{-- VALOR COMPROVANTE TED--}}
                                @include("layouts.subviews.inputs.text",["name"=>"value","label"=>"Valor que deseja sacar","data"=>""])
                                {{-- Banco--}}
                                @include("layouts.subviews.inputs.select",["name"=>"bank","label"=>"Codigo do Banco","value"=>$wallets["banks"]])
                                {{-- Agencia--}}
                                @include("layouts.subviews.inputs.text",["name"=>"ag","label"=>"Agencia","data"=>""])                        
                                {{-- Conta--}}
                                @include("layouts.subviews.inputs.text",["name"=>"cc","label"=>"Conta","data"=>""])
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-success">Salvar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    <!-- /.tab-pane -->
                </div>
              <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>


          <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><strong>Sacar em Altcoin</strong></div>
                    <div class="panel-body">
                            @if(session()->has('success_btc'))
                                <div class="alert alert-success">
                                    {{ session()->get('success_btc') }}
                                </div>
                            @endif
                            @if ($errors->get("btc_error"))
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif                 
                            @inject("coins","App\Models\Cryptocoin")
                            <div class="tab-pane active" id="settings">
                                {!! Form::open(array('route' => 'withdraw.coin',"class"=>"form-horizontal",'files'=>true)) !!}
                                    <div class="form-group">
                                        <label>Moeda</label>
                                        <select required  name="withdraw_coin" class="form-control" id="crypto-buy">
                                            <option>Escolha a moeda que deseja sacar</option>
                                            @foreach($coins::where("symbol","BTC")->orWhere("symbol","ETH")->orWhere("symbol","BCH")->orWhere("symbol","LTC")->orWhere("symbol","XRP")->get() as $coin)
                                                <option value="{{$coin->id}}">{{$coin->name}}</option>
                                            @endforeach
                                        </select>          
                                        @include('layouts.subviews.alerts.errors', ['field' => "buy_coin"])                                
                                    </div>                                 
                                    {{-- VALOR COMPROVANTE TED--}}
                                    @include("layouts.subviews.inputs.text",["name"=>"withdraw_value","label"=>"Valor que deseja sacar","data"=>""])
                                    {{-- Carteira--}}
                                    @include("layouts.subviews.inputs.text",["name"=>"withdraw_address","label"=>"Endereco da carteira","data"=>""])
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" class="btn btn-success">Salvar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                    <!-- /.nav-tabs-custom -->
            </div>
        </div>

  </section>

@endsection