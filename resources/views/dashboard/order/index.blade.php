@extends('dashboard.layouts.main')

@push("body-class") horizontal-menu @endpush

@section('content')
  @include("dashboard.partials.head")

  <div class="page-container row-fluid">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="page-content">
          @include("dashboard.partials.menu") 
          <div class="clearfix"></div>
          <div class="content">
            <ul class="breadcrumb">
              <li>
                <p>VOCÊ ESTÁ AQUI</p>
              </li>
              @each('admin.partials.breadcrumb', [['name'=>'Home','active'=>true,'route'=>route('admin.index')]], 'list')
            </ul>
            <div class="page-title"> <i class="icon-custom-left"></i>
              <h3>Dashboard - <span class="semi-bold">Informações</span></h3>
            </div>
            
            <div class="row">
                <div class="grid simple">
                    @include("components/wallets")
                </div>
            </div>

            <div class="row">
                <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4>Ordem <span class="semi-bold"> Compra/Venda</span></h4>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>

                        </div>
                    </div>
                    <div class="grid-body no-border">                
                        
                            <div class="col-md-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><strong>Ordens de Compra</strong></div>
                                    <div class="panel-body">
                                        @if(session()->has('success_buy'))
                                            <div class="alert alert-success">
                                                {{ session()->get('success_buy') }}
                                            </div>
                                        @endif                        
                                        @if ($errors->get("buy_error"))
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endif                          
                                        @inject("coins","App\Models\Cryptocoin")
                                        <form action="{{route("order.buy")}}" method="POST" class="" id="buy-form">
                                            {{ csrf_field() }}
                                            <div class="form-group">
                                                <label>Moeda</label>
                                                <select required  name="buy_coin" class="form-control" id="crypto-buy">
                                                    <option>Escolha a moeda que deseja comprar</option>
                                                    @foreach($coins::where("symbol","BTC")->orWhere("symbol","ETH")->orWhere("symbol","BCH")->orWhere("symbol","LTC")->orWhere("symbol","XRP")->get() as $coin)
                                                        <option value="{{$coin->id}}">{{$coin->name}}</option>
                                                    @endforeach
                                                </select>          
                                                @include('layouts.subviews.alerts.errors', ['field' => "buy_coin"])                                
                                            </div>                   
                                            <div class="form-group  invisible crypto-buy">
                                                <label for="">Quantidade da moeda:</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">$</span>
                                                    <input required type="text" name="buy_qntd" placeholder="0.00000" class="form-control buy_qntd">
                                                    @include('layouts.subviews.alerts.errors', ['field' => "buy_qntd"]) 
                                                </div>   
                                            </div>    
                                            <div class="form-group invisible crypto-buy">
                                                <label for="">Preço Unitário:</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">R$</span>
                                                    <input type="text"  name="buy_price" required placeholder="0.0000" class="form-control  buy_price">
                                                    @include('layouts.subviews.alerts.errors', ['field' => "buy_price"]) 
                                                </div>   
                                            </div> 
                                            <div class="form-group  invisible crypto-buy">
                                                <table style="margin: 5px 0;">
                                                    <tbody>
                                                        <tr>
                                                            <td class="text-right" style="padding: 0 3px;">Total (R$):</td>
                                                            <td id="buy-total" style="padding: 0 3px;">0.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" style="padding: 0 3px;">Comissão (<i class="fab fa-ethereum"></i>):</td>
                                                            <td id="buy-taxa" style="padding: 0 3px;">0,00000 - 0,00000</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right" style="padding: 0 3px;"><strong>Liquido (<i class="fab fa-bitcoin"></i>):</strong></td>
                                                            <td id="buy-liquido" style="padding: 0 3px;">0,000000</td>
                                                        </tr>
                                                    </tbody>
                                                </table>                                
                                            </div>                                 

                                            <input class="btn invisible crypto-buy btn-block btn-success mb-green" type="submit" name="submitButton" value="COMPRAR" data-loading-text="aguarde..."  >
                                        </form>                                

                                    </div>
                                </div>    
                            </div>
                            <div class="col-md-6">
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><strong>Ordens de Venda</strong></div>
                                        <div class="panel-body">
                                                @if(session()->has('success_sell'))
                                                    <div class="alert alert-success">
                                                        {{ session()->get('success_sell') }}
                                                    </div>
                                                @endif                        
                                                @if ($errors->get("sell_error"))
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                                @endif  
                                                <form action="{{route("order.sell")}}" method="POST" class="" id="sell-form">
                                                        {{ csrf_field() }}
                                                        <div class="form-group">
                                                                <label>Moeda</label>
                                                                <select  name="sell_coin" required class="form-control" id="crypto-sell">
                                                                    <option>Escolha a moeda que deseja comprar</option>
                                                                    @foreach($coins::where("symbol","BTC")->orWhere("symbol","LTC")->orWhere("symbol","ETH")->orWhere("symbol","XRP")->orWhere("symbol","BCH")->get() as $coin)
                                                                        <option value="{{$coin->id}}">{{$coin->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                                @include('layouts.subviews.alerts.errors', ['field' => "sell_coin"]) 
                                                            </div>                   
                                                        <div class="form-group crypto-sell invisible">
                                                            <label for="">Quantidade da moeda:</label>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">$</span>
                                                                <input  name="sell_qntd" required type="text" class="form-control   sell_qntd">
                                                                @include('layouts.subviews.alerts.errors', ['field' => "sell_qntd"]) 
                                                            </div>   
                                                        </div>    
                                                        <div class="form-group crypto-sell invisible">
                                                            <label for="">Preço Unitário:</label>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">R$</span>
                                                                <input  name="sell_price" required type="text" class="form-control   sell_price">
                                                                @include('layouts.subviews.alerts.errors', ['field' => "sell_price"]) 
                                                            </div>   
                                                        </div> 
                                                        <div class="form-group crypto-sell invisible">
                                                            <table style="margin: 5px 0;">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="text-right" style="padding: 0 3px;">Total (R$):</td>
                                                                        <td id="sell-total" style="padding: 0 3px;">0.00</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="text-right" style="padding: 0 3px;">Comissão (<i class="fab fa-ethereum"></i>):</td>
                                                                        <td id="sell-taxa" style="padding: 0 3px;">0,00000 - 0,00000</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="text-right" style="padding: 0 3px;"><strong>Liquido (<i class="fab fa-bitcoin"></i>):</strong></td>
                                                                        <td id="sell-liquido" style="padding: 0 3px;">0,000000</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>                                
                                                        </div>                                 
                            
                                                        <input class="btn crypto-sell invisible  btn-block btn-warning mb-red" type="submit" name="submitButton" value="VENDER" data-loading-text="aguarde..." >
                                                    </form>   
                                        </div>
                                    </div>    
                            </div>
                    </div>
                </div>
            </div>
            <input type="hidden" value="{{$tax->our_tax/100}}" id="our_tax"/>
  </section>

@endsection

@push('scripts')
<script type="text/javascript">

    $(document).ready(function(){        
        $("#crypto-buy").on("change",function(e){
            changesCoin("buy");
        });
        $("#crypto-sell").on("change",function(e){
            changesCoin("sell");
        });

        $(".buy_qntd").on("keyup",function(e){         
            valuesAtualiza("buy");
        });

        $(".buy_price").on("keyup",function(e){
            valuesAtualiza("buy");
        });

        $(".sell_qntd").on("keyup",function(e){         
            valuesAtualiza("sell");
        });

        $(".sell_price").on("keyup",function(e){
            valuesAtualiza("sell");
        });

        function valuesAtualiza(type){
            var total = $("."+type+"_qntd").val() * $("."+type+"_price").val();
            $("#"+type+"-total").html(numberFormatter( "#,##0.####",total));
            $("#"+type+"-taxa").html($("."+type+"_qntd").val()*$("#our_tax").val());
            $("#"+type+"-liquido").html($("."+type+"_qntd").val()*(1-$("#our_tax").val()));
        }

        function changesCoin(type){
            $("#"+type+"-total").html("0.0000");
            $("#"+type+"-taxa").html("0.0000 - 0.0000");
            $("#"+type+"-liquido").html("0.0000");

             

            var coin = ($("#crypto-"+type+" option:selected").text());
            if (coin=="Bitcoin"){
                $(".crypto-"+type+" > table").find(".fab").each(function(){
                    $(this).removeClass("fa-ethereum").removeClass("cc LTC").removeClass("cc BCH").addClass("fa-bitcoin");
                });
            }else if (coin=="Ethereum"){
                $(".crypto-"+type+" > table").find(".fab").each(function(){
                    $(this).removeClass("fa-bitcoin").removeClass("cc LTC").removeClass("cc BCH").addClass("fa-ethereum");
                });
            }else if (coin=="Litecoin"){
                $(".crypto-"+type+" > table").find(".fab").each(function(){
                    $(this).removeClass("fa-bitcoin").removeClass("cc BCH").addClass("cc LTC").css({ 'font-size': "18px" });
                });
            }
            else{
                $(".crypto-"+type+" > table").find(".fab").each(function(){
                    $(this).removeClass("fa-bitcoin").removeClass("cc LTC").removeClass("fa-ethereum").addClass("cc BCH").css({ 'font-size': "18px" });
                });
            }
            if ($("#crypto-"+type).val()!=undefined){
                $(".crypto-"+type+"").each(function(){
                    $(this).removeClass("invisible");
                });
            }
        }


        //coin taxs

    });
</script>
@endpush