@extends('dashboard.layouts.main')

@push("body-class") horizontal-menu @endpush

@section('content')
  @include("dashboard.partials.head")

  <div class="page-container row-fluid">
        <!-- BEGIN PAGE CONTAINER-->
        <div class="page-content">
          @include("dashboard.partials.menu") 
          <div class="clearfix"></div>
          <div class="content">
            <ul class="breadcrumb">
              <li>
                <p>VOCÊ ESTÁ AQUI</p>
              </li>
              @each('admin.partials.breadcrumb', [['name'=>'Home','active'=>true,'route'=>route('admin.index')]], 'list')
            </ul>
            <div class="page-title"> <i class="icon-custom-left"></i>
              <h3>Dashboard - <span class="semi-bold">Informações</span></h3>
            </div>

            <div class="row">
                    <div class="grid simple">
                        <div class="grid-title no-border">
                            <h4>Ordem <span class="semi-bold"> Compra/Venda</span></h4>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
    
                            </div>
                        </div>
                        <div class="grid-body no-border"> 
                                <div class="col-md-3">
                                    <div class="box">
                                        <div class="box-header">
                                        <h3 class="box-title">Ordens de Compra</h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body no-padding">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th style="width: 60px">#</th>
                                                    <th>Quantidade</th>
                                                    <th>Preço</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse ($arr["compras"] as $buy)
                                                    <tr>
                                                            <td style="font-size:11px">(
                                                            @if ($buy->coin->symbol=="BTC") <i class="fab fa-btc"></i> 
                                                            @elseif ($buy->coin->symbol=="BCH") <i  style="font-size:18px" class="fab cc BCH"></i> 
                                                            @elseif ($buy->coin->symbol=="LTC") <i   style="font-size:18px"class="fab cc LTC"></i> 
                                                            @else <i class="fab fa-ethereum"></i> @endif
                                                            )</td>
                                                        <td>{{ ($buy->qntd) }}</td>
                                                        <td>{{ ($buy->value) }}</td>
                                                       
                                                    </tr>    
                                                @empty
                                                    <tr>
                                                        <td colspan="3">Nenhuma ordem de compra</td>
                                                    </tr> 
                                                @endforelse
                                            
                                            </tbody>
                                        </table>
                                        </div>
                                        <!-- /.box-body -->
                                    </div> 
                                </div>
                                
                                <div class="col-md-3">
                                    <div class="box">
                                        <div class="box-header">
                                        <h3 class="box-title">Ordens de Venda</h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body no-padding">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th style="width:60px">#</th>
                                                    <th>Quantidade</th>
                                                    <th>Preço</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse ($arr["vendas"] as $buy)
                                                    <tr>
                                                            <td>(
                                                            @if ($buy->coin->symbol=="BTC") <i class="fab fa-btc"></i> 
                                                            @elseif ($buy->coin->symbol=="BCH") <i  style="font-size:18px" class="fab cc BCH"></i> 
                                                            @elseif ($buy->coin->symbol=="LTC") <i  style="font-size:18px" class="fab cc LTC"></i> 
                                                            @else <i class="fab fa-ethereum"></i> @endif
                                                        )</td>
                                                        <td>{{ ($buy->qntd) }}</td>
                                                        <td>{{ ($buy->value) }}</td>
                                                        
                                                    </tr>    
                                                @empty
                                                    <tr>
                                                        <td colspan="3">Nenhuma ordem de compra</td>
                                                    </tr> 
                                                @endforelse
                                        </tbody></table>
                                        </div>
                                        <!-- /.box-body -->
                                    </div> 
                                </div>

                                <div class="col-md-6">
                                    <div class="box">
                                        <div class="box-header">
                                        <h3 class="box-title">Negociações</h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body no-padding">
                                        <table class="table table-striped">
                                            <tbody>
                                                <tr>
                                                    <th style="width: 50px">#</th>
                                                    <th >Data</th>
                                                    <th>Quantidade</th>
                                                    <th>Preço</th>
                                                    <th>Tipo</th>
                                                </tr>
                                            @forelse ($arr["confirmadas"] as $order)
                                                <tr>                                                      
                                                    <td style="font-size:11px">(
                                                        @if ($buy->coin->symbol=="BTC") <i class="fab fa-btc"></i> 
                                                        @elseif ($buy->coin->symbol=="BCH") <i  style="font-size:18px" class="fab cc BCH"></i> 
                                                        @elseif ($buy->coin->symbol=="LTC") <i  style="font-size:18px" class="fab cc LTC"></i> 
                                                        @else <i class="fab fa-ethereum"></i> @endif
                                                    )</td>
                                                    <td>{{ date('d/m/Y', strtotime($order->created_at)) }} </td>
                                                    <td>
                                                        @if($order->soubrou_comprador>0)
                                                            {{$order->vendendo}}
                                                        @else
                                                            {{$order->comprando}}
                                                        @endif
                                                    </td>
                                                    @if ($order->valor_compra<=$order->valor_venda)
                                                        <td>R$ {{number_format($order->valor_compra,2,",",".")}}</td>
                                                    @else
                                                        <td>R$ {{number_format($order->valor_venda,2,",",".")}}</td>
                                                    @endif
                                                    <td>{{$order->type_order}}</td>
                                                </tr>                          
                                            @empty
                                                <tr>
                                                    <td colspan="3">Nenhuma negociações realizadas</td>
                                                </tr> 
                                            @endforelse
                                        </tbody></table>
                                        </div>
                                        <!-- /.box-body -->
                                    </div> 
                                </div>               
                        </div>
                    </div>
                </div>
    <input type="hidden" value="{{$tax->our_tax/100}}" id="our_tax"/>
  </section>

@endsection

@push('scripts')
<script type="text/javascript">

    $(document).ready(function(){        
        $("#crypto-buy").on("change",function(e){
            changesCoin("buy");
        });
        $("#crypto-sell").on("change",function(e){
            changesCoin("sell");
        });

        $(".buy_qntd").on("keyup",function(e){         
            valuesAtualiza("buy");
        });

        $(".buy_price").on("keyup",function(e){
            valuesAtualiza("buy");
        });

        $(".sell_qntd").on("keyup",function(e){         
            valuesAtualiza("sell");
        });

        $(".sell_price").on("keyup",function(e){
            valuesAtualiza("sell");
        });

        function valuesAtualiza(type){
            var total = $("."+type+"_qntd").val() * $("."+type+"_price").val();
            $("#"+type+"-total").html(numberFormatter( "#,##0.####",total));
            $("#"+type+"-taxa").html($("."+type+"_qntd").val()*$("#our_tax").val());
            $("#"+type+"-liquido").html($("."+type+"_qntd").val()*(1-$("#our_tax").val()));
        }

        function changesCoin(type){
            $("#"+type+"-total").html("0.0000");
            $("#"+type+"-taxa").html("0.0000 - 0.0000");
            $("#"+type+"-liquido").html("0.0000");

             

            var coin = ($("#crypto-"+type+" option:selected").text());
            if (coin=="Bitcoin"){
                $(".crypto-"+type+" > table").find(".fab").each(function(){
                    $(this).removeClass("fa-ethereum").addClass("fa-bitcoin");
                });
            }else{
                $(".crypto-"+type+" > table").find(".fab").each(function(){
                    $(this).removeClass("fa-bitcoin").addClass("fa-ethereum");
                });
            }
            if ($("#crypto-"+type).val()!=undefined){
                $(".crypto-"+type+"").each(function(){
                    $(this).removeClass("invisible");
                });
            }
        }


        //coin taxs

    });
</script>
@endpush