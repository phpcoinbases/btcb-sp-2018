@extends('dashboard.layouts.main')

@push("body-class") horizontal-menu @endpush

@section('content')
  <div class="container">
      <div id="dashboard"></div>
  </div>
@endsection