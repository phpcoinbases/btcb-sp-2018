import React, { Component } from 'react';

export default class Dashboard extends Component {
    constructor(props){
        super(props);
        this.state = {text: '',data: [], mode:'view',loading:true};                
      }     
      componentDidMount(){
        let uri = Laravel.json_get_wallets;
        axios.get(uri).then((response) => {
            this.setState({ loading: false });
            this.setState({ data: response.data });
        }).catch(function (error) {
            console.log(error);
        });
      }      
    render() {
        return (
            <div className="container my-5">
                <div className="row justify-content-center">
                    <div className="col-md-12">
                        <div className="card">
                            <div className="card-header">Suas Carteiras</div>
                            {
                            this.state.loading && (
                            <div className="card-body" >
                                Carregando...                                
                            </div>
                            )}
                            {
                            !this.state.loading && (
                            <div className="card-body" >
                                  <table className="table">
                                        <thead>
                                            <tr>                                            
                                                <th scope="col">Moeda</th>
                                                <th scope="col">Endereco</th>
                                                <th scope="col">Valor</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.data.map(function(item, key) {
                                                return (
                                                <tr key={key}>
                                                    <td>{item.coin}</td>
                                                    <td>{item.address}</td>
                                                    <td>{item.body}</td>
                                                </tr>
                                                )
                                            
                                            })}
                                        </tbody>
                                    </table>   
                            </div>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

