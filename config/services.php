<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],


    'facebook' => [
        'client_id' => env("FACEBOOK_CLIENT_ID"), //Facebook API
        'client_secret' => env("FACEBOOK_CLIENT_SECRET"), //Facebook Secret
        'redirect' => env("APP_URL").env("FACEBOOK_REDIRECT"),
    ],
    'google' => [
        'client_id' =>  env("GOOGLE_APP"),
        'client_secret' =>  env("GOOGLE_SECRET"),
        'redirect' =>  env("APP_URL").env("GOOGLE_REDIRECT"),
    ],        
    'live' => [
        'client_id' => env('LIVE_KEY'),
        'client_secret' => env('LIVE_SECRET'),
        'redirect' => env("APP_URL").env('LIVE_REDIRECT_URI'),  
    ], 
    'instagram' => [
        'client_id' => env('INSTAGRAM_KEY'),
        'client_secret' => env('INSTAGRAM_SECRET'),
        'redirect' => env("APP_URL").env('INSTAGRAM_REDIRECT_URI')
    ],  
];
