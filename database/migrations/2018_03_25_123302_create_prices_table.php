<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
            $table->increments('id'); 
            $table->integer('cryptocoin_id')->unsigned();
            $table->foreign('cryptocoin_id')->references('id')->on('cryptocoins');                      
            $table->string("price_usd");
            $table->string("price_btc");
            $table->string("volume_usd");
            $table->string("market_cap_usd");
            $table->string("available_supply");
            $table->string("total_supply");
            $table->string("percent_change_1h");
            $table->string("percent_change_24h");
            $table->string("percent_change_7d");
            $table->string("price_brl");
            $table->string("volume_brl");
            $table->string("market_cap_brl");
            $table->string('last_updated');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prices');
    }
}
