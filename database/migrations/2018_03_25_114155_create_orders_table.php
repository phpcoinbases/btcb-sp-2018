<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['sell', 'buy']);

            $table->float("value",20,10);
            $table->float("qntd",20,10);
  

            $table->unsignedInteger('user_id');            
            $table->foreign('user_id')->references('id')->on('users');                            

            $table->enum("status",["pending","active","negative","final"])->default("pending");
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
