<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInfosToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_info', function (Blueprint $table) {
            $table->increments('id'); 
            $table->string("uuid")->nullable();
            $table->string("perfil_file")->nullable();
            $table->string("document_file")->nullable();
            $table->string("comprovante")->nullable();
            $table->string("origem")->nullable();
            $table->string("ata")->nullable();
            $table->string("recursos")->nullable();
            $table->string("ata_pj")->nullable();
            $table->string("cpf_cnpj")->nullable();
            $table->string("cep")->nullable();
            $table->string("phone")->nullable();
            $table->string("address")->nullable();
            $table->string("number")->nullable();
            $table->string("city")->nullable();
            $table->string("uf")->nullable();
            $table->string("country")->nullable();
            $table->string("complemento")->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_info');
    }
}
