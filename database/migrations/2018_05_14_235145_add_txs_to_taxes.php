<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTxsToTaxes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('taxes', function (Blueprint $table) {
            $table->integer('tx_id')->unsigned()->nullable();
            $table->foreign('tx_id')->references('id')->on('txes');    
        });
        Schema::table('tax_users', function (Blueprint $table) {
            $table->integer('tx_id')->unsigned()->nullable();
            $table->foreign('tx_id')->references('id')->on('txes');        
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('taxes', function (Blueprint $table) {
            $table->dropForeign(['tx_id']);
        });
        Schema::table('tax_users', function (Blueprint $table) {
            $table->dropForeign(['tx_id']);
        });
    }
}
