<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeWithdrawsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('withdraws', function (Blueprint $table) {
            $table->dropColumn(['valueeth', 'valuebtc']);
            $table->dropColumn('status');
            $table->dropColumn('type');            
            $table->decimal('value_coin',20,10)->default("0.0");
            $table->decimal('value_tax',20,10)->default("0.0");
            $table->integer('cryptocoin_id')->unsigned()->nullable();
            $table->foreign('cryptocoin_id')->references('id')->on('cryptocoins');              
            $table->string("type")->nullable()->change();
            //["pending","email","active","final","negative"]
            $table->string("status")->default("pending")->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('withdraws', function (Blueprint $table) {
            $table->dropForeign(['cryptocoin_id']);
            $table->dropColumn('value_tax');
            $table->dropColumn('value_coin');
            $table->dropColumn('cryptocoin_id');
        });
    }
}
