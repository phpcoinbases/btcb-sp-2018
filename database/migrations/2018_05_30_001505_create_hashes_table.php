<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHashesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hashes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cryptocoin_id')->unsigned()->nullable();
            $table->foreign('cryptocoin_id')->references('id')->on('cryptocoins');  
            $table->string("uuid")->nullable();     
            $table->text("hash")->nullable();   
            $table->text("address")->nullable();                           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hashes');
    }
}
