<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extracts', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('tax_id')->unsigned()->nullable();
            $table->foreign('tax_id')->references('id')->on('taxes');
            $table->integer('wallet_id')->unsigned()->nullable();
            $table->foreign('wallet_id')->references('id')->on('wallets');
            $table->integer('order_id')->unsigned()->nullable();
            $table->foreign('order_id')->references('id')->on('orders');
            $table->integer('cryptocoin_id')->unsigned()->nullable();
            $table->foreign('cryptocoin_id')->references('id')->on('cryptocoins');
            $table->integer('deposit_id')->unsigned()->nullable();
            $table->foreign('deposit_id')->references('id')->on('deposits');
            $table->enum("type",["Deposito","Saque","Venda","Compra"]);           
            $table->float('value',20,10)->default("0.00");
            $table->float('qntd',20,10)->default("0.00");            
            $table->string("comprovante")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('extracts');
    }
}
