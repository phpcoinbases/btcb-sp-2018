<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUuidToTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wallets', function (Blueprint $table) {
            $table->string("uuid");
        });
        Schema::table('deposits', function (Blueprint $table) {
            $table->string("uuid");
        });
        Schema::table('transactions', function (Blueprint $table) {
            $table->string("uuid");
        });
        Schema::table('withdraws', function (Blueprint $table) {
            $table->string("uuid");
        });
        Schema::table('users', function (Blueprint $table) {
            $table->string("uuid");
        });
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
