<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddToExtractTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('extracts', function (Blueprint $table) {
            $table->integer('withdraw_id')->unsigned()->nullable();
            $table->foreign('withdraw_id')->references('id')->on('withdraws');  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('extracts', function (Blueprint $table) {
            $table->dropForeign(['withdraw_id']);
        });
    }
}
