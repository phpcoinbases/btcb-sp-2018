<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Transactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

     /*                         
     
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_ordem_compra')->unsigned()->nullable();
            $table->foreign('id_ordem_compra')->references('id')->on('orders');
            $table->integer('id_ordem_venda')->unsigned()->nullable();
            $table->foreign('id_ordem_venda')->references('id')->on('orders');

            $table->integer('seller_id')->unsigned()->nullable();
            $table->foreign('seller_id')->references('id')->on('users');   
            
            $table->integer('buyer_id')->unsigned()->nullable();
            $table->foreign('buyer_id')->references('id')->on('users');   

            $table->decimal('vendendo',20,10)->default("0.0");
            $table->decimal('comprando',20,10)->default("0.0");
            $table->decimal('taxa_compra',20,10)->default("0.0");
            $table->decimal('taxa_venda',20,10)->default("0.0");            
            $table->decimal('origem_qntd_sell',20,10)->default("0.0");
            $table->decimal('origem_qntd_buy',20,10)->default("0.0");
            $table->decimal('vendedor_vai_receber',20,10)->default("0.0");
            $table->decimal('comprador_vai_receber',20,10)->default("0.0");
            $table->decimal('soubrou_vendedor',20,10)->default("0.0");
            $table->decimal('soubrou_comprador',20,10)->default("0.0");
            $table->decimal('valor_venda',20,10)->default("0.0");
            $table->decimal('valor_compra',20,10)->default("0.0");

            
            $table->timestamps();
        
        });

        Schema::table('orders', function (Blueprint $table) {
   
            $table->decimal('original',20,10)->default("0.0");

        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn("original");
        });        
    }
}
