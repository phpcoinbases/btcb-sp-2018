<?php

use Illuminate\Database\Seeder;
use App\Models\Tax;
use App\Models\Tx;
class TaxTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $tx = Tx::create(["name"=>"Default - Taxas Padrões"]);

        $array = [
            ["our_tax"=>0,"fixed"=>0,"type"=>"deposit_cash","tx_id"=>$tx->id],
            ["our_tax"=>0,"fixed"=>0,"type"=>"deposit_btc","tx_id"=>$tx->id],
            ["our_tax"=>0.5,"fixed"=>0,"type"=>"withdraw_cash","tx_id"=>$tx->id],
            ["our_tax"=>0.5,"fixed"=>8.9,"type"=>"withdraw_not_bank","tx_id"=>$tx->id],
            ["our_tax"=>0.001,"fixed"=>0,"type"=>"withdraw_btc","tx_id"=>$tx->id],
            ["our_tax"=>0.001,"fixed"=>0,"type"=>"withdraw_eth","tx_id"=>$tx->id],
            ["our_tax"=>0.25,"fixed"=>0,"type"=>"passive","tx_id"=>$tx->id],
            ["our_tax"=>0.45,"fixed"=>0,"type"=>"ative","tx_id"=>$tx->id]            
        ];

        foreach ($array as $value){
            Tax::create($value);
        }
    }
}
