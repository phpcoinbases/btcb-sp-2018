<?php

use Illuminate\Database\Seeder;
//use EthereumPHP\Types\Hash;
use App\User;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use App\Models\Coin;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BankTableSeeder::class);
        $this->call(TaxTableSeeder::class);
        $user = User::create([
             "name"=>"Sys Admin TI",
             "email"=>"admin@coinbold.com.br",
             'password' => bcrypt('r7WvhCWfhBKds2fK6Y3A9Vp44rcDG6xhL3mDNPPg'),
             "check_admin"=>true,
        ]);
        $role = Role::create(['name' => 'super-admin']);
        $user->assignRole($role);
        Coin::create(["symbol"=>"USD","name"=>"Dolar americano"]);
        Coin::create(["symbol"=>"EUR","name"=>"Euro"]);

    }
}
